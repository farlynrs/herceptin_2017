(function(global) {
  
  // Uncomment to see trace from framework in console
   // debug();
   
   var query_string = document.location.href;
	query_string = query_string.split('=');
	var query_string_value = query_string[1];
	var appType = "json";
	var dynamicJson = '';

  //dynamicJson=window["AVASTIN"]();
  // Creating our presentation and global namespace "app"
  global.app = new Presentation({
	type: appType,
    globalElements: ['mainmenu', 'mainfooter', 'slidePopup', 'popupBackButton', 'wPenBtns'],
	dynamicJson: dynamicJson,
    manageMemory: true
  });
  //builder.checkIfNeedToLoadPresentation();

  // Initiate modules
  app.refs = new References('references', 'refPopup');
  // uncomment to enable, need to uncomment elements in index.html and the overlay-btn in menu below
  app.uguide = new Userguide('userGuide','userGuideTrigger');
  app.scroller = new Slidescroller();
  app.data = new Data();
  app.slidePopup = new SlidePopup('slidePopup', 'popupBackButton');
  app.transPopup = new SlidePopup('transPopup');
  app.discPopup = new SlidePopup('discPopup');
  app.transPopupInner = new SlidePopup('transPopupInner');
  app.servicePopup = new SlidePopup('servicePopup');
  app.shoppingPopup = new SlidePopup('shoppingPopup');
  app.patientPopup = new SlidePopup('patientPopup', 'ppHomeBtn');
  app.caseStudiesPopup = new SlidePopup('caseStudiesPopup','caseStudiesPopupCloseButton');

  //Setup of thumbnails
  app.thumbs = new BasicThumbs('thumbs', 'efficacy', true, true);
  app.fastThumbs = new BasicThumbs('thumbs', 'fasttrack', true, true);
  app.ppThumbs = new BasicThumbs('thumbs', 'patientprofile', true, true);
  app.safetyThumbs = new BasicThumbs('thumbs', 'safety', true, true);
  app.moaThumbs = new BasicThumbs('thumbs', 'moa', true, true);

  app.survivalThumbs   = new BasicThumbs('thumbs', 'in_Survival', true);
  app.tumourThumbs   = new BasicThumbs('thumbs', 'in_Control_tumour_growth', true);
  app.symptomThumbs   = new BasicThumbs('thumbs', 'in_Symptom_control', true);
  app.qualityThumbs   = new BasicThumbs('thumbs', 'in_Quality_of_life', true);
  app.toleranceThumbs   = new BasicThumbs('thumbs', 'in_Tolerance', true);
  app.treatmentThumbs   = new BasicThumbs('thumbs', 'in_Treatment_adherence', true);

  app.homeTrackThumbs = new BasicThumbs('thumbs', 'homeTrack', true);

  app.efficacyCounter = new SlideCounter('slideCounter',['efficacy'] ,'#652D89');
  app.patientCounter = new SlideCounter('slideCounter',['patientprofile'] ,'#652D89');
  app.safetyCounter = new SlideCounter('slideCounter',['safety'] ,'#652D89');
  app.moaCounter = new SlideCounter('slideCounter',['moa'] ,'#652D89');
  
  // app.slideCounter = new SlideCounter('slideCounter',['efficacy','patientprofile','safety','moa'] ,'#652D89');

  app.fastTrackSlideCounter = new SlideCounter('fastTrackSlideCounter',['fasttrack'] ,'#652D89');
  app.cavitationInlineCounter = new InlineSlideCounter('cavitationSlideCounter','patientprofile','pp_tc_slideshow' ,'gray');
  app.chemotherapySlideCounter = new InlineSlideCounter('chemotherapySlideCounter','patientprofile','pp_ce_slideshow' ,'gray');
  app.cnsSlideCounter = new InlineSlideCounter('cnsSlideCounter','patientprofile','pp_cns_slideshow' ,'gray');
  app.anticoagulationSlideCounter = new InlineSlideCounter('anticoagulationSlideCounter','patientprofile','pp_an_slideshow' ,'gray');
  app.ctlSlideCounter = new InlineSlideCounter('ctlSlideCounter','patientprofile','pp_ctl_slideshow' ,'gray');
  app.hypertensionSlideCounter = new InlineSlideCounter('hypertensionSlideCounter','patientprofile','pp_hy_slideshow' ,'gray');
  app.elderlySlideCounter = new InlineSlideCounter('elderlySlideCounter','patientprofile','pp_el_slideshow' ,'gray');
  

  // Initialize presentation
  /*for(var i=app.json.structures["storyboard"].content.length-1; i>=0; i--){
	app.init(app.json.structures["storyboard"].content[i]);
	//app.load(app.json.structures["storyboard"].content[i]);
  }*/
  /*for(var i=app.json.structures["main"].content.length-1; i>=0; i--){
    app.init(app.json.structures["main"].content[i]);
    app.load(app.json.structures["main"].content[i]);
  }*/
  //app.init("PP_intro");
  //app.init("IN_selecttopics");.
  //app.init("main");
  //app.init("home");
  app.init();
  
})(window);

// Prevent vertical bouncing of slides
document.ontouchmove = function(e) {
   e.preventDefault();
};

// function submitUniqueCustomEvent()
// {
//     console.log("submitUniqueCustomEvent");
// }