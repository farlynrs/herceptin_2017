﻿//Do not touch this parameter. Used for workflow
var firstLoaded = true; 

//Initial load time for presentation
var redirectTimeout = 1000;

//Language selection file (languages located in modules/localisation/languages
var language = "spanish.xml";

//Set the page titles
//document.title = "Kadcyla";

//Menu settings - set width of menu here;
var menuWidth = 560;

//Thumbnail settings - whether thumbnail menu shows when section clicked on
var showThumbnailsOnMenuClick = false;
//This needs further work. Please leave true
var showThumbnailIfOnlyOneSlide = true;

//Builder settings - whether slides and sections are in forced orders or not
var allowOrderingOfSections = true;
var allowOrderingOfSlides = true;
//Whether to show builder in menu
var showBuilder = true;

//This app key needs to be in the format '&ProjectName' and must be unique between projects
var appkey = '&kadcyla';

//This decides whether to show full presentation in dropdown list on home page
var showFullPresentationInDropDown = true;

//Allow swiping up and down at end of slide show to navigate between sections
var allowUpDownSwipeSectionChange = true;

//This is the list of chapters for the presentation
//Prefixing a chapter with '!' will force the section plus all its slides to be automatically chosen
//Prefixing a chapter with '+' will force all slides in that section to be chosen automatically. They cannot be deselected.
//Prefixing a chapter with a '-' will make the chapter unavailable in the builder
var chapterarray = ['!Inicio', 'MOA', 'Presentación', 'Eficacia', 'Seguridad', 'Indicación y dosificación', 'Referencias'];

//Array que relaciona los nombres de los secciones definidas arribas con el presentation.json
var chapterarrayReal = new Object();
chapterarrayReal['Inicio'] = 'inicio';
chapterarrayReal['MOA'] = 'MOA';
chapterarrayReal['Presentación'] = 'presentacion';
chapterarrayReal['Eficacia'] = 'eficacia';
chapterarrayReal['Seguridad'] = 'seguridad';
chapterarrayReal['Indicación y dosificación'] = 'indicacion_y_dosificacion';
chapterarrayReal['Referencias'] = 'referencias';

//These are the slides that belong to each chapter above.
//If the section has sections forced (prefix with '!') then you can prefix slides with '+' to make that slide not unselectable
var selectionsarray = new Array();

var appJSON;

var JSONURL = 'presentation.json'
var JSONURLPos = location.pathname.indexOf("Builder");
if(JSONURLPos > -1){JSONURL = '../../'+JSONURL;} 

jQuery.ajax({
	url: JSONURL,
	async: false,
	dataType: 'json',
	success: function(data){
		appJSON = data;

		//Borramos los elementos que no se usan en el Dynamic Agenda
		data['structures']['kadcyla'] = null;
		data['structures']['inicio'] = null; //Este se borra pero se reemplaza mas adelante

		//Secciones con caracteristicas especiales...
		selectionsarray.push(['+inicio']);

		//Agregamos las otras secciones
		for(var grupo in data['structures']){
			if(data['structures'][grupo] != null){
				if(data['structures'][grupo]['content'] != undefined){
					selectionsarray.push(data['structures'][grupo]['content']);
				}
			}
		}
	}
});


