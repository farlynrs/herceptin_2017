/**
* AMP MODULE - Slide Counter
 * This is a very basic slide counter module
 * To use, in setup.js call the constructor with 
 * app.slideCounter = new SlideCounter('slideCounterID',['slideshow1', 'slideshow2', ... ,'slideshown'] ,'#652D89');
 * 
 * @author - Isen Beqiri, i.beqiri@gmail.com
 */

(function() {

  window.SlideCounter = function(id, slideshows, color) {
    this.version = 'AvastinBasicThumbs_v2';
    this.id = id;
    this.slideshows = slideshows || [];
    this.ele = document.getElementById(id);
    this.ele.style.color = color || 'black' ; 
    this.totalSlides = 0;
    this.currentSlide = 0;
    this.slideCounts= {}
    this._init();
    this._countTotalSlides();
  };
  window.InlineSlideCounter = function(id, parentSlide, slideshow, color){
    this.version = 'AvastinBasicThumbs_v2';
    this.id = id;
    this.slideshow = slideshow;
    this.parentSlide = parentSlide
    this.ele = document.getElementById(id);
    this.color = color || 'black' ; 
    this.totalInlineSlides = 0;
    this.currentInlineSlide = 0;
    this.slideCounts= {}
    this._init();
  };
  SlideCounter.prototype = {
    _init: function() {

      var self = this;
      document.addEventListener('slideEnter', function() {
        currentS = app.slideshow.id       
        if (self.slideshows.indexOf(currentS) >= 0)  {
          self.ele.innerHTML = self.slideCounts[app.slideshow.current] + "/" + self.totalSlides
          self.ele.style.display = "block"
        }
      },false);
     
      document.addEventListener('slideExit', function() {
          self.ele.style.display = "none"
      },false);
    },
    _countTotalSlides: function() {
      var self= this;
      slideshows = self.slideshows
      sIndex = 1
      for(key in slideshows)        
        { 
		if(app.slideshows[slideshows[key]] != null){
          slideshow = app.slideshows[slideshows[key]].content
          for(slide in slideshow) 
          {
            this.slideCounts[slideshow[slide]] = sIndex;
            sIndex++;
          }
          self.totalSlides += slideshow.length;
		  }
        }  // console.log("SlideCounrs", this.slideCounts)

    }
    
  };

   InlineSlideCounter.prototype = {
    _init: function() {
      var self = this;
      document.addEventListener('slideEnter', function() {
        currentS = app.slideshow.id
        if (currentS === self.parentSlide && app.slideshow.inline.hasOwnProperty(self.slideshow)){
          self.ele = document.getElementById(self.id)
          if(self.ele)
          {
            self.ele.style = self.color;
            currentInline = self.slideshow
            currentIndex = app.slideshow.inline[currentInline].currentIndex + 1;
            totalInlineSlides = app.slideshow.inline[currentInline].content.length
            self.ele.innerHTML = currentIndex + '/' + totalInlineSlides
            self.ele.style.color = self.color;
            self.ele.style.display = "block";
            }
        }

      },false);

      document.addEventListener('slideExit', function() {
        // if (currentS === self.parentSlide && app.slideshow.inline.hasOwnProperty(self.slideshow)){
          self.ele = null;
          // self.ele.style.display = "none"
        // }
      },false);
    }
  };



})();
