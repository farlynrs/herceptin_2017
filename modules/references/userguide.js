/* User guide overlay Module
 * This module is customized for this presentation
 * */
(function(){
	var d = document;

	window.Userguide = function(id, trigger){
		var self = this;
		this.version = 'AvastinUserguide_v1';
		this.id = id;
		this.ele = app.elements.userguide = d.getElementById(id);
		this.trigger = d.getElementById(trigger);
		this.thumbEle = d.getElementById("thumbswrapper");
		this.current = '';
		this.previous = '';
		this.customDot = '';
		this.currentDots;
		this.markup = '';
		this.isVisible = false;
		setTimeout(function(){
			self.init();
		}, 0);
	};

	Userguide.prototype = {
		init:function(){
			var self = this;

			if(this.current === ''){
				this.current = 'IN_intro';
				self.update(this.current);
			}

			document.addEventListener('slideEnter', function(e){
				var current = e.target.id;
				self.update(current);
			});
			document.addEventListener('inlineSlideEnter', function(e){
				var current = e.target.id;
				self.update(current);
			});
			document.addEventListener('slidePopupLoad', function(e){
				var current = e.target.id;
				self.update(current);
			});
			document.addEventListener('slidePopupUnload', function(e){
				var current = app.slideshow.current;
				// When exiting the popup in a popup within ef_4months Im making sure that the correct dots are set.
				if(app.transPopup.isVisible && current === 'EF_4months'){
					self.reset("EF_4mo_jmdb");
					self.update("EF_4mo_jmdb");
				}
				else{
					self.reset(current);
					self.update(current);
				}
			});

			this.trigger.addEventListener('tap', function(e){
				touchy.stop(e);
				self.showUserGuide();
			});

			// Prevent swipe and referencepopup from working in userguide-mode
			app.elements.userguide.addEventListener('swipeleft', function(e){
				touchy.stop(e);
			});
			app.elements.userguide.addEventListener('swiperight', function(e){
				touchy.stop(e);
			});
			var two = touchy.isTouch ? 'twoFingerTap' : 'dblclick';
			app.elements.userguide.addEventListener(two, function(e){
				touchy.stop(e);
			});
			if(!touchy.isTouch){
				app.elements.userguide.addEventListener('longTouch', function(e){
					touchy.stop(e);
				});
			}
		},

		/*update:function(current){
			this.previous = '';

			if(!app.userguidemap[current]){
				app.elements.userguide.style.zIndex = '-1';
				app.elements.userguide.style.opacity = '0';
				util.removeClass(this.trigger,'ug-on');
				this.set('');
			}
			else{
				this.set(current);
			}
		},*/
		showUserGuide:function(){
			var self = this;
			if(app.userguidemap[self.current]){
				if(this.isVisible){
					this.isVisible = false;
					this.removeGuideListeners();
					app.elements.userguide.style.zIndex = '-1';
					app.elements.userguide.style.opacity = '0';
					util.removeClass(self.trigger,'ug-on');
				}else{
					this.isVisible = true;
					self.ele.innerHTML = this.assembleBuild();
					if(app.slideshow.current === 'FT_targetvegf'){
						// to check which background to use
						var state = app.slide.FT_targetvegf.element.imageContainer.className.search("active");
						if(state===-1){
							app.slide.FT_targetvegf.element.currSelected = 'normal'
						}
						var allBubbles = app.uguide.ele.querySelectorAll(".bubblebg");
						for (var i = 0; i < allBubbles.length; i++) {
							util.addClass(allBubbles[i],app.slide.FT_targetvegf.element.currSelected+'bg');
						};
					}
					var allthedots = app.elements.userguide.querySelectorAll(".guidedot");
					this.addGuideListeners(allthedots);
					app.elements.userguide.style.zIndex = '1100';
					app.elements.userguide.style.opacity = '1';
					util.addClass(self.trigger,'ug-on');
				}
			}
		},
		assembleBuild:function(){
			var fullContent = '';
			if(this.customDot){
				if(this.current !== "SA_rnk_hyper_men" && this.current !== "SA_rnk_protein_men"
				&& this.current !== "SA_rnk_thromb_men" && this.current !== "SA_rnk_haemo_men"){
					fullContent += this.buildOverlay(this.customDot);
				}
			}
			if(util.hasClass(this.thumbEle, 'active')){
				fullContent += this.buildOverlay("open_slideoverview");
			}
			if(drawTool.style.height === "150px"){
				fullContent += this.buildOverlay("open_drawtool");
			}
			if(app.refs.isVisible){
				fullContent += this.buildOverlay("open_reflist");
			}
			fullContent += this.buildOverlay(this.current);

			if(this.current === 'ST_savedtracks'){
				var savedPresentations = document.querySelector("#scrollWrapper li");
				if(!savedPresentations){
					fullContent = this.buildOverlay("generic_dots");
				}
			}
			
			return fullContent;
		},
		buildOverlay:function(slideDots){
			var content = '',
				dotType = '';

			app.userguidemap[slideDots].forEach(function(overlay){
				if(overlay.number){
					dotType = overlay.type || '';
					content += '<div class="guidedot ' + dotType + ' ' + overlay.ele + '">' + overlay.number + '</div>';
					content += '<div class="guidetextbox ' + dotType + '">' + overlay.text + '</div>';
				}
				else{
					for (var key in overlay) {
					   var obj = overlay[key];
					   dotType = obj.type || '';
					   content += '<div class="guidedot ' + dotType + ' ' + obj.ele + '">' + obj.number + '</div>';
					   content += '<div class="guidetextbox ' + dotType + '">' + obj.text + '</div>';
					}
				}				
			});

			if(app.slideshow.current === 'FT_targetvegf'){
				content+='<div class="hl-bubbles avastinbubble1 bubblebg"></div><div class="hl-bubbles avastinbubble2 bubblebg"></div><div class="hl-bubbles avastinbubble3 bubblebg"></div><div class="hl-bubbles vegfbubble1 bubblebg"></div><div class="hl-bubbles vegfbubble2 bubblebg"></div><div class="hl-bubbles vegfbubble3 bubblebg"></div><div class="hl-bubbles vegfbubble4 bubblebg"></div><div class="hl-bubbles vegfbubble5 bubblebg"></div><div class="hl-bubbles vegfbubble6 bubblebg"></div><div class="hl-bubbles vegfbubble7 bubblebg"></div><div class="hl-bubbles vegfbubble8 bubblebg"></div><div class="hl-bubbles vegfbubble9 bubblebg"></div><div class="hl-bubbles vegfbubble10 bubblebg"></div><div class="hl-bubbles vegfbubble11 bubblebg"></div><div class="hl-bubbles vegfbubble12 bubblebg"></div><div class="hl-bubbles vegfbubble13 bubblebg"></div><div class="hl-bubbles vegfbubble14 bubblebg"></div><div class="hl-bubbles vegfbubble15 bubblebg"></div><div class="hl-bubbles receptbubble1 bubblebg"></div><div class="hl-bubbles receptbubble2 bubblebg"></div><div class="hl-bubbles receptbubble3 bubblebg"></div>';
			}
			
			return content;
		},
		reset:function(name){
			this.current = name || '';
			this.ele.innerHTML = '';
		},
		set:function(name){
			var name = name || app.slideshow.current;
			this.previous = this.current;
			this.current = name;
		},
		unset:function(){
			if(this.previous){
				this.current = this.previous;
			}
		},
		removeGuideListeners:function(){
			for (var i = 0; i < this.currentDots.length; i++) {
				this.currentDots[i].removeEventListener('tap', this.showTextbox,false);
			}
		},
		addGuideListeners:function(allDots){
			this.currentDots = allDots;
			for (var i = 0; i < allDots.length; i++) {
				allDots[i].addEventListener('tap', this.showTextbox,false);
			}
		},
		showTextbox:function(e){
			var ele = e.target;
			if (ele.nodeType === 3) {
				ele = ele.parentNode;
			}
			var dotEle = ele;
			var eleString = ele.className;
			eleString = eleString.replace("guidedot ",'');
			eleString = eleString.replace("green-dot",'');
			eleString = eleString.replace("generaldot ",'');
			eleString = eleString.replace(" ",'');
			eleString = '.'+eleString+' '+'+ .guidetextbox';
			// string modifications done to be able to target specific textbox following the currentdot
			var textboxEle = app.elements.userguide.querySelector(eleString);
			var allGreenDots = app.uguide.ele.querySelectorAll(".green-dot");

			if(textboxEle.style.display === 'none' || textboxEle.style.display === ''){
				var guideTextBoxes = app.elements.userguide.querySelectorAll('.guidetextbox');
				for (var i = 0; i < guideTextBoxes.length; i++) {
					guideTextBoxes[i].style.display = 'none';
				}
				textboxEle.style.display = 'block';
				if(app.slideshow.current === 'FT_targetvegf'){
					if(dotEle.className.search("avastin")!==-1 || dotEle.className.search("vegf")!==-1 || dotEle.className.search("recept")!==-1){
						for (var i = 0; i < allGreenDots.length; i++) {
							util.removeClass(allGreenDots[i],"green-dot");
						};
						util.addClass(dotEle,"green-dot");
					}
				}
			}
			else{
				textboxEle.style.display = 'none';
				if(app.slideshow.current === 'FT_targetvegf'){
					if(dotEle.className.search("avastin")!==-1 || dotEle.className.search("vegf")!==-1 || dotEle.className.search("recept")!==-1){
						util.removeClass(dotEle,"green-dot");
					}
				}
			}
			if(app.slideshow.current === 'FT_targetvegf'){
				dotEle = dotEle.className;
				var allBubbles = app.uguide.ele.querySelectorAll('.bubblebg'),
					avastinDot = app.uguide.ele.querySelector('.ug-avastinbutton'),
					receptDot = app.uguide.ele.querySelector('.ug-receptbutton'),
					vegfDot = app.uguide.ele.querySelector('.ug-vegfbutton');

				if(dotEle.search("avastin")!==-1){
					for (var i = 0; i < allBubbles.length; i++) {
						if(util.hasClass(allBubbles[i],'avastinbubbles')){
							util.removeClass(allBubbles[i],'avastinbubbles');
						}
						else{
							util.removeClass(allBubbles[i],'receptbubbles');
							util.removeClass(allBubbles[i],'vegfbubbles');
							util.addClass(allBubbles[i],'avastinbubbles');
						}
					}
				}
				else if(dotEle.search("vegf")!==-1){
					for (var i = 0; i < allBubbles.length; i++) {
						if(util.hasClass(allBubbles[i],'vegfbubbles')){
							util.removeClass(allBubbles[i],'vegfbubbles');
						}
						else{
							util.removeClass(allBubbles[i],'avastinbubbles');
							util.removeClass(allBubbles[i],'receptbubbles');
							util.addClass(allBubbles[i],'vegfbubbles');
						}
					}
				}
				else if(dotEle.search("recept")!==-1){
					for (var i = 0; i < allBubbles.length; i++) {
						if(util.hasClass(allBubbles[i],'receptbubbles')){
							util.removeClass(allBubbles[i],'receptbubbles');
						}
						else{
							util.removeClass(allBubbles[i],'avastinbubbles');
							util.removeClass(allBubbles[i],'vegfbubbles');
							util.addClass(allBubbles[i],'receptbubbles');
						}
					}
				}
				// hack to make sure bubbles remain visible when same is clicked again
				util.addClass(avastinDot, 'hideBubble');
				util.addClass(receptDot, 'hideBubble');
				util.addClass(vegfDot, 'hideBubble');
				setTimeout(function(){
					util.removeClass(avastinDot, 'hideBubble');
					util.removeClass(receptDot, 'hideBubble');
					util.removeClass(vegfDot, 'hideBubble');
				},0);
			}
		}
	};
})();
