/* Barista Reference Module
 * This module handles both regular references and popup references
 * This module is customized for this presentation
 * */
(function(){
	var d = document;

	// Toggler on clicking outside of reference box
	document.addEventListener('tap', function(){
		if(app.refs.isVisible){
			app.refs.toggleList();
		}
	});

	window.References = function(id, popup){
		var self = this;
		this.version = 'AvastinReferences_v1';
		this.id = id;
		this.ele = app.elements.references = d.getElementById(id);
		this.listElement = d.getElementById('referencelist');
		this.popup = d.getElementById(popup);
		this.popupContent = this.popup.querySelector('.refPopupContent');
		this.closeBtn = this.popup.querySelector('.close-button'); // For refPopups
		this.openPdfLink = this.popup.querySelector('#refOpenOriginal');
		this.addPdfLinktoCart = this.popup.querySelector('#addRefToCart');
		this.current = '';
		this.currentPopup = '';
		this.monitorPopup = '';
		this.previous = '';
		this.previousPopup = '';
		this.markup = '';
		this.isVisible = false;
		this.isPopupVisible = false;
		setTimeout(function(){
			self.init();
		}, 0);
	};

	References.prototype = {
		init:function(){
			var self = this;

			// ugly workaround
			if(this.current === ''){
				this.current = 'IN_intro';
				self.update(this.current);
			}

			document.addEventListener('slideEnter', function(e){
				var current = e.target.id;
				self.update(current);
				if(self.isVisible){
					self.toggleList();
				}
				if(self.isPopupVisible){
					self.hidePopup();
				}
			});
			document.addEventListener('inlineSlideEnter', function(e){
				var current = e.target.id;
				self.update(current);
				if(self.isVisible){
					self.toggleList();
				}
				if(self.isPopupVisible){
					self.hidePopup();
				}
			});
			document.addEventListener('slidePopupLoad', function(e){
				var current = e.target.id;
				self.update(current);
				if(self.isVisible){
					self.toggleList();
				}
				if(self.isPopupVisible){
					self.hidePopup();
				}
			});

			document.addEventListener('slidePopupUnload', function(e){
				var current = app.slideshow.current;
				self.reset(current);
				self.update(current);
			});

			this.ele.addEventListener('tap', function(e){
				self.toggleList();
			});
			/*this.listElement.addEventListener('tap', function(e) {
			 self.openReference(e.target);
			 });*/
			var two = touchy.isTouch ? 'twoFingerTap' : 'dblclick';
			app.elements.presentation.addEventListener(two, function(e){
				// Prevents showpopup to be called twice
				touchy.stop(e);
				self.showPopup();
			});
			if(!touchy.isTouch){
				app.elements.presentation.addEventListener('longTouch', function(e){
					self.showPopup();
				});
			}
			this.addPdfLinktoCart.addEventListener('tap', function(e) {
				self.addPopupRefToCart();
			});
			this.closeBtn.addEventListener('click', function(e){
				self.hidePopup();
			});
			this.openPdfLink.addEventListener('tap', function(e){
				console.log("e target", e.target)
				self.openPopupRef();
			});
		},

		// Reference List methods
		update:function(current){
			this.previous = '';
			this.previousPopup = '';

			if(!app.referencemap[current]){
				app.elements.references.style.display = 'none';
				this.set('');
			}
			else{
				app.elements.references.style.display = 'block';
				this.set(current);
			}
			if(app.popupmap[current]){
				this.setPopup(current);
			}
			else{
				this.setPopup('');
			}
		},
		toggleList:function(){
			var self = this, openList, addList, i;
			if(this.isVisible){
				this.listElement.style.cssText = '-webkit-transform:translate3d(-414px,0,0);';
				this.isVisible = false;
			}else{
				this.listElement.innerHTML = this.buildList();
				this.listElement.style.cssText = '-webkit-transform:translate3d(0,0,0);';
				openList = this.listElement.getElementsByTagName("span");
				addList = this.listElement.getElementsByTagName("button");
				for(i = 0; i < openList.length; i++){
					openList[i].addEventListener('tap', function(e){
						var ele = e.target;
						if (ele.nodeType === 3) {
							ele = ele.parentNode;
						}
						self.openListReference(ele.parentNode);
						self.isVisible = true;
					});

					// checks if buttons exist
					if(addList[i]){
						addList[i].addEventListener('tap', function(e){
							pdfs = sessionStorage.pdfs;
							pdf = e.target.parentNode.getAttribute('data-reference');
							pdfLink = e.target.parentNode.getAttribute('data-PDFlink');
							if (pdfs.indexOf(pdf) == -1) {
								cart.addPdf(pdf);
								cart.addPDFLink(pdfLink);
								util.addClass(e.target.parentNode, 'inCart');
							}else{
								cart.removePdf(pdf);
								cart.removePDFLink(pdfLink);
								util.removeClass(e.target.parentNode, 'inCart');
							}
							self.isVisible = false;
						});
					}
					// make sure ref box can close again on outside box click
					document.addEventListener('tap', function(){
						if(!app.refs.isVisible){
							self.isVisible = true;
							this.removeEventListener('tap', arguments.callee);
						}
					});
				}
				// Adding a timeout so that global event listener get correct state
				setTimeout(function(){
					self.isVisible = true;
				}, 500);
			}
		},
		buildList:function(){
			var list = '';

			//checks if slide is part of case studies and if so builds a list where you cant open refs
			if(this.current.search('PP_cs_') === -1){
				app.referencemap[this.current].forEach(function(reference){
					if(reference.link == undefined || reference.link == null){
						reference.link = '<b>None</b>';
					}

					pdfs = sessionStorage.pdfs;
					if (pdfs.indexOf(reference.pdf) == -1)
	    				list += '<li  data-reference="' + reference.pdf + '" data-PDFlink="' + reference.link + '"><span>' + reference.title + '</span>';
	    			else
	    				list += '<li class="inCart" data-reference="' + reference.pdf + '" data-PDFlink="' + reference.link + '"><span>' + reference.title + '</span>';
					list += '<button class="add-to-shopingcart"></button></li>';
				});
			}
			else{
				app.referencemap[this.current].forEach(function(reference){
	    			list += '<li class="no-cart-button"><span>' + reference.title + '</span></li>';		    			
				});
			}

			return list;
		},
	    openListReference: function(ele) {
			var file = ele.getAttribute('data-reference');
			var path = 'content/pdf/' + file;

			if (file) {
				console.log('Opening file: ' + file);
				openPDF(path);
				submitDocumentOpen(path, file);
			}
	    },
	    openReference: function(ele) {
	      if (ele.nodeType === 3) {
	        ele = ele.parentNode;
	      }
	      var file = ele.getAttribute('data-reference');
	      var link = ele.getAttribute('data-PDFlink');
	      var publications = ele.parentNode.parentNode.getAttribute('data-pub');
	      if (file) {
	        if(publications == "publications"){
	          this.openPub(file,link); 
	        }
	        else{
	          this.open(file,link);  
	          console.log("opening");
	        }
	      }
	    },
	    openPub: function(file,link) {
	      console.log('Opening file: ' + file);
	      if (typeof openPDF !== 'undefined') {
	        util.openPubPDF(file,link);
	      }
	    },
	    open: function(file,link) {
	      // console.log('Opening file: ' + file);
	      if (typeof openPDF !== 'undefined') {
	        util.openPDF(file,link);
	      }
	    },
	    addToCart: function(file,link){
	      console.log('File added to shopping cart: ' + file);
	      cart.addPdf(file);
	      cart.addPDFLink(link);
	    },
		reset:function(name){
			this.current = name || '';
			this.ele.innerHTML = '';
		},
		set:function(name){
			var name = name || app.slideshow.current;
			this.previous = this.current;
			this.current = name;
		},
		unset:function(){
			if(this.previous){
				this.current = this.previous;
			}
			if(this.previousPopup){
				this.currentPopup = this.previousPopup;
			}
			else{
				this.currentPopup = '';
			}
		},

		/* === Reference Popup Methods === */
		// Add each element that was created in createPopupElements to the DOM
		buildPopup:function(){
			var self = this;
			// this.popupElements.forEach(function(element) {
			//   self.popupContent.appendChild(element);
			// });
			this.popupContent.innerHTML = this.markup;
			this.popupElements = this.popupContent.querySelectorAll('.tab');
			this.tabList = this.popupContent.querySelector('ul');
			this.tabList.module = this;
			// Listen to clicks on tabs to navigate content
			this.tabList.addEventListener('tap', this.changeTab);
			// First tab should be active
			util.addClass(this.tabList.firstChild, 'selected');
		},
		// Clean up after closing the popup
		destroyPopup:function(){
			var self = this;
			if(this.changeTab)
				this.tabList.removeEventListener('tap', this.changeTab);
			// this.popupElements.forEach(function(element) {
			//   self.popupContent.removeChild(element);
			// });
			this.popupContent.innerHTML = '';
			// this.popupElements = [];
		},
		// Create markup
		createPopupMarkup:function(popupName){
			var self = this;
			var popupName = popupName || this.currentPopup;
			var tabs = app.popupmap[popupName];
			var nrOfTabs = tabs.length;
			var tabCounter = 0;
			var tabMarkup = '<ul>';
			this.markup = '';
			this.pdfList = [];
			this.linkList = [];
			this.currentTab = 0;
			this.monitorPopup = popupName;

			tabs.forEach(function(tab, index){
				var tabClass = (index === 0 ? 'tab active' : 'tab');
				self.markup += '<div class="' + tabClass + '">';
				self.markup += '<p class="ref-summary">' + tabs[index].description + '</p>';
				self.markup += '<img src="' + tabs[index].image + '"/>';
				self.markup += '</div>';
				tabMarkup += '<li data-tab="' + index + '">Ref-' + (index + 1) + '</li>';
				self.pdfList.push(tabs[index].reference.pdf);
				self.linkList.push(tabs[index].reference.link);
			});
			tabMarkup += '</ul>';
			this.markup += tabMarkup;
			this.buildPopup();

			this.openPdfLink.setAttribute('data-reference', this.pdfList[0]);
			this.openPdfLink.setAttribute('data-pdflink', this.linkList[0]);

			setTimeout(function(){
				app.data.saveCustomSlide(popupName+'_ref_tab0');
			},500);
		},
		// Create the actual html elements that is unique per popup
		createPopupElements:function(popupName){
			var self = this;
			var popupName = popupName || this.currentPopup;
			var tabs = app.popupmap[popupName];
			var nrOfTabs = tabs.length;
			var tabCounter = 0;
			var tabMarkup = '';
			this.popupElements = [];
			this.tabList = document.createElement('ul');
			this.pdfList = [];
			this.linkList = [];

			// Build the tabs using the data in app.popupmap
			tabs.forEach(function(tab){
				var tabEle = document.createElement('div');
				var desc = document.createElement('p');
				var img = document.createElement('img');
				tabEle.className = (tabCounter === 0 ? 'tab active' : 'tab');
				desc.className = 'ref-summary';
				desc.innerHTML = tabs[tabCounter].description;
				img.src = tabs[tabCounter].image;
				// Add PDF file name to list for access when opening
				self.pdfList.push(tabs[tabCounter].reference.pdf);
				self.linkList.push(tabs[tabCounter].reference.link);
				tabEle.appendChild(desc);
				tabEle.appendChild(img);
				self.popupElements.push(tabEle);

				tabCounter++;
				tabMarkup += '<li data-tab="' + (tabCounter - 1) + '">Ref-' + tabCounter + '</li>';
			});
			this.tabList.innerHTML = tabMarkup;
			this.popupElements.push(this.tabList);
			this.buildPopup();
			// Listen to clicks on tabs to navigate content
			this.tabList.addEventListener('tap', this.changeTab);
			// First tab should be active
			util.addClass(this.tabList.firstChild, 'selected');
			this.currentTab = 0;
			// Add a reference to this object from the tabList
			this.tabList.module = this;
			this.openPdfLink.setAttribute('data-reference', this.pdfList[0]);
			this.openPdfLink.setAttribute('data-pdflink', this.linkList[0]);
		},
		changeTab:function(e){
			var self = this.module; // Access to the reference module instance
			var ele = e.target;
			var tabNr;
			if(ele.nodeType === 3){
				ele = ele.parentNode;
			}
			tabNr = ele.getAttribute('data-tab');
			if(tabNr && tabNr !== self.currentTab){
				self.monitorPopup = self.currentPopup;
				app.data.saveCustomSlide(self.currentPopup+'_ref_tab'+tabNr);
				util.removeClass(self.popupElements[self.currentTab], 'active');
				util.addClass(self.popupElements[tabNr], 'active');
				util.addClass(ele, 'selected');
				util.removeClass(self.tabList.childNodes[self.currentTab], 'selected');
				// Update PDF file name data attribute
				self.openPdfLink.setAttribute('data-reference', self.pdfList[tabNr]);
				self.openPdfLink.setAttribute('data-pdflink', self.linkList[tabNr]);
				self.currentTab = tabNr;
			}
		},
		openPopupRef:function(e){
			var reference = this.openPdfLink.getAttribute('data-reference');
			var path = 'content/pdf/' + reference;
			console.log('Opening file: ' + reference);
			openPDF(path);
			submitDocumentOpen(path, reference);
		},
	    addPopupRefToCart: function(e) {
	      var reference = this.openPdfLink.getAttribute('data-reference');
	      var link = this.openPdfLink.getAttribute('data-pdflink');
	      this.addToCart(reference, link);
	    },
		setPopup:function(name){
			this.previousPopup = this.currentPopup;
			this.currentPopup = name;
		},
		hidePopup:function(){
			if(this.monitorPopup){
				console.log("Closing popup");
				// Custom renaming to make sure the correct slide is entered when exiting refpopup
				if(this.monitorPopup.search('_chart') !== -1 || this.monitorPopup.search('_men') !== -1){
					this.monitorPopup = this.monitorPopup.replace('_chart','');
					this.monitorPopup = this.monitorPopup.replace('_men','');
				}
				// Custom subslidefinding on overall slide to make sure which slide is suppose to be logged as enter when refpop is closed
				else if(this.monitorPopup === 'EF_overall'){
					var activeSubSlide = document.querySelector('.graphbox.active');
					this.monitorPopup = activeSubSlide.getAttribute('data-subslide');
				}

				app.data.saveCustomSlide(this.monitorPopup);
				this.isPopupVisible = false;
				this.destroyPopup();
				util.removeClass(this.popup, 'displaying');
				this.monitorPopup = '';
			}
		},
		showPopup:function(){
			this.isPopupVisible = true;
			if(this.currentPopup !== ''){
				this.createPopupMarkup();
				util.addClass(this.popup, 'displaying');

			}
		}
	};
})();
