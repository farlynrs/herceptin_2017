/**
* AMP MODULE - Basic Thumbs
 * This is a very basic slide thumb menu that automatically
 * links to all your slides.
 * If using images, place them in content/img/thumbs
 * @author - Stefan Liden, stefan.liden@gmail.com
 */

(function() {

  window.BasicThumbs = function(id, slideshow, hasImages, rating) {
    this.version = 'AvastinBasicThumbs_v2';
    this.id = id;
    this.rating = rating || false;
    this.ele = app.elements.thumbs = document.getElementById(id);
    this.wrapper = document.getElementById('thumbswrapper');
    this.footer = document.getElementById('mainfooter');
    this.slideshow = window[slideshow];
    this.hasImages = hasImages || false;
    this.ssName = slideshow;
    this.initialized = false;
    this._init();

  };

  BasicThumbs.prototype = {
    _init: function() {
      var self = this;
      document.addEventListener('slideshowLoad', function() {
        if (app.slideshow.id === self.ssName) {
          if (self.initialized) {
            self._insert();
            app.thumbScroller.enable();
          }
          else {
            self.slideshow = app.slideshows[self.ssName];
            self._build();
            self._insert();
            self._connect();
            if(self.rating)
              self._rate();
            self.initialized = true;
            t = document.getElementById('thumbs');
            if(t) i = t.getElementsByTagName('img')[0];
            if(i) all_thumbs_width = (i.offsetWidth + 22) * app.slideshow.length

            if((all_thumbs_width/1) > 1024){
              app.thumb_width = i.offsetWidth + 22;

              width = 0-(Math.abs(app.slideshow.length-3))*app.thumb_width;
              app.thumbScroller = new Draggy('thumbs', {restrictY: true, limitsX: [width, 0]});
            }else{
              if(app.thumbScroller)
                {
                 app.thumbScroller = new Draggy('thumbs', {restrictY: true, limitsX: true});
                 app.thumbScroller.reset();
                }
            }
          }
        }
      });

      document.addEventListener('slideshowUnload', function() {
        if (app.slideshow.id === self.ssName) {
          self._remove();
          self.initialized = false;
        }
      }, false);
      // Listening to 'slideEnter' to set selected item
      // 'slideEnter is dispatched from slideshow._scroll
      document.addEventListener('slideEnter', function() {
        if (app.slideshow.id === self.ssName) {
          setTimeout(function() {
            self._setCurrent();
          },0);
        }
      }, false);
    },
    // Create the markup to be inserted for the thumbs
    _build: function() {
      var self = this,
          markup = '<ul class="basicthumbs">';

      // Create the button that will show the thumbnails
      this.btn = document.createElement('div');
      this.btn.setAttribute('class', 'thumb-btn');
      this.thumbBar = document.createElement('div');
      this.thumbBar.setAttribute('class', 'thumb-bar');
      this.thumbArrow = document.createElement('div');
      this.thumbArrow.setAttribute('class', 'thumb-arrow');

      // Create the actual thumbnail markup
      this.slideshow.content.forEach(function(slide) {
        var name, thumb;
        if (self.hasImages) {
          thumb = '<img src="content/img/thumbs/'+slide+'.jpg" data-slide="' + slide + '" alt="'+slide+'" />';
        // If rating is set as true, include rating content.
        if (self.rating)
            thumb += '<div class="ranking"><p>0</p><span class="ranker" data-rank="' + slide + '"></span><div class="empty"></div><div class="star"></div><div class="star"></div><div class="star"></div><div class="star"></div><div class="star"></div></div>';
        }
        else {
          thumb = '<div class="thumbindicator" data-slide="' + slide + '"></div>';
        }
        markup += '<li>'+thumb+'</li>';
      });
      markup += '</ul>';
      this.markup = markup;
    },
    
    _insert: function() {
      this.ele.innerHTML = this.markup;
      this.footer.appendChild(this.btn);
      this.footer.appendChild(this.thumbBar);
      this.footer.appendChild(this.thumbArrow);
    },

    _remove: function() {
      this.ele.innerHTML = '';
      this.footer.removeChild(this.btn);
      this.footer.removeChild(this.thumbBar);
      this.footer.removeChild(this.thumbArrow);
      app.scroller.enableAll();
      this.wrapper.setAttribute('class', '');
    },    

    // Connect the thumbs to the slideshow.scrollTo method
    _connect: function() {
      var self = this;
      // Clicking on the thumb button will use CSS class to display thumbs
      this.btn.addEventListener('click', function(event) {
        //Checks if the current slide is slide nr4 or more in the current section and if so make sure it shows up as the middle slide.
        if(app.slideshow.currentIndex > 2){
          var centralPos = app.thumb_width * (app.slideshow.currentIndex-2);
          centralPos = centralPos - (centralPos*2);
          app.thumbScroller.moveTo(centralPos,0);
        }

        self.wrapper.setAttribute('class', 'active');
        ref = document.getElementById('references')
        if(ref) ref.style.zIndex = 0;
        if (app.thumbScroller) app.thumbScroller.enable();  
        //app.scroller.disableAll();

        // if you click anywhere above the thumbs they will close 
        app.loaded.ele.addEventListener('click', function(event) {
          var ele = event.target;
          if(ele.className !== "thumb-btn"){
            self.wrapper.setAttribute('class', '');
            ref = document.getElementById('references')
            if(ref) ref.style.zIndex = 3;
            if(app.thumbScroller) app.thumbScroller.reset();

            // Make sure that scroller isnt enabled in the PP slides
            if(app.loaded.current !== "PP_intro" && app.loaded.current !== "PP_casestudies"){
              app.scroller.enableAll();
            }

            this.removeEventListener('click', arguments.callee);
          }
        });
      });

      // this.ele.parentNode is the thumbswrapper
      this.ele.parentNode.addEventListener('click', function(event) {
        var ele = event.target;
        var slide = ele.getAttribute('data-slide');
        if(slide){
          app.slideshow.scrollTo(slide);
        }
        else if(ele.className === 'star' || ele.className === 'ranker' || ele.tagName ==='P' || ele.className ==='s_active'){
          return;
        }
        
        self.wrapper.setAttribute('class', '');
        ref = document.getElementById('references')
        if(ref) ref.style.zIndex = 3;
        if(app.thumbScroller) app.thumbScroller.reset();

        // Make sure that scroller isnt enabled in the PP slides
        if(app.loaded.current !== "PP_intro" && app.loaded.current !== "PP_casestudies"){
          app.scroller.enableAll();
        }
      });
      self._setRates();
      document.addEventListener('doubleTap', self._monitorRankedSlides);
    },
    _rate: function() {
      var self = this;
      
       this.ele.addEventListener('click', function(event) {
        var e = event.target;
        if(e.tagName === 'DIV' && util.hasClass(e, 'ranking'))
          e = e
        else if(e.tagName ==='P')
          e = e.parentElement
        else if(e.tagName ==='SPAN')
          e = e.parentElement
        else if(e.tagName === 'DIV' && util.hasClass(e, 'star'))
          e = e.parentElement
        else
          e = null

        if (e != null && e != 'undefined' && util.hasClass(e, 'ranking')){
          if(!util.hasClass(e, 'r_active'))
          {
            if(app.thumbScroller) app.thumbScroller.disable();  
            util.addClass(e, 'r_active');
            // listener to close rank when another rank is clicked
            document.addEventListener('tap', function(){
              util.removeClass(e, 'r_active');
              this.removeEventListener('tap', arguments.callee);
            });
          }

          var rank = e.getAttribute('data-rank');
          if(e)
          { 
            //app.scroller.disableAll();
            self.wrapper.setAttribute('class', 'active');
            self._rateSlide(e)
            }
          }
      }, false);
         
      
    },
    // Called on 'slideEnter'
    _setCurrent: function() {
      var prev = this.ele.querySelector('.selected'),
          slide = this.slideshow.getIndex() + 1,
          link = this.ele.querySelector('li:nth-child('+slide+')');
      if (prev) { prev.setAttribute('class', ''); }
      link.setAttribute('class', 'selected');
    },
    _rateSlide: function(ele){
      self = this;
      var rates = ["0","1","2","3","4","5"];
      rater = ele.getElementsByTagName('span')[0];

      app.rater = new Draggy(rater, {restrictX: true, limitsY: [0, 200], onChange:function(x,y){
            limit = y/15;
            stars = ele.getElementsByTagName('div');
            num = ele.getElementsByTagName('p')[0];
            self.finalNumber = 0;
            stars.forEach(
              function(item){
                if (item && util.hasClass(item, 's_active') )
                  util.removeClass(item, 's_active');  
            });
            for(i=0;i<Math.round(limit); i++)
            {
              if(i < rates.length)
              {
                num.innerHTML = i;
                self.finalNumber = i;
                if(stars[i])
                  util.addClass(stars[i], "s_active");
              }
            }
          }
        });
      document.addEventListener('onDrop', function(){
            if (app.rater) {
              app.rater.reset();
            }
            if (util.hasClass(ele,'r_active'))
              if(app.thumbScroller) app.thumbScroller.enable();
              setTimeout(
                function(){
                  util.removeClass(ele, 'r_active')
                }
                ,200);
              if(self.finalNumber !== 0){
                self._rankSlide(rater, self.finalNumber);
              }
              self.finalNumber = 0;
        }, false); 
    },
    _rankSlide: function(id,value){      
      var self = this,
          currentId = id.getAttribute('data-rank'),
          rankedSlides = sessionStorage.rankedSlides;

      if (rankedSlides.indexOf(currentId) === -1) {
        rankedSlides += currentId + '; ' + value + '% ';
      }
      else{
        var index = rankedSlides.indexOf(currentId) + currentId.length+2;
        rankedSlides = self._setCharAt(rankedSlides, index, value);
      }

      sessionStorage.rankedSlides = rankedSlides;
    },
    _setRates: function(){
      var self = this;
      if (!sessionStorage.rankedSlides) { sessionStorage.rankedSlides = ''; }

      var rankedSlides = sessionStorage.rankedSlides;
      var allRankable = document.querySelectorAll('.ranker');
      var rankId, rankValue, index;

      for (var i = 0; i < allRankable.length; i++) {
        rankId = allRankable[i].getAttribute('data-rank');
        if(rankedSlides.indexOf(rankId) !== -1){          
          index = rankedSlides.indexOf(rankId) + rankId.length+2;
          rankValue = rankedSlides[index];
          self._setStars(allRankable[i].parentNode,rankValue);
        }
      }
    },
    _monitorRankedSlides: function(){
      if (!sessionStorage.rankedSlides) { sessionStorage.rankedSlides = ''; }
      var rankedSlides = sessionStorage.rankedSlides.split('% '),
          currentSlide,
          rankedOne = '',
          rankedTwo = '',
          rankedThree = '',
          rankedFour = '',
          rankedFive = '';

      for (var i = 0; i < rankedSlides.length; i++) {
        if(rankedSlides[i]){
          currentSlide = rankedSlides[i].split('; ');

          var monitorSlideName = currentSlide[0];

          switch(currentSlide[1]) {
            case '1':
              rankedOne += window.monitormap.slides[monitorSlideName] + '; ';
              break;
            case '2':
              rankedTwo += window.monitormap.slides[monitorSlideName] + '; ';
              break;
            case '3':
              rankedThree += window.monitormap.slides[monitorSlideName] + '; ';
              break;
            case '4':
              rankedFour += window.monitormap.slides[monitorSlideName] + '; ';
              break;
            case '5':
              rankedFive += window.monitormap.slides[monitorSlideName] + '; ';
              break;
          }
        }
      };

      submitUniqueCustomEvent('1015 - Page ranking', '1015001 - 1 star pages', rankedOne);
      submitUniqueCustomEvent('1015 - Page ranking', '1015002 - 2 stars pages', rankedTwo);
      submitUniqueCustomEvent('1015 - Page ranking', '1015003 - 3 stars pages', rankedThree);
      submitUniqueCustomEvent('1015 - Page ranking', '1015004 - 4 stars pages', rankedFour);
      submitUniqueCustomEvent('1015 - Page ranking', '1015005 - 5 stars pages', rankedFive);
    },
    _setStars: function(ele, slideValue){
      var number = ele.querySelector('p');
      var stars = ele.querySelectorAll('div');

      number.innerHTML = slideValue;
      for (var i = 0; i <= slideValue; i++) {
        util.addClass(stars[i],'s_active');
      }
    },
    _setCharAt: function(str,index,chr) {
      if(index > str.length-1) return str;
      return str.substr(0,index) + chr + str.substr(index+1);
    },
    _createTitle: function(slide) {
      // TODO: replace _-. with a space
      return slide[0].toUpperCase() + slide.slice(1);
    }
  };

})();
