document.addEventListener('presentationInit', function () {
	var slide = app.slide.showplaylist = {

		dynamicTemplate:null,

		onEnter:function (ele) {

			//Add click event to the slide
			this.init();

		},
		//onExit is a FrameWork method used when exit slide.
		onExit:function (ele) {

		},
		init:function () {

			this.buildMenu();

			// var returnbutton = document.getElementById("home");
			// returnbutton.addEventListener("click", this.returnToHome, false);
//						
			var playButton = document.getElementById("playbutton");
			playButton.addEventListener("click", this.buttonSelect, false);
			$("#playbutton").addClass("buttonselected");

			var editbutton = document.getElementById("editbutton");
			editbutton.addEventListener("click", this.buttonSelect, false);

			var deletebutton = document.getElementById("deletebutton");
			deletebutton.addEventListener("click", this.buttonSelect, false);

			this.dynamicTemplate = new DynamicAgendaTemplate({
				dynamicContentMap		:null,
				createMode				:false,
				name					:"PLACEBO"		
			});

			this.dynamicTemplate.dynamicAgenda.updateApp();
			this.dynamicTemplate.getSavedSections("myList");
			var up = document.getElementById("arrrowUp");
			var down = document.getElementById("arrowDown");

			var list = document.getElementById('list'),
				offset = list.parentNode.offsetHeight,
				curOffset = 0;
			up.addEventListener("click", function(){
				curOffset -= offset;
				if (curOffset < 0){
					curOffset = 0;
				}
				list.style.webkitTransform = 'translate3d(0,-'+curOffset+'px,0)';
			});
			down.addEventListener("click", function(){
				curOffset += offset;
				if (curOffset >= (list.offsetHeight-20)){
					curOffset -= offset;
				}
				list.style.webkitTransform = 'translate3d(0,-'+curOffset+'px,0)';
			});
		},
		returnToHome:function (e) {
			console.log('clickkked');
			DynamicAgendaTemplate.dynamicAgendaName.reset();
			DynamicAgendaTemplate.dynamicAgendaName = {};
			app.init("start");
		},
		buttonSelect:function (e) {

			var parentNode = e.target.parentNode;
			var header = "";

			//first we remove all buttonSelect class and we dont want to change homebutton
			//So we start from 1 and not from 0 in the for statment.
			for (var i = 1; i < parentNode.childNodes.length; i++) {
				$("#" + parentNode.childNodes[i].id).removeClass("buttonselected");
			}

			//Add the class so color can change.
			$("#" + e.target.id).addClass("buttonselected");

			switch (e.target.id.toLowerCase()) {
				case "playbutton":
					header = "My Presentations";
					break;
				case "editbutton":
					header = "Edit Presentations";
					break;
				case "deletebutton":
					header = "Remove Presentations";
					break;
				default:
					break;
			}
			$(".agendatitleH1").text(header);
		},
		buildMenu:function () {
			$("#showplaylistMenu").addClass("menuholder");
		}
	};
});
