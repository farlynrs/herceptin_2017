document.addEventListener('presentationInit', function() {

var slide = app.slide.builder = {
			dynamicTemplate: null,
  			onEnter: function(ele) {
					
					//Add click event to the slide
					this.init();
					
				},
				//onExit is a FrameWork method used when exit slide.
				onExit: function(ele) {
						
				},
				init: function (){
						
						this.buildMenu();
						
						var addbutton = document.getElementById("addslides");
							addbutton.addEventListener('click', this.addslides, false);
												
						var popupSaveButton = document.getElementById("saveallbutton");
							popupSaveButton.addEventListener("click", this.popupSaveButtonEvent, false);
						
						var cancelbutton = document.getElementById("cancelsave");
							cancelbutton.addEventListener('click', this.cancleSave, false);	

						var playbutton = document.getElementById("play");
							playbutton.addEventListener("click", this.playPresentation, false);
						
						$("#save").addClass("disabled");
						$("#play").addClass("disabled");
						
						//app.dynamicChapters.create();
				           
				      	var listAttr = {
					 	    				"id":"id",
					 	    				"idValue":"chapterlist",
					 	    				"className":"class",
					 	    				"classValue":"chapterlist"
					 	    			};
					 	   slide.dynamicTemplate = new DynamicAgendaTemplate({
					 	   							dynamicContentMap : null,
					 	   							createMode			: true,
					 	   							name				:"PLACEBO"
					 	   						});
					 	   						 			
					 	    this.dynamicTemplate.addSections("chapters",listAttr);			
					 	
					 	   
 
				},
				addslides: function(e){

					var attr =  {
									"className": "chaptertitle",
									"id"	: ""
								};
					
					
								
					var res = slide.dynamicTemplate.addSectionsWithSlides("chapters", attr);			
										
					if(res == 0)
					{		$(".agendatitle").html("<h1>Select slides</h1>");
							$("#"+e.target.id).addClass("disabled");
							$("#save").removeClass("disabled");
							saveButton = document.getElementById("save");
							saveButton.addEventListener('click', slide.saveCustomPresentation, false);
					}
					else {
							
							errorstr = 'Save a chapter before adding slides';
							slide.showMessageDialog(errorstr);
						
					}
	
				},
				saveCustomPresentation: function(e){
				
				if(!slide.dynamicTemplate.getSlidesChecked())
				{
					
					errorstr = 'Choose slides before saving';
					slide.showMessageDialog(errorstr);
					
				}else {
					$("#savewindow").removeClass("hide");
					$("#savewindow").addClass("show");
				}
				
				
				
				},
				returnToHome: function(e){
						app.init("start");
				},
				popupSaveButtonEvent: function(e){
					var presId =  document.getElementById("title").value;
					
					if(presId === "")
					{
						
					}
					else {
						slide.dynamicTemplate.saveSectionSlides(presId);
						
						$("#savewindow").removeClass("show");
						$("#play").removeClass("disabled");
						$("#save").addClass("disabled");
						
					}
											
				},
				cancleSave: function(){
					
					$("#savewindow").removeClass("show");
					$("#savewindow").addClass("hide");
				},
				playPresentation:function(e)
				{
					var dyAg = slide.dynamicTemplate.dynamicAgenda;
					$("#mainmenu").removeClass("menuholder");
					var section = dyAg.data[dyAg.current].name;
					slide.dynamicTemplate.playPresentation(section);
					
				},
				buildMenu: function(){
					$("#buildnewMenu").addClass("menuholder");			
					$("#buildnewMenu").removeClass("activeDrag");
				},
				showMessageDialog: function(errorstr){
					document.getElementById('errorwindow').addEventListener('click', this.closeerror, false);
					
					$("#errorwindow").addClass('show');
					document.getElementById('errordialog').innerHTML = '<h1>'+errorstr+'</h1>';
				},
				closeerror: function() {
				     $("#errorwindow").removeClass('show');
				     document.getElementById('errorwindow').childNodes[0].innerHTML = '';
				},

			};
			
});
