document.addEventListener('presentationInit', function() {
	var slide = app.slide.start_index = {
		onEnter: function(ele) {
				
				//Add click event to the slide
				this.init();
		
			},
			//onExit is a FrameWork method used when exit slide.
		onExit: function(ele) {
			
		},
		init: function() {	 	  
			       var play = document.getElementById('play');
			       var select = document.getElementById('select');
			       var show = document.getElementById('showplaylist');
			       var build = document.getElementById('buildnew');
			       
			       play.addEventListener('click', app.slide.start_index.startFullPresentation, false);
			       select.addEventListener('click', app.slide.start_index.showObjectives, false);
			       show.addEventListener('click', app.slide.start_index.openList, false);
			       build.addEventListener('click', app.slide.start_index.openBuilder, false);
		},
		openList: function(e) {
			app.whitePopup.show('showplaylist');
		},
		openBuilder: function(e) {
			app.whitePopup.show('builder');
		},
		openpage: function(e){
			    		
			    		 			    		 
			    		 	app.goTo(e.srcElement.id);
		},
		startFullPresentation: function(e){
			app.goTo('placebo', 'introduction');			
		},
		showObjectives: function(e) {
			app.whitePopup.show('select_objectives');
		} 
			
	};

});   
 
 