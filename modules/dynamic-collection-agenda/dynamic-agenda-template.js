///Template for DynmaicAgenda presentation.
///Author Agnitio A/S
///2012-01
///

(function() {
	window.DynamicAgendaTemplate = function(config) {
	
		delete app.dynamicChapters;
		this.dynamicContentMap	= config.dynamicContentMap;
		this.dynamicAgenda		= new DynamicAgenda(config.name);
		this.order				= 0;
		this.createMode			= config.createMode;
		app.dynamicChapters		= this.dynamicAgenda;
		this.version			= "v1.0";
		
		if(this.createMode)
		{
		
			this.dynamicAgenda.create();
			this.dynamicContentMap = this.dynamicAgenda.current;
		}
	
	};
})();

DynamicAgendaTemplate.prototype = {
			
			addSections: function(divId, listAttr, section){
				var xDA = DynamicAgendaTemplate.prototype;
				var sections = "";
				var listitem = "";
				var list = document.createElement("ul");
				var chapterIsSaved = false;
				
				
				sections = app.collections.buildchapters.content;

				list.setAttribute(listAttr.id,listAttr.idValue);
				list.setAttribute(listAttr.className,listAttr.classValue);
				
				document.getElementById(divId).appendChild(list);
				
				for(var i=0; i<sections.length; i++)
				{
					 listitem = document.createElement('li');
		 	         listitem.innerHTML = sections[i].replace(/_/gi, ' ').toUpperCase() ;
		 	         
		 	         listitem.setAttribute('id', "list_"+sections[i]);
		 	         listitem.sectioncode = sections[i];
		 	    	 listitem.dynamicAgendaContentMap = this.dynamicContentMap;
		 	         list.appendChild(listitem);
		 	         
		 	         if(section)
		 	         {
			 	         for(var y=0; y<section.sections.length; y++)
			 	         {
			 	         	var sectionName = section.sections[y].replace(section.name+"_", "");
			 	         	if(sections[i] == sectionName)
			 	         	{
			 	         	
			 	         		chapterIsSaved = true;
			 	         	}
			 	         	
			 	         }
			 	     }
			 	    
			 	    if(!chapterIsSaved)
			 	    {
			 	     		listitem.addEventListener('click', xDA.sortarray, false);
			 	    }
			 	    else{
			 	    		listitem.addEventListener('click', xDA.sortreset, false);
			 	    }
			 	     
		 	         		
		 	      		
					
				}									
			},
			/// <summary>
			///	Event for adding sections
			/// </summary>
			/// <param name="e">Element div target</param>		
			sortarray: function(e){
			
			$(e.target).addClass("active");			
					var xDAT			= DynamicAgendaTemplate.prototype;
					var parentNode		= e.target.parentNode;
					var dyAgenda		= app.dynamicChapters;
					var order			= app.dynamicChapters.data[app.dynamicChapters.current].sections.length;
					var idContent		= dyAgenda.contentMap[$(".agendatitleH1").text()];
					var sections 		= dyAgenda.data[dyAgenda.current].sections;
					var order			= sections.length;
					
					dyAgenda.addSection(e.target.sectioncode);
					
			
				     
				   	parentNode.removeChild(e.target);
				    parentNode.insertBefore(e.target, parentNode.childNodes[order]); 
				      
				      e.target.removeEventListener('click', xDAT.sortarray, false);
				      e.target.addEventListener('click', xDAT.sortreset, false);   
			
			},
			/// <summary>
			///	Event for removing section
			/// </summary>
			/// <param name="e">Element div target</param>	
			sortreset: function(e){
						 var parentNode		= e.target.parentNode;
						 var dynamicAgenda	= app.dynamicChapters;
						 
						 dynamicAgenda.removeSection(e.target.sectioncode);
					     e.target.style.display = 'none';
					     
					     parentNode.removeChild(e.target);
					     parentNode.appendChild(e.target); 
				
					     e.target.style.cssText = '-webkit-transform:translate3d(0px,'+ (-e.target.offsetTop) +'px,0px);';	
					   
					      DynamicAgendaTemplate.order--;
					      $("#" +e.target.id).removeClass("active");
					    
					      e.target.addEventListener('click', DynamicAgendaTemplate.prototype.sortarray, false);
					      e.target.removeEventListener('click', DynamicAgendaTemplate.prototype.sortreset, false);    
				
			},
			/// <summary>
			///	builds a list element section with all it´s slides and marks the checked ones.
			/// </summary>
			/// <param name="elemId">Div container for the section list</param>
			/// <param name="newSectionAttr">Object containing attributes for the elemId</param>
			/// <returns>boolean true/false</returns>
			addSectionsWithSlides: function(elemId, newSectionAttr){
						
						var dyAg = this.dynamicAgenda;
						var sectionsChecked = [];
						var wrapper = "";
						var slides = [];
						var slideswrapper = document.getElementById("slidethumbs1");
						var slidelistitem;
						var checkvar = 'checkbox_placebo';
						
						
						if(!dyAg){
							
							console.log("dynamicAgendaName is not mapped");
							return 0;
						}
						
						sectionsChecked = dyAg.getSections();
						
						if(sectionsChecked.length == 0){	
							 						
							return -1;
						}
						wrapper = document.getElementById(elemId);	
					    wrapper.removeChild(wrapper.childNodes[0]);
					      
					      this.buildSectionSlides(sectionsChecked,slideswrapper, newSectionAttr,checkvar);
					      
					     				      
					      $("#chapters").removeClass("unsorted");
					      $("#chapters").addClass("sorted");
					      $("#slidethumbs1").addClass("sorted");
	
					return 0;
				
			},
			/// <summary>
			///	Builds the slection page for adding slides for section.
			/// </summary>
			/// <param name="sectionsChecked">Object list of marked sections/collections for the custom presentation</param>
			/// <param name="slideswrapper">Div container for showing the selected sections</param>
			/// <param name="newSectionAttr">Object contaning div attibutes. class and id dictionary</param>
			/// <returns></returns>
			buildSectionSlides: function(sectionsChecked, slideswrapper, newSectionAttr){
			
				var childSection;
				var checkvar = 'checkbox_placebo';
				var dyAg = app.dynamicChapters;
				
//				check if the user has choosen more than 4 slides so we can add a slide event,
//				else the page would see vary strange.
				this.checkSectionCounts(sectionsChecked, slideswrapper);
				
				 for(var i = 0; i<sectionsChecked.length; i++) {
						var idSection = sectionsChecked[i].split(dyAg.current + "_")[1];
								          slides = app.json.structures[idSection];
								          
								          var newlist = document.createElement('ul');
								     	  
								     	  var newLi = document.createElement('li');	
								     	  	newLi.setAttribute("id", "list_"+ idSection);
								     	  	newLi.setAttribute("class",newSectionAttr.className);
								     	  	newLi.innerHTML = idSection.toUpperCase().replace(/_/gi, ' ');

								          newlist.appendChild(newLi);
								          slideswrapper.appendChild(newlist);
								           
								          
								            for (var j = 0; j<slides.content.length; j++) {
								           			 
								           			
								                    
								                    slidelistitem = document.createElement('li');
				
								                    slidelistitem.style.cssText = 'z-index:-1;background: url(content/img/agenda/'+checkvar+'.png) no-repeat 14px  0px, url(content/img/thumbs/'+slides.content[j]+'.jpg)  no-repeat 15px  5px;';
								                    slidelistitem.slidethumb = slides.content[j];
								                    slidelistitem.setAttribute('id', slides.content[j] +'_slide');
								                    slidelistitem.setAttribute('class', 'slideitem');
								                    slidelistitem.slide = slides.content[j];
								                    slidelistitem.section = idSection;
								                    slidelistitem.clickstate = 0;
								                    slidelistitem.slidesorder = 0;
								                   
								                    slidelistitem.addEventListener('click', this.sortslidesarray, false);
								                    
								                     
								                    newlist.appendChild(slidelistitem);
								                    
								                    				            }
								      }
				
			
			},
			/// <summary>
			///	If these is more than 4 sections chosen we extend the page for right swipe.
			/// </summary>
			/// <param name="section">Object list of marked sections/collections for the custom presentation</param>
			/// <param name="slideswrapper">Div container for showing the selected sections</param>
			/// <returns></returns>
			checkSectionCounts: function(section, slideswrapper){
				
				if (section.length > 4) {
				      slideswrapper.style.width = '2048px';
				      
				      document.addEventListener('swipeleft', this.scrollleft); 
				      document.addEventListener('swiperight', this.scrollright);
				}
			},
			/// <summary>
			///	Events for swipe left the page. Adds a webkit to the div container scroller.
			/// </summary>
			/// <returns></returns>
			scrollleft: function(){
			    document.getElementById('scroller').style.cssText = '-webkit-transform:translate3d(-1024px,0px,0px);';
			  
			 },
			 /// <summary>
	 		 ///	Events for swipe right the page. Adds a webkit to the div container scroller.
	 		 /// </summary>
	 		 /// <returns></returns>
	 		scrollright: function(){
	 		    document.getElementById('scroller').style.cssText = '-webkit-transform:translate3d(0px,0px,0px);';
	 		  
	 		 },
	 		sortslidesarray: function(e){
	 					
	 				var dyAg = app.dynamicChapters;
				 	var listNode =  e.target.parentNode;
				 	var xDAT	= DynamicAgendaTemplate.prototype;
				 	var checkvar = 'checkbox_green';
				 	var targetID  = dyAg.current+"_"+e.target.section;  
	 				 	dyAg.addSlide(e.target.section,e.target.slide);

	 				 	e.target.style.cssText = 'background: url(content/img/agenda/'+checkvar+'.png) no-repeat 14px  0px, url(content/img/thumbs/'+ e.target.slidethumb+'.jpg)  no-repeat 15px  5px;';
	 				 	
	 		
	 			 	var index = dyAg.data[dyAg.current].slides[targetID];
	 				var newPlace = listNode.childNodes[index.length];
	 				var removeChild = xDAT.checkSortElements(e.target,index);	
	 				
	 						
	 						
	 			 	      if(removeChild)
	 			 	      {
	 			 	      	listNode.removeChild(e.target);
	 			 	      	listNode.insertBefore(e.target, newPlace); 
	 			 	      }
	 
	 			 	   	  
	 			 	      e.target.removeEventListener('click', xDAT.sortslidesarray, false);
	 			 	      e.target.addEventListener('click', xDAT.sortslidesreset, false); 
	 			 	  
	 			       
	 			
	 		},
			checkSortElements: function(elem, checkedContainer){
			
					var removeChild = true;
					var indexOfSildes = checkedContainer.length;
					var listNode = elem.parentNode;
					
					
					var checkFirstNode = listNode.childNodes[1].id == elem.id ? true: false;
					
					var checkIfLastNode = elem.nextSibling == null && checkFirstNode==false && (indexOfSildes != 1) && (listNode.childNodes.length-1) - indexOfSildes != 1 ? true: false;
					var checkIfSecondLast = elem.nextSibling != null && elem.nextSibling.nextSibling == null && indexOfSildes != 1 && (listNode.childNodes.length-1) - indexOfSildes == 1 ? true: false;
					
					
					if(checkFirstNode || checkIfLastNode || checkIfSecondLast )
					{
						removeChild = false;
					}
						
					return removeChild
			},
			sortslidesreset: function(e){
					var xDAT	= DynamicAgendaTemplate.prototype;
					var dyAg = app.dynamicChapters;
				 	dyAg.removeSlide(e.target.section,e.target.slide);
				 	var checkvar = 'checkbox_placebo';
				 	
			      	e.target.style.cssText = 'background: url(content/img/agenda/'+checkvar+'.png) no-repeat 14px  0px, url(content/img/thumbs/'+ e.target.slidethumb+'.jpg)  no-repeat 15px  5px;';
				 	      
				 	      var listNode = e.target.parentNode;
	
				 	      listNode.removeChild(e.target);
				 	      listNode.appendChild(e.target); 
	
				 	      e.target.addEventListener('click', xDAT.sortslidesarray, false);
				 	      e.target.removeEventListener('click', xDAT.sortslidesreset, false);    
			
			},
			saveSectionSlides: function(presentationName){
				var dyAg = this.dynamicAgenda;
					
					this.checkSlides(true);
				
					dyAg.update(presentationName);
							
			},
			getSlidesChecked: function(){
					var xDA = DynamicAgendaTemplate.prototype;
					var sildesAreChecked = false;
					var editMode = true;
					
					sildesAreChecked = xDA.checkSlides(false, editMode);
					
					return sildesAreChecked;
			
			},
			 playPresentation: function(section){
			 				 		
			 	var xDA = DynamicAgendaTemplate.prototype;
			 	var dyAg = this.dynamicAgenda ? this.dynamicAgenda : app.dynamicChapters;			 		                   
			 	var sectionMap = dyAg.current ? dyAg.current : dyAg.contentMap[section];
			 	var res = xDA.buildMenuSection(sectionMap);
			 	
			 	var popupSaveButton = document.getElementById("editbutton");
			 	
			 	if(popupSaveButton)
			 	{
			 		popupSaveButton.removeEventListener("click", xDA.popupSaveButtonEvent, false);
			 	}
			 	
			 	if(res)
			 	{
				
					setTimeout(function(){
						app.goTo(sectionMap, "introduction");
					}, 100);
			 	}
			 },
			 buildMenuSection: function(sectionID){
	 		 
	 		 	var content = app.collections[sectionID];
	 		 	var links = [];
	 		 	var idSplit = sectionID + "_";
	 		 	
	 		 	if(!typeof(content))
	 		 	{
	 		 		return false;
	 		 	}
	 			
	 			links.push({ title: 'Builder', goTo: "app.whitePopup.show('start_index');" });
	 			
	 		 	for(var i=0; i< content.content.length; i++)
	 		 	{
	 		 		var titleName = content.content[i].indexOf(idSplit) == -1 ? content.content[i]: content.content[i].split(idSplit)[1];
	 			 	
	 			 	// var titleNameReplaced = titleName.replace(/_/gi, " ");
	 			 	var titleStr = app.json.structures[titleName].name;
	 			 	var goToName = i<2	? titleName: sectionID+"_"+titleName;
	 			 	var classname = '';

	 			 	//if (titleName === 'introduction') {
	 			 		classname = 'home';
	 			 		titleStr = ' ';
	 			 	//}
	 			 	
	 		 		links.push({ title: titleStr, goTo: sectionID + "."+ goToName, className: classname });
	 		 	}
	 		 	
	 		 	app.menu = new Menu({
	 		 	  attachTo: sectionID,
	 		 	  offset:	-306,//404
	 		 	  links: links
	 		 	});
				
				// app.menu.attachTo.push(sectionID);

	 		 	return true;
	 		 },
	 		getSavedSections: function(divID){	

	 				console.log(divID);

	 				var list = document.getElementById(divID);
	 				var savedSections = this.dynamicAgenda.list();
	 				var slideshows = Object.keys(this.dynamicAgenda.contentMap);
	 				var count = 0;

	 				
	 				slideshows.forEach(function(name) {
	 					var newlink = document.createElement('li');
	 				  	newlink.addEventListener('click', this.openpage, false);
	 				  	newlink.innerHTML = name;
	 				  	list.appendChild(newlink);
	 				  count ++;
	 				});
	 				

	 				for(var i =0, len = list.childNodes.length-1; i<len;i++)
	 				{
		 				if (list.children[i]) {
	 						list.children[i].addEventListener('click', this.openpage, false);
	 					}
	 				}
	 				
	 									
	 				
	 				
	 		},
			openpage: function(e){
				console.log('cliiicked');
			
				var menuButtons = $(".listmenu").children('li');
				var buttonSelected = "";
	
				for(var i=0; i<menuButtons.length;i++)
				{
						if($("#"+menuButtons[i].id).hasClass("buttonselected"))
						{
							buttonSelected = menuButtons[i].id;
						}
				}
				
				
				switch (buttonSelected.toLowerCase()) 
				{
					case "playbutton":
								DynamicAgendaTemplate.prototype.playPresentation(e.target.innerHTML);
						break;
					
					case "editbutton":
						var dyAg = this.dynamicAgenda;
						
						DynamicAgendaTemplate.prototype.showEditMode(e.target);
						
						break;
								
					case "deletebutton":
							DynamicAgendaTemplate.prototype.deletePresentation(e.target.innerHTML);
							e.target.parentNode.removeChild(e.target);
						break;
					default :
						return false;			
						
				}
			},
			deletePresentation: function(name){
					var dyAg =  app.dynamicChapters;
					var sectionName = dyAg.contentMap[name];
				 	dyAg.destroy(name);
	
			},
			showEditMode: function(elem){
				var dyAg = this.dynamicAgenda ? this.dynamicAgenda : app.dynamicChapters;
				var xDA = DynamicAgendaTemplate.prototype;
				var divId = elem.parentNode.parentNode;
				var ulNode = elem.parentNode;
				var listAttr = {
				   				"id":"id",
				   				"idValue": "chapterlist",
				   				"className":"class",
				   				"classValue": "chapterlist"
				   			};
				var contentName = "";
				var section = "";   				
					
				divId.removeChild(ulNode);	
				divId.id = "chapters";
				
				$("#"+divId.id).removeClass("buttons");
				$("#"+divId.id).addClass("chapters");
				$("#"+divId.id).addClass("unsorted");
				
				$("#arrrowUp").removeClass("arrrowUp");
				$("#arrrowUp").addClass("hide");
				
				$("#arrowDown").removeClass("arrowDown");
				$("#arrowDown").addClass("hide");
				
				$(".agendatitleH1").text(elem.innerHTML);
				
				contentName =	dyAg.contentMap[elem.innerHTML];
				section = dyAg.data[contentName];
				dyAg.edit(elem.innerHTML);
				
				xDA.showEditPresentationMenu();	
				xDA.initEditmode();		
				xDA.addSections(divId.id, listAttr, section);
				

				for(var i =0; i<section.sections.length; i++)
				{
					var listID = section.sections[i]
					var docElem = document.getElementById("list_"+ section.sections[i].replace(contentName+"_", ""));
					xDA.lazySortarray(docElem);
								
					
					
				}
				
					
			},
			showEditPresentationMenu: function(){
					
					$(".menuholder").removeClass("showlist");
					$("#playbutton").html("Edit Slides");
					$("#editbutton").html("Save");
					$("#editbutton").removeClass("buttonselected");
					$("#deletebutton").html("Play");
					
					
					document.getElementById("playbutton").removeEventListener("click", app.slide.showplaylist.buttonSelect, false);
					document.getElementById("editbutton").removeEventListener("click", app.slide.showplaylist.buttonSelect, false);
					document.getElementById("deletebutton").removeEventListener("click", app.slide.showplaylist.buttonSelect, false);
	
					
					
	
			},
			initEditmode: function (){
					
					var addbutton = document.getElementById("playbutton");
						addbutton.addEventListener('click', this.editModeAddSlides, false);

					
					
					$("#editbutton").addClass("disabled");
					$("#deletebutton").addClass("disabled");   

			},
			editModeAddSlides: function(e){
				var xDA = DynamicAgendaTemplate.prototype;
				var attr =  {
								"className": "chaptertitle",
								"id"	: ""
							};			
			   var slideswrapper = document.getElementById("slidethumbs1");	
			   var dAg = app.dynamicChapters;
			   var contentName = dAg.contentMap[$(".agendatitleH1").text()];
			   var sections = dAg.data[contentName].sections;
			   
			   if(sections.length == 0)
			   {
				  	var  	errorstr = 'Save a chapter before adding slides';
				   	xDA.showMessageDialog(errorstr);
			   	
			   }
			   else {
			   		var popupSaveButton = document.getElementById("editbutton");
			   					   	popupSaveButton.addEventListener("click", xDA.popupSaveButtonEvent, false);
			   					   
			   					   
			   					   // xDA.removeEventList("showplaylistMenu");	
			   					   var sectionsChecked = sections.length;		
			   					   	
			   						document.getElementById("chapters").removeChild(document.getElementById("chapterlist"));		
			   						
			   						xDA.buildSectionSlides(sections, slideswrapper, attr);
			   						
			   								$("#slidethumbs1").addClass("sorted")
			   								$("#playbutton").addClass("disabled");
			   								$("#editbutton").removeClass("disabled");
			   							
			   		
			   							
			   						for(var i=0; i<sections.length; i++)
			   						{
			   							var slides = dAg.data[contentName].slides[sections[i]];	
			   							var order = 0;
			   							
			   		
			   							for(var y=0; y<slides.length; y++)
			   							{
			   								var elem = document.getElementById(slides[y] + "_slide");
			   									elem.removeEventListener('click', xDA.sortslidesarray, false);
			   									xDA.lazySortSlide(elem, y);	
			   									elem.addEventListener('click', xDA.sortslidesreset, false); 
			   							}					
			   		
			   						}		
			   		
			   }
			   				

			},
			removeEventList: function(divID){
			 	var elm = document.getElementById(divID);
			 	var xDA = DynamicAgendaTemplate.prototype; 
			 	
			 	for(var i=1; i<elm.children.length; i++)
			 	{
				 		elm.children[i].removeEventListener("click", xDA.sortarray, false);
				 		
				 		elm.children[i].removeEventListener("click", xDA.sortreset, false);
			 	}
			 		
			 },
	 		 lazySortarray: function(elem){
	 		 	//var pNode = e.target.parentNode;
	 		 	var pNode = elem.parentNode;
	 		 	var xDA = DynamicAgendaTemplate.prototype;
	 		 	var dAg = app.dynamicChapters;
	 		 	var contentName = dAg.contentMap[$(".agendatitleH1").text()];
	 		 	var sections = dAg.data[contentName].sections;
	 		 	var order = null;
	 		 	
	 		 	
	 		 	
	 		 	
	 		 	for(i=0; i<sections.length; i++)
	 		 	{
	 		 		if(sections[i].indexOf(elem.sectioncode) != -1)
	 		 		{
	 		 			order = i;
	 		 			break;
	 		 		}
	 		 	}
	 		 	
	 		 	$("#"+elem.id).addClass("active");	
	 		 
	 		 	      pNode.removeChild(elem);
	 		 	      pNode.insertBefore(elem,pNode.childNodes[order]); 
	 		 	      
	 		 	      
	 		 	      elem.addEventListener('click', xDA.sortreset, false);   
	 		 
	 		 },
	 		 lazySortSlide: function(elem, index){
	 		 	
	 		 	var pNode = elem.parentNode;
	 		 	var xDA = DynamicAgendaTemplate.prototype;
	 		 	var checkvar = 'checkbox_green';

	 		 	elem.style.cssText = 'background: url(content/img/agenda/'+checkvar+'.png) no-repeat 14px  0px, url(content/img/thumbs/'+ elem.slidethumb+'.jpg)  no-repeat 15px  5px;';
	 		 	
	 		 		$("#"+elem.id).addClass("active");	
	 		 
		 	      pNode.removeChild(elem);
		 	      pNode.insertBefore(elem, pNode.childNodes[index+1]); 
	 		 	      
	 		 	    
	 		 	  elem.addEventListener('click', xDA.sortreset, false);   
	 		 
	 		 },
	 		 openSave: function(e){
	 		 	$("#savewindow").removeClass("hide");
	 		 	$("#savewindow").addClass("show");
	 		 },
			popupSaveButtonEvent: function(e){
			
				var xDA = DynamicAgendaTemplate.prototype;
								
				if($("#"+e.target.id).hasClass("disabled"))
				{
					
				}
				else {
					
					$("#savewindow").addClass("show");
					$("#play").removeClass("disabled");
					$("#save").addClass("disabled");
					
					
					var saveallbutton = document.getElementById("saveallbutton");
					var cancelbutton = document.getElementById("cancelsave");
					
					$("#savetitle").text($(".agendatitleH1").text());
					
					cancelbutton.addEventListener('click', xDA.cancleSave, false);
					saveallbutton.addEventListener('click', xDA.saveCustomPresentation, false);
				}
				
				
					
					
			},
			saveSectionSlides: function(name){
				var xDA = DynamicAgendaTemplate.prototype;
				sildesAreChecked = xDA.checkSlides(false, false);
				
				app.dynamicChapters.update(name);
				
			},
	 		cancleSave: function(){
				
				$("#savewindow").removeClass("show");
				
			},
			saveCustomPresentation: function(e){
				var xDA = DynamicAgendaTemplate.prototype;
				var	dyA 	= app.dynamicChapters; 
				var slides = dyA.data[dyA.current].sections;
				var checkedOk = false;
				var slideName = dyA.data[dyA.current].name; 
				var emptySlides = xDA.checkSlides(true);
				
				
				
						var popupSaveButton = document.getElementById("editbutton");
							popupSaveButton.removeEventListener("click", xDA.popupSaveButtonEvent, false);
								
				
				var playbutton = document.getElementById("deletebutton");
					playbutton.addEventListener('click', xDA.playPresentation, false);
				
				
				$("#savewindow").removeClass("show");
				$("#savewindow").addClass("hide");
				
				dyA.update(slideName);
				
				$("#editbutton").addClass("disabled");
				$("#deletebutton").removeClass("disabled");
				
			},
			checkForEmptySections: function(){
				var	dyA 	= app.dynamicChapters; 
				var slides = dyA.data[dyA.current];
				
				
			},
			checkSlides: function(sendList, editMode){
				var dyAg = app.dynamicChapters;
				var slides = dyAg.data[dyAg.current].slides;
				var sections = dyAg.data[dyAg.current].sections;
				var noSlides = false;
				var emptySlides = [];
				var countSlides = 0;
			
			//Sections is the primary object for createing presentations, whith out sections you can not create new ones.
			//this statement gets all invalid object 
			if(!editMode)
			{	
				for(var i=0; i<sections.length; i++)
				{	
				
					//for (var name in slides) {
						if(slides[sections[i]])
						{
							if(slides[sections[i]].length > 0)
							{	
						    	noSlides = true;
						    }
						    else {	
						    	emptySlides.push(sections[i]);
						    }
						}else {
								emptySlides.push(sections[i]);
						}  
					  
					//}
				}
			}
				//This statment removes all objects that is not valid.
				for(var x=0; x<emptySlides.length; x++)
				{
					delete slides[emptySlides[x]];
					
					var order = sections.indexOf(emptySlides[x]);
					
					if (order !== -1) {
					  	sections.splice(order, 1);
					}
					
					
				}
				
				///this check is only for handeling come marked slides					
				if(!sendList)
				{
					//In buildMode/createMode the sections is not yet, extra check.
					if(!noSlides)
					{
						for(name in slides)
						{
							if(slides[name].length>0)
							{
								noSlides = true;
							}
						}
					}
					
					return noSlides;
				}
				else {
					return emptySlides;
				}
				
				
			},
			showMessageDialog: function(errorstr){
				var xDA = DynamicAgendaTemplate.prototype;
				document.getElementById('errorwindow').addEventListener('click', xDA.closeerror, false);
				
				$("#errorwindow").addClass('show');
				document.getElementById('errordialog').innerHTML = '<h1>'+errorstr+'</h1>';
			},
			closeerror: function() {
			     
			     $("#errorwindow").removeClass('show');
			     document.getElementById('errorwindow').childNodes[0].innerHTML = '';
			},
			 
					
}

