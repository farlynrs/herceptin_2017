(function(){
	'use strict';

	function initializeBuilder(element){
		var slidesLists = element.getElementsByClassName('slides-list'),
			slider = element.getElementsByClassName('slider')[0];
		if(slidesLists){
			slidesLists.forEach(function(slidesList){
				var scroll = new iScroll(slidesList.children[0]),
					prevButton = slidesList.previousElementSibling,
					nextButton = slidesList.nextElementSibling,
					thumbElements = slidesList.children[0].children;

				prevButton.onclick = function(){
					scroll.scrollTo(0, scroll.y + 200, '400ms');
				};
				nextButton.onclick = function(){
					scroll.scrollTo(0, scroll.y - 200, '400ms');
				};
			});
		}
		initializeNeededSlideList(element);
	}

	function initializeNeededSlideList(element){
		var wrappers = element.getElementsByClassName('needed-slides-line'),
			listWrapper, slider, slidesList;
		if(wrappers){
			listWrapper = wrappers[0].children[0];
			slidesList = new iScroll(listWrapper);
			slider = new Slider(wrappers[1]);
			slider.onchange = function(){
				var maxScrollX = listWrapper.scrollWidth - listWrapper.parentNode.offsetWidth,
					scrollOffset = (this.value / 100) * maxScrollX;
				slidesList.scrollTo(-scrollOffset, 0, '0ms');
			};
			slidesList.onscroll = function(x){
				slider.setValue(Math.round(x * 100 / this.maxScrollX));
			};
			slidesList.element.addEventListener('DOMSubtreeModified', function(){
				var width = this.offsetWidth - 997;
				if(width < 0){
					width = 997;
				}else{
					width = 1050 - width;
				}
				slider.handler.style.width = width + 'px';
				slider.refresh();
				slider.maxTranslate -= 2;
				slider.setValue(Math.round(slidesList.x * 100 / slidesList.maxScrollX));
				slider.setHandlerTranslate(slider.value * slider.fold);				
			}, false);
		}
	}

	document.addEventListener('presentationInit', function(){
		var slide = app.slide.BT_buildtrack = {};
		slide.onEnter = function(element){
			builder.loadSlides(element);
			app.elements.wPenBtns.style.display = 'none';
		};
		slide.onExit = function(element){
			app.elements.wPenBtns.style.display = 'block';
		};
	});
	document.addEventListener('contentLoad', function(){
		if(app.slideshow.id === 'buildtrack'){
			initializeBuilder(document.getElementById('BT_buildtrack'));
		}
	});
})();