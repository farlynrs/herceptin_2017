	var manager = new jsAnimManager(); 
	function animarDesdeArriba(idTemp){
		shroom = document.getElementById(idTemp);  
  
		manager.registerPosition(idTemp);  
		  
		shroom.setPosition(-64,8);   
		var anim = manager.createAnimObject(idTemp);  
		  
		anim.add({property: Prop.positionSemicircle(true),   
			to: new Pos(-42,32), duration: 1000/*, ease: jsAnimEase.bounceSmooth*/, onComplete: function() { animarDesdeAbajo(idTemp); } }); 
	}

	function animarDesdeAbajo(idTemp){
		shroom = document.getElementById(idTemp);  
  
		manager.registerPosition(idTemp);  
		  
		shroom.setPosition(-42,32);   
		var anim = manager.createAnimObject(idTemp);  
		  
		anim.add({property: Prop.positionSemicircle(false),   
			to: new Pos(-64,8), duration: 1000, onComplete: function() { animarDesdeArriba(idTemp); }  }); 
	}	
	