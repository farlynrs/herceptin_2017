document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide63 = {
        elements: {

        },
        onEnter: function(ele) {

            $(".btnDosis").on("tap", function() {
                app.goTo('storyboard', 'slide63', 'slide63');
            });
            $(".btnPreparacion2").on("tap", function() {
                app.goTo('storyboard', 'slide63', 'slide64');
            });
            $(".btnAdministracion").on("tap", function() {
                app.goTo('storyboard', 'slide63', 'slide65');
            });
            $(".btnAlmacenamiento").on("tap", function() {
                app.goTo('storyboard', 'slide63', 'slide66');
            });
            $(".btnVital").on("tap", function() {
                app.goTo('storyboard', 'slide63', 'slide67');
            });

            $(".wPen-btns").css({ "display": "none" });
            $("#logoRoche").css({ "display": "block" });
            $(".wPen-btns").css({ "display": "none" });
            $("#logo").css({ "display": "block" });
            $("#wPenReferencias").css("display", "none");
            $(".texto_footer").css("display", "block");
        },
        onExit: function() {

        }
    };
});