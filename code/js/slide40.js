document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide40 = {
        elements: {

        },
        onEnter: function(ele) {
            $("#wPenBtns").css({ "display": "none" });
            $("#slide40 #lupa1").on("tap", function() {
                $("#slide40 #popUpLupa1").fadeIn();
                $(".texto_footer").css({ "display": "none" });
                $("#logo").css({ "display": "none" });
            });
            $("#slide40 #cierraLupa1").on("tap", function() {
                $("#slide40 #popUpLupa1").fadeOut();
                $(".texto_footer").css({ "display": "block" });
                $("#logo").css({ "display": "block" });
            });

            $("#slide40 #lupa2").on("tap", function() {
                $("#slide40 #popUpLupa2").fadeIn();
                $(".texto_footer").css({ "display": "none" });
                $("#logo").css({ "display": "none" });
            });
            $("#slide40 #cierraLupa2").on("tap", function() {
                $("#slide40 #popUpLupa2").fadeOut();
                $(".texto_footer").css({ "display": "block" });
                $("#logo").css({ "display": "block" });
            });

            $("#slide40 #lupa3").on("tap", function() {
                $("#slide40 #popUpLupa3").fadeIn();
                $(".texto_footer").css({ "display": "none" });
                $("#logo").css({ "display": "none" });
            });
            $("#slide40 #cierraLupa3").on("tap", function() {
                $("#slide40 #popUpLupa3").fadeOut();
                $(".texto_footer").css({ "display": "block" });
                $("#logo").css({ "display": "block" });
            });

            $("#slide40 #lupa4").on("tap", function() {
                $("#slide40 #popUpLupa4").fadeIn();
                $(".texto_footer").css({ "display": "none" });
                $("#logo").css({ "display": "none" });
            });
            $("#slide40 #cierraLupa4").on("tap", function() {
                $("#slide40 #popUpLupa4").fadeOut();
                $(".texto_footer").css({ "display": "block" });
                $("#logo").css({ "display": "block" });
            });
        },


        onExit: function() {
            $("#logo").css({ "display": "block" });
            $("#wPenBtns").css("display", "block");
            $(".texto_footer").css({ "display": "block" });
            $("#slide40 #popUpLupa1").fadeOut();
            $("#slide40 #popUpLupa2").fadeOut();
            $("#slide40 #popUpLupa3").fadeOut();
            $("#slide40 #popUpLupa4").fadeOut();
        }
    };

});