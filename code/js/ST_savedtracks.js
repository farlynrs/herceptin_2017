(function(){
	'use strict';
	var template;

	function loadPresentationsList(slideElement){
		var avastinStorage = localStorage.getItem('avastin-global-2'), presentations,
			presentationList = document.getElementById('scrollWrapper');
		if(avastinStorage){
			avastinStorage = JSON.parse(avastinStorage);
			presentations = avastinStorage.presentations;
			if(presentations.length > 6){
				presentationList.parentNode.addClass('big-list');
			}
			presentations.forEach(function(presentation){
				var presentationInfo = document.createElement('li'), thumbLine,
					slideshowThumbs, deleteButton, editButton, playButton;

				presentationInfo.innerHTML = template.replace('{name}', presentation.name).replace('{date}', presentation.date);
				presentationList.appendChild(presentationInfo);
				thumbLine = presentationInfo.getElementsByClassName('slide-collection')[0].children[0];

				slideshowThumbs = document.createDocumentFragment();
				presentation.map.forEach(function(slideId){
					var element = document.createElement('div');
					element.style.background = 'white url(content/img/thumbs/' + slideId + '.jpg) no-repeat';
					slideshowThumbs.appendChild(element);
				});
				thumbLine.appendChild(slideshowThumbs);

				if(presentation.map.length > 6){
					thumbLine.parentNode.addClass('big-list');
				}

				deleteButton = presentationInfo.getElementsByClassName('delete-presentation')[0];
				deleteButton.onclick = function(){
					presentations.splice(presentations.indexOf(presentation), 1);
					localStorage.setItem('avastin-global-2', JSON.stringify(avastinStorage));
					presentationList.removeChild(presentationInfo);
					if(presentations.length < 7){
						presentationList.parentNode.removeClass('big-list');
					}
				};

				editButton = presentationInfo.getElementsByClassName('edit-presentation')[0];
				editButton.onclick = function(){
					app.goTo('buildtrack', 'BT_buildtrack');
					builder.showPresentation(presentation.id);
				};

				playButton = presentationInfo.getElementsByClassName('start-presentation')[0];
				playButton.onclick = function(){
					builder.playPresentation(presentation.id);
				}
			});
		}
	}

	document.addEventListener('presentationInit', function(){
		utils.ajax('slides/saved-track-template.html', function(){
			if(this.readyState === 4){
				template = this.responseText;
			}
		}, true);
		app.slide.ST_savedtracks = {
			onEnter: function(){
				app.elements.wPenBtns.style.display = 'none';
				new iScroll(document.getElementById('scrollWrapper'), {momentum:false, bounceLock: true});
				document.getElementsByClassName('scroll-horizontal').forEach(function(element){
					var scroll = new iScroll(element, {
							momentum:false, bounceLock: true, stopPropagation: true
						});
					element.parentNode.parentNode.addEventListener('swipeleft', function(){
						element.parentElement.addClass('hidden');
						element.parentElement.nextElementSibling.addClass('open').removeClass('close');
					});
					element.parentNode.parentNode.addEventListener('click', function(){
						if(!util.hasClass(element.parentElement, 'hidden')){
							element.parentElement.addClass('hidden');
							element.parentElement.nextElementSibling.addClass('open').removeClass('close');
						}
						else{
							element.parentElement.removeClass('hidden');
							element.parentElement.nextElementSibling.removeClass('open').addClass('close');
							scroll.refresh();	
						}
					});
					element.parentNode.parentNode.addEventListener('swiperight', function(){
						element.parentElement.removeClass('hidden');
						element.parentElement.nextElementSibling.removeClass('open').addClass('close');
						scroll.refresh();
					});
				});
			},
			onExit: function(){
				app.elements.wPenBtns.style.display = 'block';
			}
		};
	});
	document.addEventListener('contentLoad', function(){
		if(app.slideshow.id === 'savedtracks'){
			loadPresentationsList(document.getElementById('ST_savedtracks'));
		}
		$("#goToPrev").bind("click",function(){
		if(app.slideshow.id!="homeTrack"){
			app.collection.previous();
			app.slideshow.getIndex()=app.slideshow.getIndex()-1;
			event.stopPropagation();
			}else if(app.slideshow.id=="homeTrack"){
			app.slideshow.previous();
			$("#goToNext").css("display","block");
			if(app.slideshow.getIndex()==0){
				$("#goToPrev").css("display","none");
			}
			app.slideshow.getIndex()=app.slideshow.getIndex()-1;
			event.stopPropagation();
			} 
		});
		$("#goToNext").bind("click",function(){
		if(app.slideshow.id!="homeTrack"){
			app.collection.next();
			app.slideshow.getIndex()=app.slideshow.getIndex()-1;
			event.stopPropagation();
			}else if(app.slideshow.id=="homeTrack"){
			app.slideshow.next();
			$("#goToPrev").css("display","block");
			if(app.slideshow.length-1==app.slideshow.getIndex()){
				$("#goToNext").css("display","none");
			}
			app.slideshow.getIndex()=app.slideshow.getIndex()-1;
			event.stopPropagation();
			}
		});
		
	});
})();