document.addEventListener('presentationInit', function(){
	var i,
		slideList = ['PP_cs_reck1_questionwhat', 'PP_cs_reck2_questionwhat', 'PP_cs_reck2_questionwith', 'PP_cs_serke_questionwhat', 'PP_cs_serke_questionwith', 'PP_cs_grossi_questiongiven', 'PP_cs_reck1_questionhow'],
		onSlideEnter = function(slide){
			var area, slice = [].slice;
			if(!this.isInit){
				this.isInit = true;
				area = slide.querySelector(".draggable-wrapper .questions");
				this.targetManager = new MultiDrag.TargetManager(
					area,
					slice.call(area.querySelectorAll("li:not(.drop-target)"), 0),
					slice.call(area.querySelectorAll("li.drop-target"), 0)
				);
				this.targetManager.targets[0].onAdd = function(obj){
					obj.el.addClass("selected");
				};
				this.targetManager.targets[0].onRemove = function(obj){
					obj.el.removeClass("selected");
				};

				slide.querySelector('.submit-drag').addEventListener(touchy.events.start, function(){

					var selected = slide.querySelector(".content-wrapper .draggable-wrapper .questions li.selected");
					if(selected){
						//Extracts monitordata from object with all the answerdata
						var monitorData = app.slide.PP_cs_grossi_questionbased.slideMonitor[slide.id];
						var answerText = selected.querySelectorAll('p');
						var monitorAnswer = ''; 
						// some of the answerboxes has only one paragraph
						if(answerText.length > 1){
							monitorAnswer = answerText[1].innerText;
						}
						else{
							monitorAnswer = answerText[0].innerText;
						}
						submitUniqueCustomEvent('1009 - Patient profiling - Case studies', monitorData, monitorAnswer);
						app.data.saveCustomSlide(selected.getAttribute("data-popup"));
						app.caseStudiesPopup.show(selected.getAttribute("data-popup"));
					}
				});
			}
		};

	document.addEventListener('contentLoad', function(){
		for(i in slideList){
			app.slide[slideList[i]].isInit = false;
		}
	});

	for(i in slideList){
		app.slide[slideList[i]] = {
			isInit:false,
			onEnter:onSlideEnter,
			onExit:function(){
			}
		};
	}				
});