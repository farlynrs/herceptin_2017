document.addEventListener('presentationInit', function() {
  var slide = app.slide.SE_discuss = {
    onEnter: function(ele) {
      this.wrapper = ele.querySelector('#ipad_boxes_wrapper');
      this.wrapper.addEventListener('tap', this.iPadClick);
      
      //Checking through sessionstorage to if choices have been checked before and if so resets them.
      var iPads = ele.querySelectorAll('#ipad_boxes_wrapper .ipad_box');
      var topics = sessionStorage.topics.split('; ');

      for (var i=0, len=topics.length -1; i<len; i++) {
        for (var j = 0; j < iPads.length; j++) {
          if(iPads[j].getAttribute('data-topic') === topics[i]){
            iPads[j].style.opacity = 1;
          }
        }
      }
    },
    onExit: function(ele) {
      cart.addOtherTopic();
      this.wrapper.removeEventListener('tap', this.iPadClick);
    },
    iPadClick: function(e) {
      var ele = e.target;
      var topic = ele.getAttribute('data-topic');
      // Do not allow clicks if iPads are faded out
      if (app.slide.SE_discuss.wrapper.style.opacity !== '0.1') {
        // Make sure we have clicked an iPad
        if (topic) {
          ele.style.opacity = ele.style.opacity != 1 ? 1 : 0.6;
          ele.style.opacity == 1 ? cart.addTopic(topic) : cart.removeTopic(topic);
        }
      }
    }
  };  
}); 
