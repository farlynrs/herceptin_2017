document.addEventListener('presentationInit', function() {
 /* var slide = app.slide.SC_closing = {
  	elements: {
		shoppingNav: "#shopping_nav"
	},*/
    onEnter: function(ele) {
    	util.addClass(slide.element.shoppingNav,'nextactive');
    	app.shoppingPopup.show('SE_discuss');
    	
    	app.elements.wPenBtns.style.display = 'none';
    	app.addEvent('tap', slide.switchTab, slide.element.shoppingNav);
    },
    onExit: function(ele) {
    	app.elements.wPenBtns.style.display = 'block';
    },
    switchTab: function(e){
		var ele = e.target;
		if (ele.nodeType === 3) {
			ele = ele.parentNode;
		}
		if (ele.className === 'btn-text') {
			ele = ele.parentNode;
		}

		util.removeClass(slide.element.shoppingNav,'nextactive');
		util.removeClass(slide.element.shoppingNav,'cartactive');

		if(ele.className === 'next-btn'){
			util.addClass(slide.element.shoppingNav,'nextactive');
			app.shoppingPopup.show('SE_discuss');
		}
		else if(ele.className === 'cart-btn'){
			util.addClass(slide.element.shoppingNav,'cartactive');
			app.shoppingPopup.show('SE_shoppingcart');
		}
    }
  };  
}); 

