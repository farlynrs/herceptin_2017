(function() {
  window.monitormap = {
    subCampaignID:    '10160000',
    country:          'Global',
    presentation:     'Avastin Colon Rectal',
    slideshows: {
      IN_intro:                 'Portada',
	    slide153:                	'Patient Centric Approach',
      slide141:                	'Historia del RAS',
      slide25:                  'Menu'
    },
    slides: {
    IN_intro : 'Portada',
    slide2 : 'Tiempo en el Hospital',    
    slide3 : 'La evolución de Herceptin',
    slide4 : 'Herceptin en 5 minutos',  
    slide6 :  'Tiempo promedio de administración',         
    slide11 :  'Video beneficios de Herceptin en 5 min.',      
    slide12 :  'Diseño del estudio Hannah',      
    slide15 :  'Eficacia comparable a IV',      
    slide17 :  'Concentraciones Cvalle precirugia',      
    slide18 :  'Características de Juana',      
    slide19 :  'Diseño del estudio SafeHEr',      
    slide21 :  'El estudio SafeHEr confirma seguridad',      
    slide22 :  'Incidencia de eventos adversos',     
    slide26 :  'Dosis fija',      
    slide28 :  'Video eficacia y seguridad',      
    slide30 :  'Formulación SC liberando recursos',      
    slide32 :  'Impactando en la eficiencia del centro',     
    slide33 :  'Satisfacción de los profesionales de salud',      
    slide34 :  'Video libera recursos',      
    slide36 :  'Diseño del estudio PrefHer',     
    slide38 :  'Preferencia de pacientes',      
    slide40 :  'Cierre beneficios Herceptin SC',     
    slide45 :  'Ciclos de administración',     
    slide46 :  '18 ciclos son estándar de tratamiento',     
    slide50 :  'NSABP-NCCTG, HERA, PHARE',     
    slide52 :  'NSABP-NCCTG, HERA, PHARE 1.1',     
    slide53 :  'Video de administración',     
    slide54 :  'Video de administración 1.1',      
    slide55 :  'Video de administración 1.2',      
    slide56 :  'Video de administración 1.3',      
    slide58 :  'Manejo de tejidos',      
    slide59 :  'Manejo de tejidos 1.1',      
    slide60 :  'Manejo de tejidos 1.2',      
    slide61 :  'Manejo de tejidos 1.3',      
    slide63 :  'Dosis y administracón Herceptin SC',     
    slide64 :  'Dosis y administracón Herceptin SC 1.1',      
    slide65 :  'Dosis y administracón Herceptin SC 1.2',     
    slide66 :  'Dosis y administracón Herceptin SC 1.3',     
    slide67 :  'Dosis y administracón Herceptin SC 1.4',      
    slide68 :  'Videos y animaciones',    
    slideReferencias :  'Referencias',           
    guia:				'Guia'
    }
  };
  
  var date = new Date();

  // Save campaign info
  /*submitUniqueCustomEvent('Campaign', 'SubCampaignID', monitormap.subCampaignID);
  submitUniqueCustomEvent('Campaign', 'Timezone Offset', date.getTimezoneOffset());*/
  
 ag.submit.data({
 category: "Versions",
 label: "Framework version",
 value: "2.4.0"
 });
 ag.submit.data({
 category: "Versions",
 label: "Presentation version",
 value: "1.0.0"
 });

})();
