/*FormCheckSend para Jquery por Salvador Gonzalez (@sgb004)*/
(function($){
	function Form(_this,o){
		var _self = this;
		var d = {
			action: '',
			method: '',
			before: '',
			complete: '',
			loading: '',
			dataType: 'json',
			inputValidate: new Array('numeric','email'),
			dataArray: 0,
			tag: 'obligatory',
		}

		jQuery.extend(d,o);

		$.each({action:'',method:''},function(k,v){
			if(d[k] == ''){d[k] = _this.attr(k);}
		});

		if(d.complete == ''){
			d.complete = _this.attr('name');
			d.complete = d.complete+'_complete';
		}

		$.each({before:'',complete:''},function(k,v){
			if(typeof d[k] == 'string'){d[k] = window[d[k]];}
		});

		if(d.action == ''){d.action = location.href;}

		$.each(d,function(k,v){_self[k] = d[k];});

		this.form = $(_this);
		this.sending = false;
		this.data = '';
		this.empty = false;
		this.validate = true;
		this.inputs;
		this.inputs_obligatory;
		this.inputsButtons;
		this.inputError = '';
		this.inputErrorName = '';
		this.status = '';
		this.stop = false;

		this.init(_self);

		return this;
	}

	Form.prototype = {
		init: function(_this){
			if(this.loading == ''){this.loading = this.form.find('.loading');}

			this.form.find('.validate-numeric').on('keydown',only_numbers);
			this.form.on('submit',function(e){
				e.preventDefault();
				if(typeof _this.before == 'function'){_this.before();}
				if(!_this.stop){_this.submit();}
				_this.stop = false;
			});
		},
		get_data: function(){
			this.data = '';
			if(this.dataArray == 0){
				this.data = this.form.serializeArray();
			}else if(this.dataArray == 1){
				this.data = this.form.serialize();
			}else{
				var name = '';
				var value = '';
				var input;

				this.data = null;
				this.data = {};

				this.get_inputs();
				for(var i=0; i<this.inputs.length; i++){
					input = $(this.inputs[i]);

					name = input.attr('name');
					value = this.get_input_value(input);

					this.data[name] = value;
				}
			}
			return this.data;
		},
		get_inputs: function(){
			this.inputs = this.form.find('*[name]');
			return this.inputs;
		},
		get_inputs_obligatory: function(){
			this.inputs_obligatory = this.form.find('*['+this.tag+']');
			return this.inputs_obligatory;
		},
		get_inputButtons: function(){
			this.inputButtons = this.form.find('.input-button');
			return this.inputButtons;
		},
		get_input_value: function(input){
			var value = '';
			var name = input.attr('name');
			var type = input.attr('type');

			type = (type == undefined) ? '' : type.toLowerCase();
			value = (type == 'radio' || type == 'checkbox') ? this.form.find('*[name="'+name+'"]:checked').val() : input.val();

			return value;
		},
		submit: function(e){
			this.check();

			if(this.status == ''){
				this.send();
			}else{
				this.complete(this.status);
			}
		},
		send: function(e){
			if(this.sending){
				this.complete('sending');
			}else{
				var _this = this;
				this.get_data();
				this.get_inputs();
				this.get_inputButtons();
				this.loading_show();
				this.disabled();
				this.sending = true;

				$.ajax({
					type: this.method,
					url: this.action,
					data: this.data,
					dataType: this.dataType
				})
				.done(function(data){
					_this.status = 'done';
					_this.loading_hidden();
					_this.enabled();
					_this.sending = false;
					_this.complete(_this.status,data);
				})
				.fail(function(data,status){
					_this.status = status;
					_this.loading_hidden();
					_this.enabled();
					_this.sending = false;
					_this.complete(_this.status,data.responseText);
				})
			}
		},
		check: function(){
			var required = this.form.find('*['+this.tag+']');
			var name = '';
			var value = '';
			var inputValidate = false;

			this.status = '';
			this.inputErrorName = '';
			this.empty = false;
			this.validate = true;

			this.get_inputs();
			this.get_inputs_obligatory();
			this.get_inputButtons();

			for(var i=0; i<this.inputs_obligatory.length; i++){
				this.inputError = $(this.inputs_obligatory[i]);

				name = this.inputError.attr('name');
				if(name != this.inputErrorName){
					this.inputErrorName = name;

					value = this.get_input_value(this.inputError);

					if(typeof value == 'string'){value = value.trim();}

					if(value==undefined || value==null || value==''){
						this.empty = true;
						break;
					}else{
						this.empty = false;
						for (var j=0; j<this.inputValidate.length; j++) {
							inputValidate = this.inputError.hasClass('validate-'+this.inputValidate[j]);
							if(inputValidate){
								this.validate = this['check_validate_'+this.inputValidate[j]](value);
							}
							if(!this.validate){break;}
						}
						if(!this.validate){break;}
					}
				}
			}

			if(this.empty){
				this.status = 'empty';
			}else if(!this.validate){
				this.status = 'invalid';
			}else{
				this.status = '';
				this.inputError = null;
				this.inputErrorName = '';
			}
		},
		check_validate_numeric: function(v){	
			var validate = false;
			v = parseInt(v);
			validate = !isNaN(v);
			return validate;
		},
		check_validate_email: function(v){
			var validate = false;
			var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			validate = filter.test(v);
			return validate;
		},
		disabled: function(){
			this.inputs.attr('disabled',true);
			this.inputButtons.attr('disabled',true).css({'opacity':'0.5'});
		},
		enabled: function(){
			this.inputs.removeAttr('disabled');
			this.inputButtons.removeAttr('disabled').css({'opacity':'1'});
		},
		loading_show: function(){
			if(this.loading.length > 0){this.loading.css({'display':'block'});}
		},
		loading_hidden: function(){
			if(this.loading.length > 0){this.loading.css({'display':'none'});}
		},
		reset: function(){
			this.form[0].reset();
		},
		input_focus: function(input){
			if(input != undefined){
				$(input).focus().select();
			}
		},
		inputError_focus: function(input){
			this.inputError.focus().select();
		},
		check_password: function(pass,veri,min,max){
			var passLen = 0;
			
			pass = this.form[0][pass];
			veri = this.form[0][veri];
			
			passLen = pass.value.length;

			if(passLen < min || passLen > max){
				this.status = 'invalid';
				this.validate = false;
				this.inputError = $(pass);
				this.inputErrorName = pass.getAttribute('name');
			}else if(pass.value != veri.value){
				this.status = 'invalid';
				this.validate = false;
				this.inputError = $(veri);
				this.inputErrorName = veri.getAttribute('name');
			}
		}
	}

	$.fn.wForm = function(o,s){
		var result = [];

		this.each(function($this){
			var _this = $(this);
			var form = _this.data('_wForm');

			if(form == undefined || typeof o != 'string'){
				form = new Form(_this,o);
				_this.data('_wForm',form);
				result.push(_this);
			}else{
				form = _this.data('_wForm');
				var ele = form[o];

				if(typeof ele == 'function'){
					ele = form[o](s);
				}else{
					if(s != undefined || s != null){
						form[o] = s;
						result.push(_this);
					}else{
						ele = form[o];
						result.push(ele);
					}
				}
			}
		})

		return $.each(result,function(k,v){return v;});
	}
})(jQuery);

function only_numbers(e){
	var keynum = window.event ? window.event.keyCode : e.which;
	if(keynum == 8 || keynum == 13 || keynum == 9 || keynum == 16) return true;
	if(keynum > 57){keynum -= 48;}
	return /\d/.test(String.fromCharCode(keynum));
}