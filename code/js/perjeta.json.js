function PERJETA(){
	return'{\n'+
'	"storyboard" : ["StoryBoardKP"],\n'+
'	"slides" : {\n'+
'		"perjeta_home" : {\n'+
'			"name" : "Inicio",\n'+
'			"file" : "perjeta_home.html"\n'+
'		},\n'+
'		"perjeta_hacerMas" : {\n'+
'			"name" : "Hacer más",\n'+
'			"file" : "perjeta_hacerMas.html"\n'+
'		},\n'+
'		"perjeta_perjeta_s1" : {\n'+
'			"name" : "Perjeta 1",\n'+
'			"file" : "perjeta_perjeta_s1.html"\n'+
'		},\n'+
'		"perjeta_perjeta_s2" : {\n'+
'			"name" : "Perjeta 2",\n'+
'			"file" : "perjeta_perjeta_s2.html"\n'+
'		},\n'+
'		"perjeta_perjeta_s3" : {\n'+
'			"name" : "Perjeta 3",\n'+
'			"file" : "perjeta_perjeta_s3.html"\n'+
'		},\n'+
'		"perjeta_eficacia_cleopatra" : {\n'+
'			"name" : "Eficacia cleopatra",\n'+
'			"file" : "perjeta_eficacia_cleopatra.html"\n'+
'		},\n'+
'		"perjeta_eficacia_supervivencia" : {\n'+
'			"name" : "Eficacia supervivencia",\n'+
'			"file" : "perjeta_eficacia_supervivencia.html"\n'+
'		},\n'+
'		"perjeta_eficacia_supervivencia_global" : {\n'+
'			"name" : "Eficacia supervivencia global",\n'+
'			"file" : "perjeta_eficacia_supervivencia_global.html"\n'+
'		},\n'+
'		"perjeta_eficacia_tasas_respuesta" : {\n'+
'			"name" : "Eficacia tasas respuesta",\n'+
'			"file" : "perjeta_eficacia_tasas_respuesta.html"\n'+
'		},\n'+
'		"perjeta_calidad_vida_s1" : {\n'+
'			"name" : "Calidad de vida 1",\n'+
'			"file" : "perjeta_calidad_vida_s1.html"\n'+
'		},\n'+
'		"perjeta_calidad_vida_s2" : {\n'+
'			"name" : "Calidad de vida 2",\n'+
'			"file" : "perjeta_calidad_vida_s2.html"\n'+
'		},\n'+
'		"perjeta_perfil_seguridad_s1" : {\n'+
'			"name" : "Perfil de seguridad 1",\n'+
'			"file" : "perjeta_perfil_seguridad_s1.html"\n'+
'		},\n'+
'		"perjeta_perfil_seguridad_s2" : {\n'+
'			"name" : "Perfil de seguridad 2",\n'+
'			"file" : "perjeta_perfil_seguridad_s2.html"\n'+
'		},\n'+
'		"perjeta_perfil_seguridad_s3" : {\n'+
'			"name" : "Perfil de seguridad 3",\n'+
'			"file" : "perjeta_perfil_seguridad_s3.html"\n'+
'		},\n'+
'		"perjeta_perfil_seguridad_s4" : {\n'+
'			"name" : "Perfil de seguridad 4",\n'+
'			"file" : "perjeta_perfil_seguridad_s4.html"\n'+
'		},\n'+
'		"perjeta_administracion" : {\n'+
'			"name" : "Administración",\n'+
'			"file" : "perjeta_administracion.html"\n'+
'		},\n'+
'		"perjeta_resumen_s1" : {\n'+
'			"name" : "Resumen 1",\n'+
'			"file" : "perjeta_resumen_s1.html"\n'+
'		},\n'+
'		"perjeta_resumen_s2" : {\n'+
'			"name" : "Resumen 2",\n'+
'			"file" : "perjeta_resumen_s2.html"\n'+
'		},\n'+
'		"perjeta_resumen_s3" : {\n'+
'			"name" : "Resumen 3",\n'+
'			"file" : "perjeta_resumen_s3.html"\n'+
'		},\n'+
'		"perjeta_resumen_s4" : {\n'+
'			"name" : "Resumen 4",\n'+
'			"file" : "perjeta_resumen_s4.html"\n'+
'		},\n'+
'		"guia" : {\n'+
'			"name" : "Guía",\n'+
'			"file" : "guia.html"\n'+
'		}\n'+
'	},\n'+
'	"structures" : {\n'+
'		"StoryBoardKP" : {\n'+
'			"name" : "PERJETA",\n'+
'			"type" : "collection",\n'+
'			"content" : ["perjeta_home","perjeta_hacerMas","perjeta_perjeta","perjeta_eficacia","perjeta_calidad_vida","perjeta_perfil_seguridad","perjeta_administracion","perjeta_resumen","guia"],\n'+
'			"valid": true\n'+
'		},\n'+
'		"perjeta_home" : {\n'+
'			"content" : ["perjeta_home"],\n'+
'			"name" : "Inicio",\n'+
'			"type" : "slideshow",\n'+
'			"valid" : true\n'+
'		},\n'+
'		"perjeta_hacerMas" : {\n'+
'			"content" : ["perjeta_hacerMas"],\n'+
'			"name" : "Hacer más",\n'+
'			"type" : "slideshow",\n'+
'			"valid" : true\n'+
'		},\n'+
'		"perjeta_perjeta" : {\n'+
'			"content" : ["perjeta_perjeta_s1","perjeta_perjeta_s2","perjeta_perjeta_s3"],\n'+
'			"name" : "Perjeta",\n'+
'			"type" : "slideshow",\n'+
'			"valid" : true\n'+
'		},\n'+
'		"perjeta_eficacia" : {\n'+
'			"content" : ["perjeta_eficacia_cleopatra","perjeta_eficacia_supervivencia","perjeta_eficacia_supervivencia_global","perjeta_eficacia_tasas_respuesta"],\n'+
'			"name" : "Eficacia",\n'+
'			"type" : "slideshow",\n'+
'			"valid" : true\n'+
'		},\n'+
'		"perjeta_calidad_vida" : {\n'+
'			"content" : ["perjeta_calidad_vida_s1","perjeta_calidad_vida_s2"],\n'+
'			"name" : "Calidad de vida",\n'+
'			"type" : "slideshow",\n'+
'			"valid" : true\n'+
'		},\n'+
'		"perjeta_perfil_seguridad" : {\n'+
'			"content" : ["perjeta_perfil_seguridad_s1","perjeta_perfil_seguridad_s2","perjeta_perfil_seguridad_s3","perjeta_perfil_seguridad_s4"],\n'+
'			"name" : "Perfil de seguridad",\n'+
'			"type" : "slideshow",\n'+
'			"valid" : true\n'+
'		},\n'+
'		"perjeta_administracion" : {\n'+
'			"content" : ["perjeta_administracion"],\n'+
'			"name" : "Administración",\n'+
'			"type" : "slideshow",\n'+
'			"valid" : true\n'+
'		},\n'+
'		"perjeta_resumen" : {\n'+
'			"content" : ["perjeta_resumen_s1","perjeta_resumen_s2","perjeta_resumen_s3","perjeta_resumen_s4"],\n'+
'			"name" : "Resumen",\n'+
'			"type" : "slideshow",\n'+
'			"valid" : true\n'+
'		},\n'+
'		"guia" : {\n'+
'			"content" : ["guia"],\n'+
'			"name" : "Guía",\n'+
'			"type" : "slideshow",\n'+
'			"valid" : true\n'+
'		}\n'+
'	}\n'+
'}';
}