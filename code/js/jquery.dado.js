/*DADO Drag and Drop para Jquery por Salvador Gonzalez (@sgb004)*/
(function($){
	function Dado($ele, o){
		var d = {
			elements: '',
			elementSelected: '',
			before: '',
			start: '',
			end: ''
		}
		var callbacks = new Array('before','start','end','move');

		jQuery.extend(d,o);

		this.container = $ele;
		this.containerPosition = {};
		this.containerPositionX = 0;
		this.containerPositionY = 0;
		this.elements = $(d.elements);
		this.elementSelected = '';
		this.elementAdd = false;
		this.elementPosition = {};
		this.elementPositionX = 0;
		this.elementPositionY = 0;
		this.elementMove = false;
		this.dadoX = 0;
		this.dadoY = 0;

		if(d.elementSelected != ''){
			this.elementAdd = true;
			this.elementSelected = $(d.elementSelected);
		}

		for(var v in callbacks){
			if(typeof d[callbacks[v]] == 'function'){
				this[callbacks[v]] = d[callbacks[v]];
			}
		}

		this.init();

		return this;
	}

	Dado.prototype = {
		init: function(){
			var _this = this;

			this.elements.on('mousedown',function(e){_this.elements_mousedown(e,this);});
			this.container.on('mousemove',function(e){_this.container_mousemove(e);});
			$('html').on('mouseup',function(e){_this.html_mouseup();});
				
			this.container[0].addEventListener("touchstart", this.touchHandler, true);
			this.container[0].addEventListener("touchmove", this.touchHandler, true);
			this.container[0].addEventListener("touchend", this.touchHandler, true);
			this.container[0].addEventListener("touchcancel", this.touchHandler, true);
		},
		touchHandler: function(event){
			//Parte de la libreia touch.js
			//http://ross.posterous.com/2008/08/19/iphone-touch-events-in-javascript/
		    var touches = event.changedTouches,
				first = touches[0],
				type = "";
		    
		    switch(event.type){
				case "touchstart": type = "mousedown"; break;
				case "touchmove":  type="mousemove"; break;
				case "touchend":   type="mouseup"; break;
				default: return;
		    }
		    var simulatedEvent = document.createEvent("MouseEvent");
		    simulatedEvent.initMouseEvent(type, true, true, window, 1,
							first.screenX, first.screenY,
							first.clientX, first.clientY, false,
							false, false, false, 0/*left*/, null);
		    
		    first.target.dispatchEvent(simulatedEvent);
		    event.preventDefault();
		},
		elements_mousedown: function(e,_this){
			document.onselectstart = function (){return false};
			document.onmousedown = function (){return false};

			_this = $(_this);

			this.elementPosition = _this.offset();
			this.elementPositionX = e.pageX-this.elementPosition.left;
			this.elementPositionY = e.pageY-this.elementPosition.top;

			this.dadoX = e.pageX-this.containerPositionX-this.elementPositionX;
			this.dadoY = e.pageY-this.containerPositionY-this.elementPositionY;

			this.elementMove = true;

			if(!this.elementAdd){this.elementSelected = _this;}

			this.before(_this);

			this.elementSelected.css({'position':'absolute', 'z-index':'100', 'left': this.dadoX, 'top': this.dadoY});

			this.start(_this);
		},
		container_mousemove: function(e){
			this.position();
			this.dadoX = e.pageX-this.containerPositionX-this.elementPositionX;
			this.dadoY = e.pageY-this.containerPositionY-this.elementPositionY;
			if(this.elementMove == true){this.elementSelected.css({'left': this.dadoX, 'top': this.dadoY});}
			this.move();
		},
		html_mouseup: function(){
			if(this.elementSelected != null || this.elementSelected != undefined){
				this.elementMove = false;
				document.onselectstart = null;
				document.onmousedown = null;

				//Llama funcion al terminar el Drag and Drop
				this.end();
			}
		},
		position: function(){
			this.containerPosition = this.container.offset();
			this.containerPositionX = this.containerPosition.left;
			this.containerPositionY = this.containerPosition.top;
		},
		before: function(_this){
			this.elements.css({'position':'relative', 'z-index':'0', 'top':'0', 'left':'0'});
		},
		start: function(_this){},
		end: function(){},
		move: function(){}
	}

	$.fn['wDado'] = function(o,s){
		var result = [];

		this.each(function($this){
			var $ele = $(this);
			var select = $ele.data('_wDado');

			if(select == undefined || typeof o != 'string'){
				select = new Dado($ele,o);
				$ele.data('_wDado',select);
				result.push($ele);
			}else{
				select = $ele.data('_wDado');
				var ele = select[o];

				if(typeof ele == 'function'){
					ele = select[o](s);
				}else{
					if(s != undefined || s != null){
						select[o] = s;
						result.push($ele);
					}else{
						ele = select[o];
						result.push(ele);
					}
				}
			}
		})

		return $.each(result,function(k,v){return v;});
	}
})(jQuery);