document.addEventListener('presentationInit', function(){
	var slide = app.slide.SE_shoppingcart = {
		onEnter:function(ele){
			slide.element.submitEmail = ele.querySelector("#emailbutton");
			slide.element.this_slide = ele;
			slide.element.pdfSelectArea = ele.querySelector('#all_CartPdfs');
			slide.element.linkSelectArea = ele.querySelector('#all_CartLinks');
			slide.element.pubSelectArea = ele.querySelector('#all_CartPubs');

			//Start draggy for Full PDF list
			slide.element.fullRefList = ele.querySelector('.allPdfContainer');
			slide.element.fullRefListPdfs = ele.querySelectorAll('.allCartPdfs li');

			var oneRefHeight = slide.element.fullRefListPdfs[0].getBoundingClientRect().height;
			var allRefHeight = slide.element.fullRefListPdfs.length * oneRefHeight;
			var refContainerHeight = slide.element.fullRefList.offsetHeight;
			this.scrollLimitFull = refContainerHeight - allRefHeight;

			if(this.scrollLimitFull < 0){
				slide.fullRefList = new Draggy('all_CartPdfs', {restrictX:true, limitsY:[this.scrollLimitFull - 10, 0]});
			}
			//End draggy for Full PDF list

			//Start draggy for Full PUB list
			slide.element.fullPubList = ele.querySelector('.allPubContainer');
			slide.element.fullPubListPdfs = ele.querySelectorAll('.allCartpubs li');

			oneRefHeight = slide.element.fullPubListPdfs[0].getBoundingClientRect().height;
			allRefHeight = slide.element.fullPubListPdfs.length * oneRefHeight;
			refContainerHeight = slide.element.fullPubList.offsetHeight;
			this.scrollLimitPubFull = refContainerHeight - allRefHeight;

			if(this.scrollLimitPubFull < 0){
				slide.fullPubList = new Draggy('all_CartPubs', {restrictX:true, limitsY:[this.scrollLimitPubFull - 10, 0]});
			}
			//End draggy for Full PUB list

			app.addEvent('tap', slide.togglePdfs, slide.element.pdfSelectArea);
			app.addEvent('tap', slide.togglePubs, slide.element.pubSelectArea);
			app.addEvent('tap', slide.togglelinks, slide.element.linkSelectArea);
			app.addEvent('tap', slide.sendEmail, slide.element.submitEmail);

			cart.updateCartList();
			slide.updateDynamicDraggy();
			slide.updateDynamicPubDraggy();
			setTimeout(function(){
				slide.updateFullRefList();
				slide.updateFullPubList();
				slide.updateFullLinkList();
			}, 100);
		},
		onExit:function(ele){
			if(slide.scrollLimit < 0){
				slide.refList.reset();
			}
			if(this.scrollLimitFull < 0){
				slide.fullRefList.reset();
			}
			if(this.scrollLimitPubFull < 0){
				slide.fullPubList.reset();
			}
			if(slide.scrollPubLimit < 0){
				slide.pubList.reset();
			}
		},
		sendEmail:function(e){
			e.preventDefault();
			var email = slide.element.this_slide.querySelector('#emailaddress').value;
			if(email.length === 0){
				alert("Por favor ingrese su dirección de mail");
			}
			else{
				cart.sendData(e);
			}
		},
		updateDynamicDraggy:function(){
			setTimeout(function(){
				slide.element.list = slide.element.this_slide.querySelector('.pdf-list');
				slide.element.allPdfs = slide.element.this_slide.querySelectorAll('.pdf-list li');
				if(slide.element.allPdfs.length > 0){
					var oneRefHeight = slide.element.allPdfs[0].getBoundingClientRect().height;
					var allRefHeight = slide.element.allPdfs.length * oneRefHeight;
					var refContainerHeight = slide.element.list.offsetHeight;
					slide.scrollLimit = refContainerHeight - allRefHeight;

					if(slide.scrollLimit < 0){
						slide.refList = new Draggy('cartPdfs', {restrictX:true, limitsY:[slide.scrollLimit - 20, 0]});
					}
				}
			}, 100);
		},
		updateDynamicPubDraggy:function(){
			setTimeout(function(){
				slide.element.plist = slide.element.this_slide.querySelector('.pub-list');
				slide.element.allPubs = slide.element.this_slide.querySelectorAll('.pub-list li');
				if(slide.element.allPubs.length > 0){
					var oneRefHeight = slide.element.allPubs[0].getBoundingClientRect().height;
					var allRefHeight = slide.element.allPubs.length * oneRefHeight;
					var refContainerHeight = slide.element.plist.offsetHeight;
					slide.scrollPubLimit = refContainerHeight - allRefHeight;

					if(slide.scrollPubLimit < 0){
						slide.pubList = new Draggy('cartPubPdfs', {restrictX:true, limitsY:[slide.scrollPubLimit - 20, 0]});
					}
				}
			}, 100);
		},
		updateFullRefList:function(){
			var dynPdfList = slide.element.this_slide.querySelectorAll('.pdf-list .pdf-text');

			for(var i = 0; i < slide.element.fullRefListPdfs.length; i++){
				var curPdf = slide.element.fullRefListPdfs[i].getAttribute('data-reference');
				curPdf = curPdf.replace('.pdf', '');

				for(var j = 0; j < dynPdfList.length; j++){
					if(dynPdfList[j].innerHTML === curPdf){
						util.addClass(slide.element.fullRefListPdfs[i], 'selected');
					}
				}
			}
		},
		updateFullPubList:function(){
			var dynPdfList = slide.element.this_slide.querySelectorAll('.pub-list .pdf-text');

			for(var i = 0; i < slide.element.fullPubListPdfs.length; i++){
				var curPdf = slide.element.fullPubListPdfs[i].getAttribute('data-reference');
				curPdf = curPdf.replace('.pdf', '');

				for(var j = 0; j < dynPdfList.length; j++){
					if(dynPdfList[j].innerHTML === curPdf){
						util.addClass(slide.element.fullPubListPdfs[i], 'selected');
					}
				}
			}
		},
		updateFullLinkList:function(){
			var dynLinkList = slide.element.this_slide.querySelectorAll('#cartLinks .small-link');
			var fullLinkList = slide.element.this_slide.querySelectorAll('#all_CartLinks li');

			for(var i = 0; i < fullLinkList.length; i++){
				var curLink = fullLinkList[i].getAttribute('data-href');
				curLink = curLink.split('..');

				for(var j = 0; j < dynLinkList.length; j++){
					if(dynLinkList[j].innerHTML === curLink[1]){
						util.addClass(fullLinkList[i], 'selected');
					}
				}
			}
		},
		togglePdfs:function(e){
			var ele = e.target;
			if(ele.nodeType === 3){
				ele = ele.parentNode;
			}
			if(ele.tagName === 'P'){
				ele = ele.parentNode;
			}

			var doc = ele.getAttribute('data-reference');
			var link = ele.getAttribute('data-PDFlink');

			if(util.hasClass(ele, 'selected')){
				util.removeClass(ele, 'selected');
				cart.removePdf(doc);
				cart.removePDFLink(link);
			}
			else{
				util.addClass(ele, 'selected');
				cart.addPdf(doc);
				cart.addPDFLink(link);
			}
			cart.updateCartList();
			slide.updateDynamicDraggy();
		},
		togglePubs:function(e){
			var ele = e.target;
			if(ele.nodeType === 3){
				ele = ele.parentNode;
			}
			if(ele.tagName === 'P'){
				ele = ele.parentNode;
			}

			var doc = ele.getAttribute('data-reference');
			var link = ele.getAttribute('data-PDFlink');

			if(util.hasClass(ele, 'selected')){
				util.removeClass(ele, 'selected');
				cart.removePubPdf(doc);
				cart.removePubPDFLink(link);
			}
			else{
				util.addClass(ele, 'selected');
				cart.addPubPdf(doc);
				cart.addPubPDFLink(link);
			}
			cart.updateCartList();
			slide.updateDynamicPubDraggy();
		},
		togglelinks:function(e){
			var ele = e.target;
			if(ele.nodeType === 3){
				ele = ele.parentNode;
			}
			if(ele.tagName === 'P'){
				ele = ele.parentNode;
			}

			var link = ele.getAttribute('data-href');

			if(util.hasClass(ele, 'selected')){
				util.removeClass(ele, 'selected');
				cart.removeLink(link);
			}
			else{
				util.addClass(ele, 'selected');
				cart.addLink(link);
			}

			cart.updateCartList();
		}
	};
}); 
