document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide54 = {
        elements: {

        },
        onEnter: function(ele) {

            $("#videoslide54").attr("poster", "content/img/slide54/video1.png");
            $(".wPen-btns").css({ "display": "none" });
            $("#logoRoche").css({ "display": "block" });
            $(".wPen-btns").css({ "display": "none" });
            $("#logo").css({ "display": "none" });
            $("#wPenReferencias").css("display", "none");
            $(".texto_footer").css("display", "none");
        },
        onExit: function() {
            document.getElementById("videoslide54").pause();
            document.getElementById("videoslide54").currentTime = 0;
        }
    };
});