var slides = {
    zoomMode:		false
}

//var zoomSlideActual = "#btnZoom"+idSlideActual; 

function Box(inElement) {
	var self = this;
	this.currentScale = 1;
	this.element = inElement;
	this.position = '1,0';
	this.scale = 1.0;
	this.rotation = 1.0;
	
	this.element.addEventListener('gesturestart', function (e) {
		return self.onGestureStart(e)
	}, false);
	this.element.addEventListener('touchstart', function (e) {
		return self.onTouchStart(e)
	}, false);
}
Box.prototype = {
	get position() {
		return this._position;
	}, set position(pos) {
		this._position = pos;
		var components = pos.split(',');
		var x = components[0];
		var y = components[1];
		this.element.style.webkitTransform = 'translate3d(' + x + 'px, ' + y + 'px, 0)';
	}, get scale() {
		return this._scale;
	}, set scale(newScale) {
		if(newScale > 1){
			slides.zoomMode = true;
			$(".slideshows").css({"background":"white"});
			
		}else{
			slides.zoomMode = false;
			$(".slideshows").css({"background-image":"url(content/img/backgrounds/wave.png)"});
			
		}
			
		this._scale = newScale;
		var pos = this.position;
		var components = pos.split(',');
		var x = components[0];
		var y = components[1];
		this.element.style.webkitTransform = 'translate3d(' + x + 'px, ' + y + 'px, 0) scale('+ newScale + ')';
	}, get x() {
		return parseInt(this._position.split(',')[0]);
	}, set x(inX) {
		var comps = this._position.split(',');
		comps[0] = inX;
		this.position = comps.join(',');
	}, get y() {
		return parseInt(this._position.split(',')[1]);
	}, set y(inY) {
		var comps = this._position.split(',');
		comps[1] = inY;
		this.position = comps.join(',');
	}, onGestureStart: function (e) {
		/*alert("slideActual: "+idSlideActual);
		alert(zoomSlideActual);*/
		e.preventDefault();
		var self = this;
		this.element.addEventListener('gesturechange', function (e) {
			return self.onGestureChange(e)
		}, true);
		this.element.addEventListener('gestureend', function (e) {
			return self.onGestureEnd(e)
		}, true);
		return false;
	},
		onGestureChange: function (e) {
		e.preventDefault();
		newScale = this.currentScale * e.scale;
		//newScale = e.scale;
		$("#btnZoom"+idSlideActual).css("display","none");
		$("#botonMoradoBarras1_"+idSlideActual).css("display","none");
		$("#botonMoradoBarras2_"+idSlideActual).css("display","none");
		$("#botonMoradoBarras3_"+idSlideActual).css("display","none");
		$("#btnHR47_2").css("display","none");
		$("#btnHR53_2").css("display","none");
		if (newScale < 1) {
			$("#btnZoom"+idSlideActual).css("display","block");
			$("#botonMoradoBarras1_"+idSlideActual).css("display","block");
			$("#botonMoradoBarras2_"+idSlideActual).css("display","block");
			$("#botonMoradoBarras3_"+idSlideActual).css("display","block");
			if(idSlideActual=="slide4"){
				$("#btnHR47_2").css("display","block");
				$("#btnHR53_2").css("display","block");
			}
			this.position = '1,1';
			newScale = 1;
		} else if (newScale > 2) {
			newScale = 2;
		}
		this.scale = newScale;
		return false;
	},
	onGestureEnd: function (e) {
		e.preventDefault();
		this.currentScale = this.scale;
		this.element.removeEventListener('gesturechange', this.gestureChangeHandler, true);
		this.element.removeEventListener('gestureend', this.gestureEndHandler, true);
		return false;
	},
	onTouchStart: function (e) {
		if (e.targetTouches.length != 1) {
			return;
		}
		this.startX = e.targetTouches[0].clientX;
		this.startY = e.targetTouches[0].clientY;
		var self = this;
		this.element.addEventListener('touchmove', function (e) {
			return self.onTouchMove(e)
		}, false);
		this.element.addEventListener('touchend', function (e) {
			return self.onTouchEnd(e)
		}, false);
	},
	onTouchMove: function (e) {
		e.preventDefault();
		if (e.targetTouches.length != 1) {
			return false;
		}
		var leftDelta = e.targetTouches[0].clientX - this.startX;
		var topDelta = e.targetTouches[0].clientY - this.startY;
		var newLeft = (this.x) + leftDelta;
		var newTop = (this.y) + topDelta;
		var widthElement = $(this.element).width();
		var heightElement = $(this.element).height();
		if (newLeft < (-widthElement)) {
			newLeft = (-widthElement);
		} else if (newLeft > widthElement) {
			newLeft = widthElement;
		}
		if (newTop < (-heightElement)) {
			newTop = (-heightElement);
		} else if (newTop > heightElement) {
			newTop = heightElement;
		}
		this.startX = e.targetTouches[0].clientX;
		this.startY = e.targetTouches[0].clientY;
		if (this.scale > 1) {
			this.position = newLeft + ',' + newTop;
		}
		this.scale = this.scale;
		var currentScale = this.scale;

		return false;
		e.preventDefault();
	},
	onTouchEnd: function (e) {
		e.preventDefault();
		if (e.targetTouches.length > 0) {
			return false;
		}
		this.element.removeEventListener('touchmove', function (e) {
			return self.onTouchMove(e)
		}, false);
		this.element.removeEventListener('touchend', function (e) {
			return self.onTouchEnd(e)
		}, false);
		return false;
	}
}

function loaded() {
	$('article').addClass('gestureZoom');
	$('.gestureZoom').each(function () {
		new Box(this);
	});
}
$(window).load(function () {
	loaded();
});