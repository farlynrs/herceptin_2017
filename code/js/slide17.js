document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide17 = {
        elements: {

        },
        onEnter: function(ele) {
            $("#slide17 #zoom").on("tap", function() {
                $("#slide17 #fondoPopup_slide17").fadeIn();
            });

            $("#slide17 #btnCierraZoom").on("tap", function() {
                $("#slide17 #fondoPopup_slide17").fadeOut();
            });
            $(".wPen-btns").css({ "display": "none" });
            $("#logoRoche").css({ "display": "block" });
            $(".wPen-btns").css({ "display": "none" });
            $("#logo").css({ "display": "block" });
            $("#wPenReferencias").css("display", "none");

        },
        onExit: function() {

        }
    };
});