
document.addEventListener('presentationInit', function() {

  	var slide = app.slide.slide46 = {
		elements: {
			
		},
		onEnter: function(ele) {
			$("#wPenBtns").css({"display":"none"});
			$("#slide46 #lupa1").on("tap",function(){
				$("#slide46 #popUpLupa1").fadeIn();
			});
			$("#slide46 #cierraLupa1").on("tap",function(){
				$("#slide46 #popUpLupa1").fadeOut();
			});

			$("#slide46 #lupa2").on("tap",function(){
				$("#slide46 #popUpLupa2").fadeIn();
			});
			$("#slide46 #cierraLupa2").on("tap",function(){
				$("#slide46 #popUpLupa2").fadeOut();
			});

			$("#slide46 #lupa3").on("tap",function(){
				$("#slide46 #popUpLupa3").fadeIn();
			});
			$("#slide46 #cierraLupa3").on("tap",function(){
				$("#slide46 #popUpLupa3").fadeOut();
			});
		},


		onExit: function(){
			$("#slide46 #popUpLupa1").fadeOut();
$("#slide46 #popUpLupa2").fadeOut();
$("#slide46 #popUpLupa3").fadeOut();
			$("#logo").css({"display":"block"});
			$("#wPenBtns").css("display","block");
		}
	}; 

}); 