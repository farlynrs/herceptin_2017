document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide6 = {
        elements: {

        },
        onEnter: function(ele) {
            $("#slide6 #slide6img1").on("tap", function() {
                $("#slide6 #burbuja1").fadeIn();
            });
            $("#slide6 #slide6img2").on("tap", function() {
                $("#slide6 #burbuja2").fadeIn();
            });
            $("#slide6 #slide6img3").on("tap", function() {
                $("#slide6 #burbuja3").fadeIn();
            });

            $("#slide6 #burbuja1").on("tap", function() {
                $("#slide6 #burbuja1").fadeOut();
            });
            $("#slide6 #burbuja2").on("tap", function() {
                $("#slide6 #burbuja2").fadeOut();
            });
            $("#slide6 #burbuja3").on("tap", function() {
                $("#slide6 #burbuja3").fadeOut();
            });

            $(".wPen-btns").css({ "display": "none" });
            $("#logoRoche").css({ "display": "block" });
            $(".wPen-btns").css({ "display": "none" });
            $("#logo").css({ "display": "block" });
            $("#wPenReferencias").css("display", "none");

        },
        onExit: function() {
            $(".texto_footer").css("display", "block");
        }
    };
});