document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide53 = {
        elements: {

        },
        onEnter: function(ele) {
            $("#enviaIntroducion").on("tap", function() {
                app.goTo('storyboard', 'slide53', 'slide54');
            });
            $("#enviaPreparacion").on("tap", function() {
                app.goTo('storyboard', 'slide53', 'slide55');
            });
            $("#enviaAdministra").on("tap", function() {
                app.goTo('storyboard', 'slide53', 'slide56');
            });

            $("#videoslide53").attr("poster", "content/img/slide53/video1.png");
            $(".wPen-btns").css({ "display": "none" });
            $("#logoRoche").css({ "display": "block" });
            $(".wPen-btns").css({ "display": "none" });
            $("#logo").css({ "display": "none" });
            $("#wPenReferencias").css("display", "none");
            $(".texto_footer").css("display", "none");
        },
        onExit: function() {
            $(".texto_footer").css("display", "none");
            document.getElementById("videoslide53").pause();
            document.getElementById("videoslide53").currentTime = 0;
        }
    };
});