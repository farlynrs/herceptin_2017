document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide4 = {
        elements: {

        },
        onEnter: function(ele) {
            $("#slide4 #zoom").on("tap", function() {
                $("#slide4 #fondoPopup").fadeIn();
            });

            $("#slide4 #btnCierraZoom").on("tap", function() {
                $("#slide4 #fondoPopup").fadeOut();
            });
            $(".wPen-btns").css({ "display": "none" });
            $("#logoRoche").css({ "display": "block" });
            $(".wPen-btns").css({ "display": "none" });
            $("#logo").css({ "display": "block" });
            $("#wPenReferencias").css("display", "none");

        },
        onExit: function() {
$(".texto_footer").css("display","block");
        }
    };
});