document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide58 = {
        elements: {

        },
        onEnter: function(ele) {
            $(".btnHeterogeneidad").on("tap", function() {
                app.goTo('storyboard', 'slide58', 'slide58');
            });
            $(".btnPreparacion").on("tap", function() {
                app.goTo('storyboard', 'slide58', 'slide59');
            });
            $(".btnReaccion").on("tap", function() {
                app.goTo('storyboard', 'slide58', 'slide60');
            });
            $(".btnDuracion").on("tap", function() {
                app.goTo('storyboard', 'slide58', 'slide61');
            });
            $(".wPen-btns").css({ "display": "none" });
            $("#logoRoche").css({ "display": "block" });
            $(".wPen-btns").css({ "display": "none" });
            $("#logo").css({ "display": "block" });
            $("#wPenReferencias").css("display", "none");
            $(".texto_footer").css("display", "block");
        },
        onExit: function() {

        }
    };
});