
document.addEventListener('presentationInit', function() {

  	var slide = app.slide.slide32 = {
		elements: {
			
		},
		onEnter: function(ele) {
			$(".wPen-btns").css({"display":"none"});
			$("#logoKadcyla").css({"display":"none"});	
				$("#slide32 #lupa1").on("tap", function() {
                $("#slide32 #popUpLupa1").fadeIn();       
            });
            $("#slide32 #cierraLupa1").on("tap", function() {
                $("#slide32 #popUpLupa1").fadeOut();       
            });

            $("#slide32 #lupa2").on("tap", function() {
                $("#slide32 #popUpLupa2").fadeIn();       
            });
            $("#slide32 #cierraLupa2").on("tap", function() {
                $("#slide32 #popUpLupa2").fadeOut();       
            });

            $("#slide32 #lupa3").on("tap", function() {
                $("#slide32 #popUpLupa3").fadeIn();       
            });
            $("#slide32 #cierraLupa3").on("tap", function() {
                $("#slide32 #popUpLupa3").fadeOut();       
            });
		},
		onExit: function(){
			
		}
	};  
}); 