document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide15 = {
        elements: {

        },
        onEnter: function(ele) {
            $("#slide15 #zoom").on("tap", function() {
                $("#slide15 #fondoPopup_slide15").fadeIn();
            });

            $("#slide15 #btnCierraZoom").on("tap", function() {
                $("#slide15 #fondoPopup_slide15").fadeOut();
            });

        },
        onExit: function() {

        }
    };
});