(function(){
	'use strict';
	var builder = {},
		slidesMap = {
			efficacy: ['efficacy'],
			safety: ['safety'],
			patientprofile: ['patientprofile'],
			moa: ['moa'],
			fasttrack: ['fasttrack'],
		}, neededSlidesList, chaptersList, presentationName, presentationWelcomeText, builderSlideElement, wasTransfered = false;

	function transferSlidesMap(){
		var slideshow;
		for(slideshow in slidesMap){
			if(slidesMap.hasOwnProperty(slideshow)){
				slidesMap[slideshow] = slidesMap[slideshow].map(function(slideshow){
					return app.json.structures[slideshow].content;
				}).reduce(function(listA, listB){
					return listA.concat(listB);
				});
			}
		}
	}

	function loadSlidesThumbInList(){
		var slideshow, slidesList;
		for(slideshow in slidesMap){
			if(slidesMap.hasOwnProperty(slideshow)){
				slidesList = chaptersList.filter(function(list){
					return list.getAttribute('data-slideshow-id') === slideshow;
				})[0].children[0];
				slidesMap[slideshow].forEach(function(slideId){
					var thumb = document.createElement('li');
					thumb.style.background = 'white url(content/img/thumbs/' + slideId + '.jpg) no-repeat';
					thumb.setAttribute('data-slide-id', slideId);
					thumb.setAttribute('data-slideshow', slideshow);
					thumb.addEventListener('tap', function(){
						if(!thumb.hasClass('was-added')){
							thumb.toggleClass('active');
							thumb.draggable = !thumb.draggable;
						}
					});
					dragDealer.elements.push(thumb);
					slidesList.appendChild(thumb);
				});
			}
		}
	}

	function getElements(slideElement){
		builderSlideElement = slideElement;
		chaptersList = slideElement.getElementsByClassName('slides-list');
		neededSlidesList = slideElement.getElementsByClassName('needed-slides-line')[0].children[0];
		presentationName = document.getElementsByName('presentation-name')[0];
		presentationWelcomeText = document.getElementsByName('welcome-text')[0];
	}

	function fieldsIsValid(){
		return neededSlidesList.children.length > 1;
	}

	function getNewSlidesMap(){
		var map = {}, slideList = [];
		neededSlidesList.children.slice(0, -1).forEach(function(thumb){
			slideList.push(thumb.getAttribute('data-slide-id'));
		});
		return slideList;
	}

	function initializeControls(panel){
		var playButton = panel.children[1],
			saveButton = panel.children[0];
		playButton.onclick = function(){
			if(fieldsIsValid()){
				builder.savePresentation();
				builder.playPresentation();
			}
		};
		saveButton.onclick = function(){
			if(fieldsIsValid()){
				builder.savePresentation();
				alert("Presentation saved");
			}
		};
	}

	function getNormalDate(format){
		var date = Date().split(' ').slice(1, 4), buffer;
		buffer = ('0' + (['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'].indexOf(date[0]) + 1)).slice(-2);
		return format.replace('y', date[2]).replace('m', buffer).replace('d', date[1]);
	}

	function getNormalFullDate(){
		return getNormalDate('y-m-d') + ' ' + Date().split(' ')[4].slice(0, -3);
	}

	function getPresentation(id){
		var avastinStorage = JSON.parse(localStorage.getItem('avastin-global-2'));
		return avastinStorage.presentations.filter(function(presentation){
			return presentation.id === id;
		})[0];
	}

	builder.loadSlides = function(slideElement){
		this.id = new Date().getTime();
		if(!wasTransfered){
			transferSlidesMap();
			wasTransfered = true;
		}
		getElements(slideElement);
		loadSlidesThumbInList();
		initializeControls(slideElement.getElementsByClassName('main-buttons')[0]);
		dragDealer.initialize(slideElement);
	};
	builder.savePresentation = function(){
		var avastinStorage = JSON.parse(localStorage.getItem('avastin-global-2')) || {presentations: []},
			newPresentationEntry = {
				name: presentationName.value || getNormalFullDate(),
				date: getNormalDate('d-m-y'),
				welcome: presentationWelcomeText.value || ' ',
				map: getNewSlidesMap(),
				id: this.id,
				hasAutoName: !presentationName.value
			}, i, wasAdded = false;
		for(i = 0; i < avastinStorage.presentations.length; i++){
			if(avastinStorage.presentations[i].id === this.id){
				avastinStorage.presentations[i] = newPresentationEntry;
				wasAdded = true;
				break;
			}
		}
		if(!wasAdded){
			avastinStorage.presentations.push(newPresentationEntry);
		}
		localStorage.setItem('avastin-global-2', JSON.stringify(avastinStorage));
	};
	builder.playPresentation = function(id){
		document.location.href = 'index.html?puid=' + (id || this.id);
	};
	builder.showPresentation = function(id){
		var presentation = getPresentation(id);
		this.id = id;
		presentation.map.forEach(function(slideId){
			dragDealer.coordX = dragDealer.dropZone.left;
			dragDealer.coordY = dragDealer.dropZone.top;
			dragDealer.createFakeElement(dragDealer.coordX, dragDealer.coordY, builderSlideElement.querySelector('[data-slide-id="' + slideId + '"]'));
			dragDealer.endEvent();
		});
		if(!presentation.hasAutoName){
			presentationName.value = presentation.name;
		}
		if(presentation.welcome !== '' && presentation.welcome !== ' '){
			presentationWelcomeText.value = presentation.welcome;
		}
	};
	builder.checkIfNeedToLoadPresentation = function(){
		var params = window.location.search, puidReg = /puid=([0-9]+)/, id, presentation;
		if(params && puidReg.test(params)){
			id = parseInt(puidReg.exec(params)[1], 10);
			presentation = getPresentation(id);
			app.slideshows['home'].content = app.slideshows['home'].content.slice(0, 1).concat(presentation.map);
			sessionStorage.setItem('customWelcomeText', presentation.welcome);
		}
	};
	window.builder = builder;
})();