document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide11 = {
        elements: {
            
        },
        onEnter: function(ele) {
           $("#wPenBtns").css({ "display": "none" });
            $("#slide11 #img_play").on("tap", function() {
                $("#slide11 #videoSlide11").fadeIn();
                document.getElementById("slide11Vid").play();
                $(".texto_footer").css({ "display": "none" });
                $("#logo").css({ "display": "none" });
            });
            $("#slide11 #cierraLupa1").on("tap", function() {
                $("#slide11 #popUpLupa1").fadeOut();
            });
            $(".wPen-btns").css({"display":"none"});
            $("#logoRoche").css({"display":"block"});   
            $(".wPen-btns").css({"display":"none"});
            $("#logo").css({"display":"block"});
            $("#wPenReferencias").css("display","none");
            
        },
        onExit: function(){
           document.getElementById("slide11Vid").pause();
            document.getElementById("slide11Vid").currentTime = 0;
            $("#slide11 #videoSlide11").fadeOut();
            $(".texto_footer").css("display","block");
        }
    };  
}); 