document.addEventListener('presentationInit', function() {
  app.slide.SE_online = {
    onEnter: function(ele) {
      this.wrapper = ele.querySelector('#sites_link');
      this.wrapper.addEventListener('tap', this.toggleLink);

      //Checking through sessionstorage to if choices have been checked before and if so resets them.
      var allLinks = ele.querySelectorAll('.accessLink');
      var links = sessionStorage.links.split('; ');

      for (var i=0, len=links.length -1; i<len; i++) {
        for (var j = 0; j < allLinks.length; j++) {
          var savedLink = links[i].split('..');
          var everyLink = allLinks[j].getAttribute('data-href').split('..');
          if(savedLink[0] === everyLink[0]){
            allLinks[j].setAttribute('data-state', 'chosen');
          }
        }
      }
    },
    onExit: function(ele) {
    },
    toggleLink: function(e) {
      e.preventDefault();
      var ele = e.target;
      if (ele.nodeType === 3) {
        ele = ele.parentNode;
      }
      var link = ele.getAttribute('data-href');
      var state = ele.getAttribute('data-state');
      if (link) {
        if (state === 'notChosen') {
          cart.addLink(link);
          ele.setAttribute('data-state', 'chosen');
        }
        else {
          cart.removeLink(link);
          ele.setAttribute('data-state', 'notChosen');
        }
      }
    }
  };  
}); 