document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide38 = {
        elements: {

        },
        onEnter: function(ele) {

            $("#slide38_btnZoom").on("tap", function() {
                $("#fondoPopupSlide38").fadeIn();
            });

            $("#slide38_btnCierra_zoom").on("tap", function() {
                $("#fondoPopupSlide38").fadeOut();
            });
$(".wPen-btns").css({ "display": "none" });
            $("#logoRoche").css({ "display": "block" });
            $(".wPen-btns").css({ "display": "none" });
            $("#logo").css({ "display": "block" });
            $("#wPenReferencias").css("display", "none");
        },
        onExit: function() {
$(".texto_footer").css("display", "block");
        }
    };
});