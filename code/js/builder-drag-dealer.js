(function(){
	'use strict';
	var dragDealer = {
			elements: []
		}, createDraggableElement;

	function addRemovingButton(thumb){
		var removeButton = document.createElement('button');
		removeButton.onclick = function(){
			var thumb = this.parentNode,
				originalThumb = document.querySelector('li[data-slide-id="' + thumb.getAttribute('data-slide-id') + '"].was-added');

			// Checks to make see that if the removed slide is part of fasttrack and if so removes it.
			for (var i = 0; i < app.slideshows.fasttrack.content.length; i++) {
				if(app.slideshows.fasttrack.content[i] === originalThumb.getAttribute('data-slide-id')){
					var fasttrackThumbs = document.querySelectorAll('li[data-slideshow="fasttrack"]');
					for (var i = 0; i < fasttrackThumbs.length; i++) {
						if(originalThumb.getAttribute('data-slide-id') === fasttrackThumbs[i].getAttribute('data-slide-id')){
							var fasttrackThumb = fasttrackThumbs[i];
							fasttrackThumb.className = '';
						}
					};
				}
			};
			
			originalThumb.className = '';
			thumb.parentNode.removeChild(thumb);
		};
		thumb.appendChild(removeButton);
	}

	createDraggableElement = (function(){
		var activeElement, eventController;
		eventController = {
			handleEvent: function(event){
				var target;
				if(event.target.hasClass('can-be-draggable')){
					switch(event.type){
						case touchy.events.start:
							this.wasTouched = true;
							break;
						case touchy.events.move:
							if(this.wasTouched){
								this.wasTouched = false;
								if(event.changedTouches){
									event = event.changedTouches[0];
								}
								target = event.target;
								dragDealer.createFakeElement(event.clientX, event.clientY, target);
								dragDealer.realElements = document.querySelectorAll('[data-slide-id="' + target.getAttribute('data-slide-id') + '"].active.was-added');
								dragDealer.realElements.forEach(function(element){
									element.removeClass('active').removeClass('was-added');
								});
								dragDealer.wasTouched = true;
								setTimeout(function(){
									target.addClass('invisible'); //INVISIBLE! Everyone wants to be... Invisible! To hide, To hide
								}, 0);
							}
							break;
					}
				}
			}
		};
		return function(element){
			element.addEventListener('tap', function(){
				if(activeElement){
					activeElement.removeClass('can-be-draggable');
				}
				activeElement = this.toggleClass('can-be-draggable');
			});
			['start', 'move'].forEach(function(event){
				element.addEventListener(touchy.events[event], eventController, false);
			});
		};
	})();
	

	dragDealer.initialize = function(slideElement){
		var dropZoneElement;
		this.parentElement = slideElement;
		['start', 'move', 'end'].forEach(function(event){
			slideElement.addEventListener(touchy.events[event], this, false);
		}, this);
		dropZoneElement = slideElement.getElementsByClassName('needed-slides-line')[0];
		this.dropZone = dropZoneElement.getBoundingClientRect();
		this.dropZoneElement = dropZoneElement.children[0];
		this.thumbPlaceHolder = this.dropZoneElement.children[this.dropZoneElement.children.length - 1];
	};
	dragDealer.handleEvent = function(event){
		switch(event.type){
			case touchy.events.start:
				this.wasTouched = true;
				break;
			case touchy.events.move:
				if(this.wasTouched){
					this.moveEvent(event);
				}
				break;
			case touchy.events.end:
				this.wasTouched = false;
				this.endEvent();
				break;
		}
	};
	dragDealer.moveEvent = function(event){
		if(event.changedTouches){
			event = event.changedTouches[0];
		}
		if((this.elements.indexOf(event.target) !== -1 && event.target.draggable) || event.target === this.fakeElement || event.target.hasClass('can-be-draggable')){
			this.coordX = event.clientX;
			this.coordY = event.clientY;
			if(!this.fakeElement){
				this.createFakeElement(event.clientX, event.clientY, event.target);
			}else{
				this.setElementTranslate(event.clientX, event.clientY);
				this.thumbPlaceHolder.addClass('highlight');
				this.checkIfElementInDropZone();
			}
		}
	};
	dragDealer.endEvent = function(){
		var invisibleElements;
		if(this.fakeElement){
			if(this.elementIsInArea()){
				this.dropZoneElement.insertBefore(this.fakeElement, this.neighbourElement || this.thumbPlaceHolder);
				this.setElementTranslate(0, 0);
				this.fakeElement.removeClass('fake-drag-element');
				this.realElements.forEach(function(element){
					element.addClass('was-added').addClass('active');
					element.draggable = false;
				});
				if(this.neighbourElement){
					this.neighbourElement.removeClass('near-active-thumb');
					this.neighbourElement = null;
				}
				addRemovingButton(this.fakeElement);
				createDraggableElement(this.fakeElement);
			}else{
				this.parentElement.removeChild(this.parentElement.lastChild);
			}
			this.thumbPlaceHolder.removeClass('highlight');
			this.fakeElement = null;
		}
		invisibleElements = this.dropZoneElement.getElementsByClassName('invisible');
		if(invisibleElements){
			invisibleElements.forEach(function(element){
				this.dropZoneElement.removeChild(element);
			}, this);
		}
	};
	dragDealer.createFakeElement = function(x, y, target){
		var element = this.fakeElement = document.createElement('li'),
			dataSlideId = target.getAttribute('data-slide-id');
		element.className = 'fake-drag-element';
		element.style.cssText = 'background-color: white; background-image: ' + target.style.backgroundImage;
		element.setAttribute('data-slide-id', dataSlideId);
		element.setAttribute('data-slideshow', target.getAttribute('data-slideshow'));
		this.realElements = this.parentElement.querySelectorAll('[data-slide-id="' + dataSlideId + '"]');
		target.parentNode.scroll.scrolling = false;
		this.setElementTranslate(x, y);
		this.parentElement.appendChild(element);
	};
	dragDealer.setElementTranslate = function(x, y){
		this.fakeElement.style.cssText += '-webkit-transform: translate3d(' + x + 'px, ' + y + 'px, 0);';
	};
	dragDealer.highlightDropZone = function(){
		this.dropZoneElement.children[this.dropZoneElement.children.length - 1].addClass('highlight');
	};
	dragDealer.checkIfElementInDropZone = function(){
		var i, leftNeighbourCoord, rightNeighbourCoord, childrenCount;
		if(this.neighbourElement){
			this.neighbourElement.removeClass('near-active-thumb');
		}
		if(this.elementIsInArea()){
			childrenCount = this.dropZoneElement.children.length;
			for(i = 0; i < childrenCount; i++){
				leftNeighbourCoord = this.dropZoneElement.children[i].getBoundingClientRect().left;
				if(leftNeighbourCoord < this.coordX && i + 1 < childrenCount){
					rightNeighbourCoord = this.dropZoneElement.children[i + 1].getBoundingClientRect().right;
					if(rightNeighbourCoord > this.coordX + 80){
						this.neighbourElement = this.dropZoneElement.children[i + 1].addClass('near-active-thumb');
						break;
					}
				}
			}
		}
	};
	dragDealer.elementIsInArea = function(){
		return this.coordX >= this.dropZone.left && this.coordX <= this.dropZone.right && this.coordY >= this.dropZone.top && this.coordY <= this.dropZone.bottom;
	};
	window.dragDealer = dragDealer;
})();