document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide34 = {
        elements: {

        },
        onEnter: function(ele) {

            $("#slide34 #abreVideo").on("tap", function() {
                $("#slide34 #videoSlide34").fadeIn();
                document.getElementById("slide34Vid").play();
                $(".texto_footer").css({ "display": "none" });
                $("#logo").css({ "display": "none" });
            });
            $("#slide34 #cierraLupa1").on("tap", function() {
                $("#slide34 #popUpLupa1").fadeOut();
            });
            $(".wPen-btns").css({ "display": "none" });
            $("#logoRoche").css({ "display": "block" });
            $(".wPen-btns").css({ "display": "none" });
            $("#logo").css({ "display": "block" });
            $("#wPenReferencias").css("display", "none");
        },


        onExit: function() {
            $("#slide34 #videoSlide34").fadeOut();
            $(".texto_footer").css({ "display": "block" });
            $("#logo").css({ "display": "block" });
            document.getElementById("slide34Vid").pause();
            document.getElementById("slide34Vid").currentTime = 0;
        }
    };

});