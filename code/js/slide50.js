document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide50 = {
        elements: {

        },
        onEnter: function(ele) {

            $("#slide50_btnZoom").on("tap", function() {
                $("#fondoPopupSlide50").fadeIn();
            });

            $("#slide50_btnCierraZoom").on("tap", function() {
                $("#fondoPopupSlide50").fadeOut();
            });
            $(".wPen-btns").css({ "display": "none" });
            $("#logoRoche").css({ "display": "block" });
            $(".wPen-btns").css({ "display": "none" });
            $("#logo").css({ "display": "block" });
            $("#wPenReferencias").css("display", "none");
        },
        onExit: function() {
            $(".texto_footer").css("display", "block");
        }
    };
});