document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide30 = {
        elements: {

        },
        onEnter: function(ele) {
            $("#slide30 #zoom").on("tap", function() {
                $("#slide30 #fondoPopup_slide30").fadeIn();
            });

            $("#slide30 #btnCierraZoom").on("tap", function() {
                $("#slide30 #fondoPopup_slide30").fadeOut();
            });
            $(".wPen-btns").css({ "display": "none" });
            $("#logoRoche").css({ "display": "block" });
            $(".wPen-btns").css({ "display": "none" });
            $("#logo").css({ "display": "block" });
            $("#wPenReferencias").css("display", "none");

        },
        onExit: function() {

        }
    };
});