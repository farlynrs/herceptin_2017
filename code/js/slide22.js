
document.addEventListener('presentationInit', function() {

  	var slide = app.slide.slide22 = {
		elements: {
			
		},
		onEnter: function(ele) {
			$(".wPen-btns").css({"display":"none"});
			$("#logoKadcyla").css({"display":"none"});	
				$("#slide22 #lupa1").on("tap", function() {
                $("#slide22 #popUpLupa1").fadeIn();       
            });
            $("#slide22 #cierraLupa1").on("tap", function() {
                $("#slide22 #popUpLupa1").fadeOut();       
            });

            $("#slide22 #lupa2").on("tap", function() {
                $("#slide22 #popUpLupa2").fadeIn();       
            });
            $("#slide22 #cierraLupa2").on("tap", function() {
                $("#slide22 #popUpLupa2").fadeOut();       
            });

            $("#slide22 #lupa3").on("tap", function() {
                $("#slide22 #popUpLupa3").fadeIn();       
            });
            $("#slide22 #cierraLupa3").on("tap", function() {
                $("#slide22 #popUpLupa3").fadeOut();       
            });
		},
		onExit: function(){
			$(".wPen-btns").css({"display":"block"});
		}
	};  
}); 