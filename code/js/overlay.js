﻿var overlay = {
    drawToolSketchpad: null,
    canvas: null,
    context: null,
    drawTool: null,
    //pencil: null,
    //brush: null,
    //clear: null,
    //close: null,
    //curSize: 3,
    //curColor: "rgb(255,0,0)",
    init: function () {

        this.setElements();

        //this.canvas = document.getElementById('sketchpad');
        //this.context = this.canvas.getContext('2d');

        // attach the touchstart, touchmove, touchend event listeners.
        //this.canvas.addEventListener('touchstart', overlay.draw, false);
        //this.canvas.addEventListener('touchmove', overlay.draw, false);
        //this.canvas.addEventListener('touchend', overlay.draw, false);
        
        document.body.addEventListener('touchmove', function (event) {
            event.preventDefault();
        }, false);
    },
    setElements: function () {
        this.drawToolSketchpad = document.createElement("div");
        //this.drawTool = document.createElement("div");
        this.drawToolSketchpad.setAttribute('id', 'drawToolSketchpad');
        //this.drawTool.setAttribute('id', 'wPenBtns');
		this.drawTool = document.getElementById('wPenBtns');
        document.body.appendChild(this.drawToolSketchpad);
        //document.body.appendChild(this.drawTool);
        this.drawToolSketchpad.innerHTML = '<canvas id="sketchpad" width="1024" height="768"></canvas>';
        
    }/*,
    // Function which tracks touch movements
    drawer: {
        isDrawing: false,
        touchstart: function (coors) {
            overlay.context.beginPath();
            overlay.context.lineJoin = "round";
            overlay.context.moveTo(coors.x, coors.y);
            this.isDrawing = true;
        },
        touchmove: function (coors) {
            if (this.isDrawing) {
                overlay.context.lineWidth = overlay.curSize;
                overlay.context.strokeStyle = overlay.curColor;
                overlay.context.lineTo(coors.x, coors.y);
                overlay.context.stroke();
            }
        },
        touchend: function (coors) {
            if (this.isDrawing) {
                this.touchmove(coors);
                this.isDrawing = false;
            }
        }
    },
    // Function to pass touch events and coordinates to drawer
    draw: function (event) {
        // get the touch coordinates
        if (event.targetTouches[0]) {
            var coors = {
                x: event.targetTouches[0].pageX,
                y: event.targetTouches[0].pageY
            };
            // pass the coordinates to the appropriate handler
            overlay.drawer[event.type](coors);
        }
    },
    tool: function (toDo) {

        switch (toDo) {
            case "open":
                this.drawToolSketchpad.style.display = "block";
                break;
            case "clear":
                this.context.clearRect(0, 0, 1024, 768);
                break;
            case "close":
                this.context.clearRect(0, 0, 1024, 768);
                this.drawToolSketchpad.style.display = "none";
                util.removeClass(overlay.brush,"selected");
                util.addClass(overlay.pencil,"selected");
                this.curSize = 3;
                this.curColor = "rgb(255,0,0)";
                break;
        }
    }*/
};

overlay.init();