document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide26 = {
        elements: {

        },
        onEnter: function(ele) {
            $("#slide26 #zoom").on("tap", function() {
                $("#slide26 #fondoPopup_slide26").fadeIn();
            });

            $("#slide26 #btnCierraZoom").on("tap", function() {
                $("#slide26 #fondoPopup_slide26").fadeOut();
            });
  $(".wPen-btns").css({ "display": "none" });
            $("#logoRoche").css({ "display": "block" });
            $(".wPen-btns").css({ "display": "none" });
            $("#logo").css({ "display": "block" });
            $("#wPenReferencias").css("display", "none");

        },
        onExit: function() {
            $(".texto_footer").css("display", "block");
        }
    };
});