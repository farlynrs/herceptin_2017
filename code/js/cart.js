var cart = {
  reprints: {
    rep_ico_1: 'Sandler, et al. NEJM 2006',
    rep_ico_2: 'Reck, et al. Ann Oncol 2010',
    rep_ico_3: 'Crino, et al. Lancet Oncol 2010',
    rep_ico_4: 'Wozniak, et al. ASCO 2010'
  },
  addTopic: function(topic) {
    var topics, topicarray;
    topicarray = sessionStorage.topics.split('; ');
    topics = sessionStorage.topics;
    if (topicarray.indexOf(topic) === -1) {
      topics += topic + '; ';
    }
    sessionStorage.topics = topics;
  },
  removeTopic: function(topic) {
    var topics, topicarray, index;
    topicarray = sessionStorage.topics.split('; ');
    index = topicarray.indexOf(topic);
    topics = sessionStorage.topics;
    if (index !== -1) {
      topicarray.splice(index, 1);
      topics = topicarray.join('; ');
    }
    sessionStorage.topics = topics;
  },
  addOtherTopic: function() {
    var ele = document.getElementById('other_theme');
    sessionStorage.otherTopic = ele.value;
  },
  addPdf: function(content) {
    var pdfs, pdfarray;
    pdfarray = sessionStorage.pdfs.split('; ');
    pdfs = sessionStorage.pdfs;
    if (pdfarray.indexOf(content) === -1) {
      pdfs += content + '; ';
    }
    sessionStorage.pdfs = pdfs;
  },
  removePdf: function(content) {
    var pdfs, pdfarray;
    pdfarray = sessionStorage.pdfs.split('; ');
    index = pdfarray.indexOf(content);
    pdfs = sessionStorage.pdfs;
    if (index !== -1) {
      pdfarray.splice(index, 1);
      pdfs = pdfarray.join('; ');
    }
    sessionStorage.pdfs = pdfs;
  },
  addPubPdf: function(content) {
    var pubpdfs, pubpdfarray;
    pubpdfarray = sessionStorage.pubpdfs.split('; ');
    pubpdfs = sessionStorage.pubpdfs;
    if (pubpdfarray.indexOf(content) === -1) {
      pubpdfs += content + '; ';
    }
    sessionStorage.pubpdfs = pubpdfs;
  },
  removePubPdf: function(content) {
    var pubpdfs, pubpdfarray;
    pubpdfarray = sessionStorage.pubpdfs.split('; ');
    index = pubpdfarray.indexOf(content);
    pubpdfs = sessionStorage.pubpdfs;
    if (index !== -1) {
      pubpdfarray.splice(index, 1);
      pubpdfs = pubpdfarray.join('; ');
    }
    sessionStorage.pubpdfs = pubpdfs;
  },
  addPDFLink: function(PDFlink) {
      var PDFlinks, PDFlinkarray;
      if(PDFlink == undefined) { 
            return
      }
      PDFlinkarray = sessionStorage.PDFlinks.split('; ');
      PDFlinks = sessionStorage.PDFlinks;
      if (PDFlinkarray.indexOf(PDFlink) === -1) {
              PDFlinks += PDFlink + '; ';
      }
      sessionStorage.PDFlinks = PDFlinks;
  },
  removePDFLink: function(PDFlink) {
    var PDFlinks, PDFlinkarray;
    PDFlinkarray = sessionStorage.PDFlinks.split('; ');
    index = PDFlinkarray.indexOf(PDFlink);
    PDFlinks = sessionStorage.PDFlinks;
    if (index !== -1) {
      PDFlinkarray.splice(index, 1);
      PDFlinks = PDFlinkarray.join('; ');
    }
    sessionStorage.PDFlinks = PDFlinks;
  },
  addPubPDFLink: function(PDFlink) {
      var PubPDFlinks, PubPDFlinkarray;
      if(PDFlink == undefined) { 
            return
      }
      PubPDFlinkarray = sessionStorage.PubPDFlinks.split('; ');
      PubPDFlinks = sessionStorage.PubPDFlinks;
      if (PubPDFlinkarray.indexOf(PDFlink) === -1) {
              PubPDFlinks += PDFlink + '; ';
      }
      sessionStorage.PubPDFlinks = PubPDFlinks;
  },
  removePubPDFLink: function(PDFlink) {
    var PubPDFlinks, PubPDFlinkarray;
    PubPDFlinkarray = sessionStorage.PubPDFlinks.split('; ');
    index = PubPDFlinkarray.indexOf(PDFlink);
    PubPDFlinks = sessionStorage.PubPDFlinks;
    if (index !== -1) {
      PubPDFlinkarray.splice(index, 1);
      PubPDFlinks = PubPDFlinkarray.join('; ');
    }
    sessionStorage.PubPDFlinks = PubPDFlinks;
  },
  addLink: function(link) {
    var links, linkarray;
    linkarray = sessionStorage.links.split('; ');
    links = sessionStorage.links;
    if (linkarray.indexOf(link) === -1) {
      links += link + '; ';
    }
    sessionStorage.links = links;
  },
  removeLink: function(link) {
    var links, linkarray, index;
    linkarray = sessionStorage.links.split('; ');
    index = linkarray.indexOf(link);
    links = sessionStorage.links;
    if (index !== -1) {
      linkarray.splice(index, 1);
      links = linkarray.join('; ');
    }
    sessionStorage.links = links;
  },
  includeLinks: function(btn) {
    sessionStorage.includeLinks = 'yes';
    $(btn).addClass('transparent');
    $('.linksSaved').removeClass('transparent');
    this.updateCartList();
  },
  updateCartList: function() {
    var links = this.buildLinks(),
        pdflinks = this.buildPDFLinks(),
        pubpdflinks = this.buildPubPDFLinks(),
        topics = this.buildTopics(),
        pdfs = this.buildPdfs(),
        pubpdfs = this.buildPubPdfs(),
        pdflist = document.getElementById('cartPdfs'),
        pubnewlist = document.getElementById('cartPubPdfs');
        topiclist = document.getElementById('cartTopics');
        linklist = document.getElementById('cartLinks');
        //PDFlinklist = document.getElementById('cartPdfLinks');
    topiclist.innerHTML = topics;
    pdflist.innerHTML = pdfs;
    pubnewlist.innerHTML = pubpdfs;
    linklist.innerHTML = links;
    //PDFlinklist.innerHTML = pdflinks;      
  },
  buildTopics: function() {
    var topics = sessionStorage.topics.split('; '),
        markup = '';
    for (var j=0, len=topics.length -1; j<len; j++) {
      var oneWord = topics[j].split(' ');
      oneWord = oneWord[0];
      markup += '<div class="small-ipad '+oneWord+'"></div>';
    }
    // if (sessionStorage.otherTopic && sessionStorage.otherTopic.length > 2) {
    //   markup += 'Other topic: ' + sessionStorage.otherTopic + '<br />';
    // }
    return markup;
  },
  buildEmailTopics: function() {
    var topics = sessionStorage.topics.split('; '),
        markup = '';
    for (var j=0, len=topics.length -1; j<len; j++) {
      markup += topics[j] + '<br />';
    }
    if (sessionStorage.otherTopic && sessionStorage.otherTopic.length > 2) {
      markup += 'Other topic: ' + sessionStorage.otherTopic + '<br />';
    }
    return markup;
  },
  buildPubPdfs: function() {
    var pubpdfs = sessionStorage.pubpdfs.split('; '),
             markup = '';
        
    for (var j=0, len=pubpdfs.length -1; j<len; j++) {
           pubpdfs[j] = pubpdfs[j].replace('.pdf','');
           markup += '<li><p class="pdf-text">' + pubpdfs[j] + '</p></li>';
    }
    return markup;
  },
  buildEmailPubPdfs: function() {
    var pubpdfs = sessionStorage.pubpdfs.split('; '),
        links = sessionStorage.PubPDFlinks.split('; '),
        markup = '';
        
    for (var j=0, len=pubpdfs.length -1; j<len; j++) {
           markup += '<a href=' + links[j] + '>' + pubpdfs[j] + '</a><br />';
    }
    return markup;
  },
  buildPdfs: function() {
    var pdfs = sessionStorage.pdfs.split('; '),
             markup = '';
        
    for (var j=0, len=pdfs.length -1; j<len; j++) {
           pdfs[j] = pdfs[j].replace('.pdf','');
           markup += '<li><p class="pdf-text">' + pdfs[j] + '</p></li>';
    }
    return markup;
  },
  buildEmailPdfs: function() {
    var pdfs = sessionStorage.pdfs.split('; '),
        links = sessionStorage.PDFlinks.split('; '),
        markup = '';
        
    for (var j=0, len=pdfs.length -1; j<len; j++) {
           markup += '<a href=' + links[j] + '>' + pdfs[j] + '</a><br />';
    }
    return markup;
  },
  
  buildPDFLinks: function() {
    var links = sessionStorage.PDFlinks.split('; '),
        markup = '';
    for (var j=0, len=links.length -1; j<len; j++) {
           markup += links[j] + '<br />';
    }
    return markup;
  },
  buildPubPDFLinks: function() {
    var links = sessionStorage.PubPDFlinks.split('; '),
        markup = '';
    for (var j=0, len=links.length -1; j<len; j++) {
           markup += links[j] + '<br />';
    }
    return markup;
  },
  
  buildLinks: function() {
    var links = sessionStorage.links.split('; '),
        markup = '';
    for (var j=0, len=links.length -1; j<len; j++) {
      var nameAndLink = links[j].split('..');
      markup += '<li>' + nameAndLink[0] + '<br /><span class="small-link">' + nameAndLink[1] + '</span></li>';
    }
    return markup;
  },
  sendData: function(e) {
    e.preventDefault();
    var data = this.data,
        email = document.getElementById('emailaddress').value,
        customNote = document.getElementById('customNote').value,
        topics = sessionStorage.topics.split('; '),
        pdfs = sessionStorage.pdfs.split('; '),
        pubpdfs = sessionStorage.pubpdfs.split('; '),
        links = sessionStorage.links.split('; '),
        PDFlinks = sessionStorage.PDFlinks.split('; '),
        PubPDFlinks = sessionStorage.PubPDFlinks.split('; '),
        content = 'Estimado(a) Doctor(a): <br /> <br /> Usted optó por recibir la siguiente información de una presentación de Roche Avastin en Cáncer de Pulmón:<br /><br />';
    if (topics.length > 1) {
      content += "<strong>Temas para la próxima visita:</strong><br />";
      content += this.buildEmailTopics();
      content += '<br />';
    }
    if (pdfs.length > 1) {
      content += "<strong>Referencias en PDF:</strong><br />";
      content += this.buildEmailPdfs();
      content += '<br />';
    }
    if (links.length > 1) {
      content += "<strong>Recursos online:</strong><br />";
      content += this.buildLinks();
      content += '<br />';
    }
    if (pubpdfs.length > 1) {
      content += "<strong>Publicaciones y novedades sobre los últimos congresos:</strong><br />";
      content += this.buildEmailPubPdfs();
      content += '<br />';
    }
    /*if (PDFlinks.length > 1) {
      content += "<strong>Links:</strong><br />";
      content += this.buildPDFLinks();
      content += '<br />';
    }*/
    content += '<br />';
    content += customNote + '<br /><br />';
    console.log(content);
    sendMail(email, 'Roche Oncología', content);
  }
};

(function() {

  if (!sessionStorage.topics) { sessionStorage.topics = ''; }
  if (!sessionStorage.pdfs) { sessionStorage.pdfs = ''; }
  if (!sessionStorage.pubpdfs) { sessionStorage.pubpdfs = ''; }
  if (!sessionStorage.links) { sessionStorage.links = ''; }
  if (!sessionStorage.PDFlinks) { sessionStorage.PDFlinks = ''; }
  if (!sessionStorage.PubPDFlinks) { sessionStorage.PubPDFlinks = ''; }

  // Monitor topics
  document.addEventListener('doubleTap', function(e) {
    var topics = sessionStorage.topics.split('; ');
    var otherTopic = sessionStorage.otherTopic;
    topics.forEach(function(topic) {
      if (topic !== '') {
        if(topic == 'Survival'){
          submitUniqueCustomEvent('1014 - Closing - To discuss next time', '1014004 - Topic: ' + topic, 'true');
        }
        else if(topic == 'Control tumour growth'){
          submitUniqueCustomEvent('1014 - Closing - To discuss next time', '1014005 - Topic: Control tumor growth', 'true');
        }
        else if(topic == 'Symptom control'){
          submitUniqueCustomEvent('1014 - Closing - To discuss next time', '1014006 - Topic: ' + topic, 'true');
        }
        else if(topic == 'Quality of life'){
          submitUniqueCustomEvent('1014 - Closing - To discuss next time', '1014003 - Topic: ' + topic, 'true');
        }
        else if(topic == 'Tolerance'){
          submitUniqueCustomEvent('1014 - Closing - To discuss next time', '1014002 - Topic: ' + topic, 'true');
        }
        else if(topic == 'Treatment adherence'){
          submitUniqueCustomEvent('1014 - Closing - To discuss next time', '1014001 - Topic: ' + topic, 'true');
        }
      }
    });
    if (otherTopic) {
      submitUniqueCustomEvent('1014 - Closing - To discuss next time', '1014007 - Custom topic', '' + otherTopic);
    }
  });

})();
