document.addEventListener('presentationInit', function() {

    var slide = app.slide.slide28 = {
        elements: {

        },
        onEnter: function(ele) {
            $("#slide28 #img1").on("tap", function() {
                $("#slide28 #videoSlide28").fadeIn();
                document.getElementById("slide28Vid").play();
                $(".texto_footer").css({ "display": "none" });
                $("#logo").css({ "display": "none" });
            });

$(".wPen-btns").css({ "display": "none" });
            $("#logoRoche").css({ "display": "block" });
            $(".wPen-btns").css({ "display": "none" });
            $("#logo").css({ "display": "block" });
            $("#wPenReferencias").css("display", "none");
        },
        onExit: function() {
            document.getElementById("slide28Vid").pause();
            document.getElementById("slide28Vid").currentTime = 0;
             $("#slide28 #videoSlide28").fadeOut();
             $(".texto_footer").css("display", "block");
        }
    };
});