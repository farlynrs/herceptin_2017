document.addEventListener('presentationInit', function() {
  var slide = app.slide.SE_publication = {
    elements: {
      list: "#allPublications",
      conList: "#allCongress",
      allPublications: ["#allPublications li", "all"]
    },
    onEnter: function(ele) {
      var oneRefHeight = slide.element.allPublications[0].offsetHeight;
      var allRefHeight = slide.element.allPublications.length * oneRefHeight;
      var refContainerHeight = slide.element.list.offsetHeight;
      this.scrollLimit = refContainerHeight - allRefHeight;

      if(this.scrollLimit < 0){
        slide.refList = new Draggy('ref_ul', {restrictX: true, limitsY: [this.scrollLimit-20, 10]});
      }
      app.addEvent('tap', slide.onReferenceClick, slide.element.list);
      app.addEvent('tap', slide.onReferenceClick, slide.element.conList);
    },
    onExit: function(ele) {
      if(this.scrollLimit < 0){
        slide.refList.reset();
      }
    },
    onReferenceClick: function(e) {
      app.refs.openReference(e.target);
    }
  };  
}); 