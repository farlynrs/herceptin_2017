(function(){
	'use strict';
	HTMLElement.prototype.hasClass = function(className){
		var classExist = new RegExp('(^|\\s)' + className + '(\\s|$)', 'g');
		return classExist.test(this.className);
	};
	HTMLElement.prototype.addClass = function(className){
		if(!this.hasClass(className)){
			this.className = (this.className + ' ' + className).replace(/\s+/g, ' ').replace(/(^ | $)/g, ' ');
		}
		return this;
	};
	HTMLElement.prototype.removeClass = function(className){
		var classExist = new RegExp('(^|\\s)' + className + '(\\s|$)', 'g');
		this.className = this.className.replace(classExist, '$1').replace(/\s+/g, ' ').replace(/(^ | $)/g, '');
		return this;
	};
	HTMLElement.prototype.toggleClass = function(className){
		return this.hasClass(className) ? this.removeClass(className) : this.addClass(className);
	};
	HTMLElement.prototype.appendFirstChild = function(node){
		this.firstChild ? this.insertBefore(node, this.firstChild) : this.appendChild(node);
		return this;
	};
	utils.augment(NodeList, Array, 'forEach', 'indexOf', 'filter', 'some', 'every');
	utils.augment(HTMLCollection, Array, 'forEach', 'indexOf', 'filter', 'some', 'every', 'slice');

})();