(function(global){
	'use strict';
	var utils = {};
	utils.ajax = function(url, callback, async){
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = callback;
		xhr.open('GET', url, async);
		xhr.send(null);
	};
	utils.extend = function(sub, sup){
		var F = function(){};
		F.prototype = sup.prototype;
		sub.prototype = new F();
		sub.prototype.constructor = sub;
	};
	utils.augment = function(receiving, giving){
		var i, methodName;
		if(arguments[2]){
			for(i = 2; i < arguments.length; i++){
				receiving.prototype[arguments[i]] = giving.prototype[arguments[i]];
			}
		}else{
			for(methodName in giving.prototype){
				if(receiving.prototype.hasOwnProperty(methodName)){
					receiving.prototype[methodName] = giving.prototype[methodName];
				}
			}
		}
	};
	utils.getElement = function(selector){
		if(selector instanceof HTMLElement){
			return selector;
		}
		if(/[\*\.\#\>\+\:\s\[\]\(\)]/.test(selector)){
			return document.querySelector(selector);
		}else{
			return document.getElementById(selector) || document.getElementsByTagName(selector)[0];
		}
	};
	utils.getElementParent = function(element, parentTag){
		element = this.getElement(element);
		parentTag = parentTag.toUpperCase();
		if(element){
			while(element.tagName !== parentTag){
				element = element.parentNode;
				if(element.tagName === 'BODY'){
					return null;
				}
			}
		}
		return element;
	};
	global.utils = utils;
})(window);