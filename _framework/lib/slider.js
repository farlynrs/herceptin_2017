(function(global){
	'use strict';
	function Slider(element, options){
		var option;
		this.element = element = utils.getElement(element);
		this.handler = document.createElement('div');
		element.appendChild(this.handler);
		this.min = 0;
		this.max = 100;
		this.value = this.min;
		for(option in options){
			if(options.hasOwnProperty(option)){
				this[option] = options[option];
			}
		}
		this.wasTouched = false;
		this.isEnable = true;
		['start', 'move', 'end'].forEach(function(eventName){
			element.addEventListener(touchy.events[eventName], this, false);
		}, this);
		this.onchange = function(){};
		this.onfinalchange = function(){};
		this.refresh();
	}
	Slider.prototype.handleEvent = function(event){
		event.preventDefault();
		event.stopPropagation();
		if(this.isEnable){
			switch(event.type){
				case touchy.events.start:
					this.wasTouched = true;
				case touchy.events.move:
					if(this.wasTouched){
						this.moveEvent(event);
						this.onchange();
					}
					break;
				case touchy.events.end:
					this.wasTouched = false;
					this.onfinalchange();
					break;
			}
		}
	};
	Slider.prototype.moveEvent = function(event){
		var translate;
		if(event.changedTouches){
			event = event.changedTouches[0];
		}
		translate = event['client' + this.axis] - this.elementOffset - this.halfOfHandlerSize;
		this.value = Math.round(this.setHandlerTranslate(translate) / this.fold) + this.min;
	};
	Slider.prototype.setHandlerTranslate = function(translate){
		if(translate < 0){
			translate = 0;
		}else if(translate > this.maxTranslate){
			translate = this.maxTranslate;
		}
		this.handler.style.webkitTransform = 'translate' + this.axis + '(' + translate + 'px)';
		return translate;
	};
	Slider.prototype.setValue = function(value){
		if(value >= this.min && value <= this.max && value !== this.value){
			this.value = value;
			this.setHandlerTranslate((value - this.min) * this.fold);
			this.onchange();
		}
	};
	Slider.prototype.refresh = function(){
		var needPosition, needSize;
		if(this.element.offsetWidth >= this.element.offsetHeight){
			this.axis = 'X';
			needPosition = 'left';
			needSize = 'Width';
		}else{
			this.axis = 'Y';
			needPosition = 'top';
			needSize = 'Height';
		}
		this.maxTranslate = this.element['offset' + needSize] - this.handler['offset' + needSize];
		this.fold = this.maxTranslate / (this.max - this.min);
		this.elementOffset = this.element.getBoundingClientRect()[needPosition] - utils.getElementParent(this.element, 'article').getBoundingClientRect()[needPosition];	
		this.halfOfHandlerSize = this.handler['offset' + needSize] / 2;
		this.setValue(this.value);
	};
	global.Slider = Slider;
})(window);