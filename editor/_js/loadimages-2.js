//LoadImages 2 por Salvador Gonzalez (@sgb004)
function LoadImages(_this,o){
	var _self = this;
	var d = {
		list: new Array(),
		autoload: true,
	}

	if(o != undefined){
		for(e in d){if(o[e] != undefined){d[e] = o[e];}}

		if(typeof o.callback != 'function'){
			delete o.callback;
		}else{
			d.callback = o.callback;
		}
	}

	for(e in d){_self[e] = d[e];}

	if(typeof this.list != 'array'){this.list = new Array();}

	this.imgs = new Array();
	this.image = new Image();
	this.loading = false;
	this.loadingItem = 0;
	this.stop = false;
	
	this.init();
	this.add(_this);

	return this;
}

LoadImages.prototype = {
	init: function(){
		var _this = this;
		this.image.addEventListener('load',function(){
			_this.list[_this.loadingItem].status = 'complete';
			_this.callback(true,_this.list[_this.loadingItem].src,_this.list[_this.loadingItem].status);
			_this.next();
		},false);
		this.image.addEventListener('error',function(){
			_this.list[_this.loadingItem].status = 'error';
			_this.callback(true,_this.list[_this.loadingItem].src,_this.list[_this.loadingItem].status);
			_this.next();
		},false);
	},
	_autoload: function(){
		if(this.autoload){this.load();}
	},
	add_list: function(list){
		var duplicate = false;
		for(var i=0; i<list.length; i++){
			list[i] = list[i].trim();
			duplicate = this.find(list[i]);
			if(!duplicate){this.list.push({'src':list[i],'status':''});}
		}
		this._autoload();
	},
	add: function(container){
		if(typeof container == 'object'){
			var funFind = container.getElementsByTagName;
			var imgs = new Array();
			var src = '';
			var duplicate = false;

			if(container.tagName == 'IMG' || container.tagName == 'img'){
				imgs.push(container);
			}else if(typeof funFind == 'function'){
				imgs = container.getElementsByTagName('img');
			}else if(container.length != undefined){
				for(var i=0; i<container.length; i++){
					this.add(container[i]);
				}
			}
			
			for(var i=0; i<imgs.length; i++){
				src = imgs[i].getAttribute('src');
				src = src.trim();
				duplicate = this.find(src);
				if(!duplicate){this.list.push({'src':src,'status':''});}
				if(imgs[i].LoadImages == undefined){
					imgs[i].LoadImages = '';
					this.imgs.push(imgs[i]);
				}
			}
		}

		this._autoload();
	},
	callback: function(loading,src,status){},
	get_image: function(src){
		var imgSrc = '';
		var imgs = new Array();
		src = src.trim();
		for(var i=0; i<this.imgs.length; i++){
			var imgSrc = this.imgs[i].getAttribute('src');
			if(imgSrc == src){
				imgs.push(this.imgs[i]);
			}
		}
		return imgs;
	},
	get_image_status: function(src){
		var status = '';
		src = src.trim();
		for(var j=0; j<this.list.length; j++){
			if(this.list[j].src == src){
				status = this.list[j].status;
				break;
			}
		}
		return status;
	},
	find: function(src){
		var duplicate = false
		for(var j=0; j<this.list.length; j++){
			if(this.list[j].src == src){
				duplicate = true;
				break;
			}
		}
		return duplicate;
	},
	load: function(){
		this.loading = true;
		this.loadingItem = -1;
		this.stop = false;
			
		this.next();
	},
	next: function(){
		if(!this.stop){
			this.loadingItem += 1;
			if(this.list[this.loadingItem] == undefined){
				this.loading = false;
				this.callback(this.loading,'','');
			}else{
				if(this.list[this.loadingItem].status == ''){
					this.image.src = this.list[this.loadingItem].src;
				}else{
					this.callback(this.loading,this.list[this.loadingItem].src,this.list[this.loadingItem].status);
					this.next();
				}
			}
		}
	}
}