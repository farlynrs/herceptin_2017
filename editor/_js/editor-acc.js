//Variables de configuracion
var appJSON = {};
var appName = 'storyboard';
var appAgenda = 'Avastin';
var JSONURL = '../presentation.json';
var editorThumbsSRC = '../content/img/thumbs/';
var slideThumbsSRC = '../content/img/thumbs/';
var edSelCarousel;

//Otras variables
var wSelectSliders, wSelectSlidersImg, wSliderMenuSelect, wSliderMenuSelectImg, indexCarouselCon, indexCarousel, wSelectSlidersItems, loadImgs, wDialogZoomImg, presentationName, presentation, ordenar, ordenarSliders, wSelectSlidersSeparator, wdialog, wDialogAlertSaveForm, wDialogAlertSaveName, ordenarTopbarNombre, wListItems, wListNone;
var slideDeleteSliders = {length: 0,removeClass: function(){}}
var wSliderMenuType = {};

$(document).on('ready',editor_ready);
$(window).on('hashchange',window_hasChange);

function editor_ready(){
	var indexCarouselConImgs;

	presentation = new Presentation();
	wdialog = new wDialog('#wDialog');

	wSelectSliders = $('#wSelectSliders');
	wSelectSlidersImg = $('#wSelectSlidersImg');
	wSliderMenuSelect = $('#wSliderMenuSelect');
	wSliderMenuSelectImg = $('#wSliderMenuSelectImg');
	indexCarouselCon = $('#indexCarouselCon');
	indexCarousel = $('#indexCarousel');
	wSelectSlidersItems = $('#wSelectSlidersItems');
	wDialogZoomImg = $('#wDialogZoomImg');
	ordenar = $('#ordenar');
	ordenarSliders = $('#ordenarSliders');
	wDialogAlertSaveName = $('#wDialogAlertSaveName');
	ordenarTopbarNombre = $('#ordenarTopbarNombre');
	wListItems = $('#wListItems');
	wListNone = $('#wListNone');

	edSelCarousel = document.getElementById('edSelCarousel');
	edSelCarousel.addEventListener('change',edSelCarousel_change,false);

	jQuery.ajax({
	url: JSONURL,
	async: false,
	dataType: 'json',
		success: function(data){
			var wSliderMenu = $('#wSliderMenu');
			var wSliderMenuHTML = '';
			var indexCarouselHTML = '';
			var wSliderMenuHTMLClass = '';
			appJSON = data;

			if(appJSON.extra != undefined){
				for(var item in appJSON.extra){
					for(var i=0; i<appJSON.extra[item].content.length; i++){
						wSliderMenuType[appJSON.extra[item].content[i]] = item;
					}
				}
			}

			for(var slide in appJSON.structures[appName].content){
				slide = appJSON.structures[appName].content[slide];
				wSliderMenuHTML += '<a class="wSliderMenu-link '+wSliderMenuType[slide]+'" href="" slide="'+slide+'"><span	class="wSliderMenu-link-text">'+appJSON.structures[slide].name+'</span></a>';

				indexCarouselHTML += '<div class="slide"><img class="index-carousel-slide-img" src="'+editorThumbsSRC+'capitulo_'+slide+'.png" slide="'+slide+'"></div>';
			}

			wSliderMenu.html(wSliderMenuHTML);
			indexCarousel.html(indexCarouselHTML);

			$('.wSliderMenu-link').on('click',wSliderMenuLink_click);
			wSliderMenuSelectImg.on('click',wSliderMenuLink_click);
			$('.index-carousel-slide-img').on('tap',wSliderMenuLink_click);
		}
	});

	indexCarouselConImgs = $('#indexCarouselCon img');

	loadImgs = new LoadImages(indexCarouselConImgs,{'callback':indexCarouselConImgs_loading});

	$('#wSelectSlidersShowhidden').on('click',wSelectSlidersShowhidden_click);
	$('#wDialogAlertSaveForm').on('submit',wDialogAlertSaveForm_submit);
	$('#wDialogAlertUpdateReplace').on('click',wDialogAlertUpdateReplace_click);
	$('#wDialogAlertCancel').on('click',wDialogAlertCancel_click);
	$('#wListScroll').mCustomScrollbar();
	$('#abrirCerrarBtn').on('click',abrirCerrarBtn_click);
	$('#guideBack').on('click',guideBack_click);
	$('#guidePDF').on('click',guidePDF_click);
	$(window).bind('orientationchange',window_orientationchange);
	$('#wDialogZoomImgClose').on('click',img_zoom_close);

	window_hasChange();
}

function window_hasChange(){
	var hash = location.hash.trim();
	var hasArrayItem;
	var _GET = {};

	hash = hash.substring(1,hash.length);
	hash = hash.split('&');

	for(var i=0; i<hash.length; i++){
		hasArrayItem = hash[i].split('=');
		_GET[hasArrayItem[0]] = hasArrayItem[1];
	}

	presentation_load(_GET['presentacion']);
	window_change(_GET['v']);
}

function window_change(view){
	$('.content.display-block').removeClass('display-block');
	if(view == undefined || view == ''){view = 'index';}
	$('.content[view="'+view+'"]').addClass('display-block');

	view += '_pag';
	view = window[view];
	if(typeof view == 'function'){view();}
}

//

function index_pag(){
	wSelectSliders.removeClass('display-block');
}

function ordenar_pag(){
	var length = presentation.size();

	if(length == 0){
		ordenar.removeClass('edit').addClass('empty');
		ordenarSliders.removeClass('display-block');
	}else{
		var sliders = presentation.presentation;
		var ordenarSlidersHTML = '';
		var slide;
		var slideBtnDelete;

		for(var k in sliders){
			ordenarSlidersHTML += '<div class="wOrderSliders-separator ver"></div><div class="wOrderSliders-group"><div class="wOrderSliders-section">';
			ordenarSlidersHTML += slider_get_html('', k, '', '',true);
			ordenarSlidersHTML += '</div></div>';
		}

		ordenarSlidersHTML += '<div class="wOrderSliders-separator ver"></div>';
		
		$('#ordenarSliders .wOrderSliders-group, #ordenarSliders .wOrderSliders-separator').remove();
		
		ordenarSliders.prepend(ordenarSlidersHTML);
		ordenar.removeClass('empty');
		
		ordenarSliders.find('.wSelectSliders-slide-zoom').on('mousedown',img_zoom);
		ordenarSliders.find('.wSelectSliders-slide-link').on('click',disabled_click).on('mousedown',disabled_click)

		ordenarSliders.find('.wSelectSliders-slide').on('mousedown',wSelectSlidersSlide_mousedown);

		slideBtnDelete = $('.wOrderSliders-section .wSelectSliders-slide-delete');
		for(var i=0; i<slideBtnDelete.length; i++){
			slideBtnDelete_addEventListeners(slideBtnDelete[i],wSelectSlidersSlideDeleteSliders_click);
		}
	}

	wDialogAlertCancel_click();
}

function slideBtnDelete_addEventListeners(ele,click){
	ele.addEventListener('touchstart',btn_stopPropagation,false);
	ele.addEventListener('click',click,false);
}

function abrir_pag(){
	var pres = localStorage.getItem('links');
	if(pres == null){
		pres = '';
	}else{
		pres = pres.split(',');
	}

	if(pres == ''){
		wListNone.addClass('display-block');
		wListItems.removeClass('display-block');
	}else{
		var wListItemsHTML = '';
		var name = '';
		var names = new Array();
		var date = '';
		var nameApp = '&'+appName+appAgenda;
		var wListItemDate;
		var wListItemsDeleteBtn;
		wListNone.removeClass('display-block');
		wListItems.addClass('display-block');

		for(var i=0; i<pres.length; i++){
		if(pres[i].search(nameApp)!=-1){
			name = pres[i].replace(nameApp,'');
			names.push(name);
			date = pres[i]+'date';
			date = localStorage.getItem(date);
			if(date == null){date = '';}
			wListItemsHTML += '<li class="wList-item"><a class="wList-link" href="#v=ordenar&presentacion='+name+'"><div class="wList-item-name">'+name.replace(/_/g,' ')+'</div><div class="wList-item-date">'+date+'<div class="wList-item-delete-btn" presentation="'+name+'"></div></div></a></li>';
			
		}
		}
		if(wListItemsHTML.length == 0){wListNone.addClass('display-block');}
		else{
		wListItems.html(wListItemsHTML);
		}
		wListItemDate = wListItems.find('.wList-item-date');
		wListItemDate = wListItems.find('.wList-item, .wList-link');
		wListItems.find('.wList-link').on('click',disabled_click);

		for(var i=0; i<wListItemDate.length; i++){
			wListItemDate[i].addEventListener('touchstart',wListItemDate_touchstart,false);
			wListItemDate[i].addEventListener('touchmove',wListItemDate_touchmove,false);
			wListItemDate[i].addEventListener('touchend',wListItemDate_touchend,false);
			wListItemDate[i].addEventListener('mousedown',btn_stopPropagation,false);
			wListItemDate[i].data = {'position':{}};
			//wListItemDate[i].setAttribute('presentation',names[i]);
		}

		wListItemsDeleteBtn = wListItems.find('.wList-item-delete-btn');

		for(var i=0; i<wListItemsDeleteBtn.length; i++){
			slideBtnDelete_addEventListeners(wListItemsDeleteBtn[i],wListItemDeleteBtn_click);
		}
	}
}

function wListItemDate_touchstart(e){
	this.data['position']['start'] = {'x':e.changedTouches[0].pageX, 'y':e.changedTouches[0].pageY};
}

function wListItemDate_touchmove(e){
	this.data['position']['move'] = {'x':e.changedTouches[0].pageX, 'y':e.changedTouches[0].pageY};
}

function wListItemDate_touchend(e){
	var slideLeft = 0;
	var _this = $(this);
	var isLink = _this.hasClass('wList-link');
	this.data['position']['end'] = {'x':e.changedTouches[0].pageX, 'y':e.changedTouches[0].pageY};
	slideLeft = this.data.position.end.x-this.data.position.start.x;

	if(slideLeft == 0){
		
		if(isLink){
			e.stopPropagation();
			_this.off('click').click();
			/*var url = _this.attr('href');
			document.title = url;
			location.href = url;*/
		}
	}else{
		if(isLink){_this = _this.parents('.wList-item');}

		if(slideLeft > 0){
			var hasClass = _this.hasClass('show-delete');
			if(!hasClass){
				$('.show-delete').removeClass('show-delete');
				_this.addClass('show-delete');
			}
		}else if(slideLeft < 0){
			_this.removeClass('show-delete');
		}
	}
}

function wListItemDeleteBtn_click(e){
	e.preventDefault();
	e.stopPropagation();
	var _this = $(this);
	var presentation = _this.attr('presentation');
	var presentationLink = presentation+'&'+appName+appAgenda;
	var valor = 0;
	var links = localStorage['links'];
	var linksNews = new Array();
	var itemsLen = 0;
	links = links.split(',');

	for(var i=0; i<links.length; i++){
		if(links[i] != presentationLink){
			linksNews.push(links[i]);
		}
	}
	linksNews = linksNews.join();

	localStorage['links'] = linksNews;
	delete localStorage[presentation+'&'+appName+appAgenda+''];
	delete localStorage[presentation+'&'+appName+appAgenda];
	delete localStorage[presentation+'&'+appName+appAgenda+'date'];
	delete localStorage[presentation+'&'+appName+appAgenda+'menu'];

	_this.parents('.wList-item').remove();
	itemsLen = $('.wList-item');

	if(itemsLen.length == 0){wListNone.addClass('display-block');}
}

//
function disabled_click(e){e.preventDefault();}

//
function btn_stopPropagation(e){e.stopPropagation();}

var normalize = (function() {
  var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
      to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
      mapping = {};
 
  for(var i = 0, j = from.length; i < j; i++ )
      mapping[ from.charAt( i ) ] = to.charAt( i );
 
  return function( str ) {
      var ret = [];
      for( var i = 0, j = str.length; i < j; i++ ) {
          var c = str.charAt( i );
          if( mapping.hasOwnProperty( str.charAt( i ) ) )
              ret.push( mapping[ c ] );
          else
              ret.push( c );
      }      
      return ret.join( '' );
  }
 
})();

//
function presentation_get(){
	var pres = localStorage.getItem('links');
	if(pres == null){
		pres = '';
	}else{
		pres = pres.split(',');
	}
	return pres;
}

function presentation_load(pres){
	pres = (pres == undefined) ? '' : pres.trim();
	if(pres != '' && pres != 'Avastin'){
		pres = pres.replace(/ /g,'_');
		var preName = pres+'&'+appName+appAgenda;
		var slides = '';
		presentation.presentation = {};

		slides = localStorage.getItem(preName);

		if(slides != null){
			var isJSON = true;
			try{
				slides = JSON.parse(slides);
			}catch(e){
				isJSON = false;
			}

			if(isJSON){
				for(var i=0; i<slides.length; i++){
					for(var j=0; j<slides[i].length; j++){
						presentation.add_slide(slides[i][j]);
					}
				}
			}else{
				slides = slides.split(',');

				for(var i=0; i<slides.length; i++){
					presentation.add_slide(slides[i]);
				}
			}
			presentationName = pres;
			ordenar.addClass('edit');
			ordenarTopbarNombre.text(pres.replace(/_/g,' '));
		}
	}
}

//
function indexCarouselConImgs_loading(loading,src,status){
	if(!loading){
		indexCarousel.iCarousel({
			slides: 7,
			autoPlay: false,
			make3D: true,
			animationSpeed: 300,
			slidesSpace: 50,
			mouseWheel: false,
			startSlide: 0,
			perspective: 70,
			timerOpacity: 0, /* dont show timer */
			directionNav: false, /* dont show arrows */
			onAfterChange: function () {
				var section = this.defs.currentSlide[0].children[0].getAttribute('slide');
				select_slide(section);
            }
		});
		indexCarouselCon.removeClass('visibility-hidden');
	}
}

function edSelCarousel_change(){
	if(this.checked){
		wSliderMenuSelect.addClass('display-block');
		indexCarouselCon.removeClass('display-block');
	}else{
		wSliderMenuSelect.removeClass('display-block');
		indexCarouselCon.addClass('display-block');
	}
	wSelectSliders.removeClass('display-block');
	wSliderMenuSelectImg.removeClass('display-block');
}

function wSliderMenuLink_click(e){
	e.preventDefault();
	var section = this.getAttribute('slide');
	select_slide(section);
}

function select_slide(section){
	var img = editorThumbsSRC+'capitulo_'+section+'.png';
	var wSelectSlidersItemsHTML = '';
	var slideKey = '';
	var slideActive = '';

	wSelectSliders.addClass('display-block');

	wSelectSlidersImg.attr('src',img);
	wSliderMenuSelectImg.addClass('display-block').attr('src',img).attr('slide',section);

	$('.wSelectSliders-slide').remove();

	for(var slide in appJSON.structures[section].content){
		slideKey = appJSON.structures[section].content[slide];
		slide = appJSON.slides[slideKey];
		slideActive = presentation.check(slideKey) ? 'active' : '';
		
		wSelectSlidersItemsHTML += slider_get_html(section, slideKey, slide.name, slideActive, true);
	}
	wSelectSlidersItemsHTML += '<div class="clear-both"></div>'

	wSelectSlidersItems.append(wSelectSlidersItemsHTML);

	wSelectSlidersItems.find('.wSelectSliders-slide-link').on('click',wSelectSlidersSlideLink_click);
	wSelectSlidersItems.find('.wSelectSliders-slide-zoom').on('click',img_zoom);
}

function slider_get_html(section, slideKey, name, slideActive,slideThumbs){
	var thumbs = slideThumbs ? slideThumbsSRC : editorThumbsSRC;
	html = '<div class="wSelectSliders-slide" slide="'+slideKey+'"><div class="wSelectSliders-slide-link-con '+slideActive+'" slide="'+slideKey+'" section="'+section+'"><div class="wSelectSliders-slide-link '+wSliderMenuType[section]+'"><img class="wSelectSliders-slide-img" style="width:115px;height:115px;" src="'+thumbs+slideKey+'.jpg"></a><a class="wSelectSliders-slide-zoom" href=""></div><a class="wSelectSliders-slide-delete" href=""></a></div><span class="wSelectSliders-slide-name">'+name+'</span></div>';
	return html;
}

function wSelectSlidersShowhidden_click(e){
	e.preventDefault();
	var _this = $(this);
	var active = _this.hasClass('active');

	if(active){
		_this.removeClass('active');
		wSelectSlidersItems.find('.wSelectSliders-slide').removeClass('display-none');
	}else{
		_this.addClass('active');
		wSelectSlidersItems.find('.wSelectSliders-slide').addClass('display-none');
	}
}

function wSelectSlidersSlideLink_click(e){
	e.preventDefault();
	var parent = $(this.parentNode);
	var active = parent.hasClass('active');
	var section = parent.attr('section');
	var slide = parent.attr('slide');
	
	if(active){
		parent.removeClass('active');
		presentation.delete_slide(slide);
	}else{
		parent.addClass('active');
		presentation.add_slide(slide);
	}
}

function wSelectSlidersSlide_mousedown(e){
	e.preventDefault();
	e.stopPropagation();
	var _this = $(this); 
	slide_delete_active(_this);
	ordenar.on('mousedown',ordenar_mousedown);
}

function slide_delete_active(_this){
	var parent = _this.parent();
	var isSection = parent.hasClass('wOrderSliders-section');

	slide_active_disabled();
	
	if(isSection){
		slideDeleteSliders = ordenarSliders.find('.wOrderSliders-section .wSelectSliders-slide');
		for(var i=0; i<slideDeleteSliders.length; i++){
			slideDeleteSliders[i].addEventListener('touchstart',wSelectSlidersSection_touchstart,false);
			slideDeleteSliders[i].addEventListener('touchmove',wSelectSlidersSection_touchmove,false);
			slideDeleteSliders[i].addEventListener('touchend',wSelectSlidersSection_touchend,false);
		}
	}else{
		parent = parent.parent();
		slideDeleteSliders = parent.find('.wOrderSliders-sliders .wSelectSliders-slide');
		for(var i=0; i<slideDeleteSliders.length; i++){
			slideDeleteSliders[i].addEventListener('touchstart',wSelectSlidersSlide_touchstart,false);
			slideDeleteSliders[i].addEventListener('touchmove',wSelectSlidersSlide_touchmove,false);
			slideDeleteSliders[i].addEventListener('touchend',wSelectSlidersSlide_touchend,false);
		}
	}
	slideDeleteSliders.addClass('delete').addClass('wSelectSliders-slide-moving');
}

function slide_active_disabled(){
	slideDeleteSliders.removeClass('delete')
	slideDeleteSliders.removeClass('wSelectSliders-slide-moving');

	for(var i=0; i<slideDeleteSliders.length; i++){
		slideDeleteSliders[i].removeEventListener('touchstart',wSelectSlidersSection_touchstart,false);
		slideDeleteSliders[i].removeEventListener('touchmove',wSelectSlidersSection_touchmove,false);
		slideDeleteSliders[i].removeEventListener('touchend',wSelectSlidersSection_touchend,false);
		slideDeleteSliders[i].removeEventListener('touchstart',wSelectSlidersSlide_touchstart,false);
		slideDeleteSliders[i].removeEventListener('touchmove',wSelectSlidersSlide_touchmove,false);
		slideDeleteSliders[i].removeEventListener('touchend',wSelectSlidersSlide_touchend,false);
	}
}

function wSelectSlidersSection_touchstart(e){
	e.preventDefault();
	wSelectSlidersSection_touchstart_sel(e,this,'ver','x');
}

function wSelectSlidersSection_touchmove(e){
	wSelectSlidersSection_touchmove_sel(e,'ver','x');
}

function wSelectSlidersSection_touchend(){
	wSelectSlidersSection_touchend_sel('ver');
}

function wSelectSlidersSlide_touchstart(e){
	e.preventDefault();
	wSelectSlidersSection_touchstart_sel(e,this,'hor','y');
}

function wSelectSlidersSlide_touchmove(e){
	wSelectSlidersSection_touchmove_sel(e,'hor','y');
}

function wSelectSlidersSlide_touchend(){
	wSelectSlidersSection_touchend_sel('hor');
}

function wSelectSlidersSection_touchstart_sel(e,_this,direction,axis){
	var cordinate = {
		x: e.targetTouches[0].pageX,
		y: e.targetTouches[0].pageY
	}
	var _this = $(_this);

	direction = 'wSelectSlidersSection_side_'+direction;
	_this.addClass('moving');

	wSelectSlidersSeparator = window[direction](_this,cordinate[axis]);
	if(wSelectSlidersSeparator.side != ''){wSelectSlidersSeparator.addClass('active');}
}

function wSelectSlidersSection_touchmove_sel(e,direction,axis){
	var cordinate = {
		x: e.targetTouches[0].pageX,
		y: e.targetTouches[0].pageY
	}
	var x = cordinate['x']-window.pageXOffset;
	var y = cordinate['y']-window.pageYOffset;
	var _this = document.elementFromPoint(x,y);

	direction = 'wSelectSlidersSection_side_'+direction;

	wSelectSlidersSeparator = window[direction](_this,cordinate[axis]);
	if(wSelectSlidersSeparator.side != ''){wSelectSlidersSeparator.addClass('active');}
}

function wSelectSlidersSection_touchend_sel(direction){
	var separador;
	var slide = $('.wSelectSliders-slide.moving');
	var moving = (direction == 'ver') ? slide.parent().parent() : slide;
	$('.wOrderSliders-separator.active').removeClass('active');

	if(wSelectSlidersSeparator.side == 'left'){
		separador = moving.prev();
		wSelectSlidersSeparator.before(moving);
		moving.before(separador);
	}else if(wSelectSlidersSeparator.side == 'right'){
		separador = moving.next();
		wSelectSlidersSeparator.after(moving);
		moving.after(separador);
	}

	slide.removeClass('moving');
}

function wSelectSlidersSection_side_ver(_this,x){
	var separator = wSelectSlidersSection_side(_this,x,'Width','left');
	return separator;
}

function wSelectSlidersSection_side_hor(_this,y){
	var separator = wSelectSlidersSection_side(_this,y,'Height','top');
	return separator;
}

function wSelectSlidersSection_side(_this,cordinate,sizeName,offsetName){
	var isElement = false;
	var separator = {side : ''};

	_this = $(_this);
	isElement = _this.hasClass('wSelectSliders-slide-moving');
	if(!isElement){_this = _this.parents('.wSelectSliders-slide-moving');}
	_this = _this.hasClass('wSelectSliders-slide-moving') ? _this : undefined;

	if(_this != undefined){
		var offset = _this.offset();
		var size = 0;
		var side = 'left';

		sizeName = 'outer'+sizeName;

		size = _this[sizeName](true);
		size = size/2;
		size = parseInt(size);
		cordinate -= offset[offsetName];

		$('.wOrderSliders-separator.active').removeClass('active');
		if(sizeName == 'outerWidth'){_this = _this.parent().parent();}

		if(size <= cordinate){side = 'right';}
		separator = (side == 'right') ? _this.next() : _this.prev();
		separator.side = side;
	}

	return separator;
}
function ordenar_mousedown(e){
	e.preventDefault();
	e.stopPropagation();
	slide_active_disabled();
	ordenar.off('mousedown',ordenar_mousedown);
}
//
function img_zoom(e){
	e.preventDefault();
	e.stopPropagation();
	var slide = this.parentNode.parentNode.getAttribute('slide');
	var img = slideThumbsSRC+slide+'.jpg';

	indexCarousel.css({'opacity':'0'});

	wdialog.show('zoom');
	wDialogZoomImg.attr('src',img);
}

function img_zoom_close(){
	indexCarousel.css({'opacity':'1'});
}
//
function wSelectSlidersSlideDeleteSliders_click(e){
	e.preventDefault();
	e.stopPropagation();
	var _this = $(this);
	var parent = _this.parents('.wSelectSliders-slide');
	var slide = parent.attr('slide');
	var length = 0;

	presentation.delete_slide(slide);

	length = presentation.size();
	if(length == 0){
		ordenar_pag();
	}else{
		parent = parent.parents('.wOrderSliders-group');
		parent.prev().remove();
		parent.remove();
	}

	$('.wSelectSliders-slide-link-con[slide="'+slide+'"]').removeClass('active');
}

//
function wDialogAlertSaveForm_submit(e){
	e.preventDefault();
	var name = this['name'].value;
	name = name.trim();

	if(name != ''){
		name = name.replace(/ /g,'_');
		name = normalize(name);
		var preName = name+'&'+appName+appAgenda;
		var pres = presentation_exist(preName);
		
		if(pres.exist){
			wdialog.close();
			wdialog.show('alert-update');
		}else{
			presentation_save(name,preName)
		}
	}
}

function wDialogAlertUpdateReplace_click(e){
	e.preventDefault();
	var wDialogAlertSaveNameVal = wDialogAlertSaveName.val();
	var name = (wDialogAlertSaveNameVal == '') ? presentationName : wDialogAlertSaveNameVal;
	var preName = name+'&'+appName+appAgenda;

	presentation_save(name,preName);
}

function wDialogAlertCancel_click(){
	wDialogAlertSaveName.val('');
}


//
function presentation_save(name,preName){
	var groups = $('#ordenarSliders .wOrderSliders-group');
	var pres = presentation_exist(preName);
	var slideElement;
	var slide = '';
	var slides = '';
	var preMenuName = preName+'menu';
	var preDate = preName+'date';
	var strLength = 0;
	var date = new Date();
	var dateMonth = date.getMonth();
	var dateHours = date.getHours();
	var dateMinutes = date.getMinutes();
	var dateStr = '';
	var nombre="vacio";

	dateMonth += 1;
	dateHours = (dateHours < 10) ? '0'+dateHours : dateHours;
	dateMinutes = (dateMinutes < 10) ? '0'+dateMinutes : dateMinutes;
	dateStr = date.getDate()+'/'+dateMonth+'/'+date.getFullYear()+' '+dateHours+':'+dateMinutes;
	
	presentation.presentation = {};
		
	for(var i=0; i<groups.length; i++){
		slideElement = $(groups[i]).find('.wSelectSliders-slide-link-con');
		slide = slideElement.attr('slide');
		slides += slide+',';
	}
	strLength = slides.length-1;
	slides = slides.substring(0,strLength);
	
	localStorage.setItem(preName,slides);
	localStorage.setItem(preDate,dateStr);
	if(!pres.exist){
		pres.str = (pres.str == '') ? preName : pres.str+','+preName;
		localStorage.setItem('links',pres.str);
	}
	nombre = normalize(name);
	presentationName = nombre;
	
	wdialog.close();
	wDialogAlertCancel_click();
	ordenar.addClass('edit');
	ordenarTopbarNombre.text(presentationName.replace(/_/g,' '));
}

function presentation_exist(preName){
	var pres = localStorage.getItem('links');
	var res = {
		'exist': false,
		'str': pres
	}

	if(res.str == null){res.str = '';}
	pres = res.str.split(',');

	for(var i=0; i<pres.length; i++){
		if(pres[i] == preName){
			res.exist = true;
			break;
		}
	}

	return res;
}

//
function abrirCerrarBtn_click(e){
	e.preventDefault();
	wdialog.show('alert-exit');
}

//
function guideBack_click(e){
	e.preventDefault();
	window.history.back();
}

function guidePDF_click(e){
	e.preventDefault();
	openPDF("content/pdf/guia-passo-a-passo.pdf");
}

//
function window_orientationchange(){
	if (window.orientation != 0 && window.orientation != 180){
        var dir = document.location.pathname;
		var pres = (presentationName == undefined || presentationName == '') ? '' : '?='+presentationName+'&'+appName+appAgenda;

    	dir = dir.replace('index.html','');
    	dir += '../index.html'+pres;
    	document.location.href = dir;
    }
}

//
function Presentation(){
	this.presentation = {};
	return this;
}

Presentation.prototype = {
	add_slide: function(slide){
		this.presentation[slide] = true;
	},
	check: function(slide){
		var exist = (this.presentation[slide] == undefined) ? false : true;
		return exist;
	},
	check_section: function(section){
		var exist = (this.presentation[section] == undefined) ? false : true;
		return exist;
	},
	delete_slide: function(slide){
		delete this.presentation[slide];
	},
	delete_section: function(section){
		delete this.presentation[section];
	},
	size: function(){
		var length = 0;
		for(var k in this.presentation){length++;}
		return length;
	}
}

function wDialog(_this,o){
	d = {
		closeClass: 'wDialog-close'
	}

	if(typeof o == 'object'){
		for(var k in d){
			if(o[k] != undefined){
				d[k] = o;
			}
		}
	}

	for(var k in d){this[k] = d[k];}

	this.container = $(_this);
	this.init();

	return this;
}

wDialog.prototype = {
	init: function(){
		var _this = this;
		var close = '.'+this.closeClass;

		$(close).on('click',function(e){
			e.preventDefault();
			_this.close();
		});
		$('*[wdialog]').on('click',function(e){
			e.preventDefault();
			var dialog = this.getAttribute('wdialog');
			_this.show(dialog);
		})
	},
	show: function(item){
		var itemSel = this.container.attr('item');
		item = 'wDialog-'+item+'-con';
		this.container.attr('item',item).removeClass(itemSel).addClass(item).addClass('display-block');
	},
	close: function(){
		var itemSel = this.container.attr('item');
		this.container.attr('item','').removeClass(itemSel).removeClass('display-block');
	}

}